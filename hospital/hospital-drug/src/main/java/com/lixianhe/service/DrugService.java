package com.lixianhe.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface DrugService {
    /**
     * 获取所有药品信息
     * @return 所有药品的JSON
     */
    String getDrugLimit(int page);

    int getDrugMaxCount();

    int getClassifyMaxCount();

    String getClassify();


    String getDrugByID(Map<String,Object> map);
    String getDrugByClassify(int c_id);
    String getDrugByPrice(Map<String,Object> map);

    /**
     * 添加缓存信息
     * @param id 药品id
     * @param cache 药品信息的JSON字符串
     */
    void cacheDrug(String id,String cache);

    /**
     * 查询缓存
     * @param id 药品id
     * @return 查询结果
     */
    String queryCache(String id);

    /**
     * 根据关键字查询对应的药品
     * @param keyword 关键字
     * @return 与关键字相关的所有药品
     * @throws IOException IO异常
     */
    String Search(String keyword) throws IOException;

    /**
     * 向排行榜中添加数据
     */
    void addSort(String id);

    /**
     * 获取排行榜数据前十药品数据
     * @return 药品对象id和访问次数的Map集合
     */
    List<Map<String, Double>> getSortList();

    /**
     * 添加搜索记录
     * @param id 用户的信息
     * @param text 记录
     * @return 添加结果
     */
    Long addHistory(String id, String text);

    /**
     * 获取用户的搜索历史
     * @param id 用户的信息
     * @return 搜素结果
     */
    String getHistory(String id);

    String getSellSort(int id);
}
