package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.constant.StaticData;
import com.lixianhe.dao.DrugMapper;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author 李显赫
 * @Date 2022-04-01 14:14
 */
@Service
public class DrugServiceImpl implements DrugService {

    @Autowired
    private DrugMapper drugMapper;

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    @Autowired
    @Qualifier("redisTemplateHistory")
    private StringRedisTemplate redisTemplatehistory;

    @Autowired
    @Qualifier("redisTemplateSort")
    private StringRedisTemplate redisTemplateSort;

    @Autowired
    @Qualifier("redisTemplateDrugCache")
    private StringRedisTemplate redisTemplateCache;

    @Override
    public String getClassify() {
        return JSON.toJSONString(drugMapper.getClassify());
    }

    @Override
    public String getDrugLimit(int page) {
        // 这页的开始位置
        int start = (page - 1) * StaticData.PAGE_SIZE + 1;
        // 这页的结束位置
        int end = (page) * StaticData.PAGE_SIZE;
        Map<String, Object> map = new HashMap<>(2);
        map.put("start", start);
        map.put("end", end);
        return JSON.toJSONString(drugMapper.getDrugLimit(map));
    }

    @Override
    public int getDrugMaxCount() {
        return drugMapper.getDrugMaxCount();
    }

    @Override
    public int getClassifyMaxCount() {
        return drugMapper.getClassifyMaxCount();
    }

    @Override
    public String getDrugByID(Map<String, Object> map) {
        String id = (String) map.get("drugId");
        // 给热度排行榜的这个药品的权值加1
        addSort(id);
        String queryCache = queryCache(id);
        // 判断缓存是否命中
        if (queryCache == null) {
            String queryResult = JSON.toJSONString(drugMapper.getDrugByID(map));
            // 将查询结果存到缓存中
            cacheDrug(id, queryResult);
            return queryResult;
        } else return queryCache;
    }

    @Override
    public String getDrugByClassify(int id) {
        return JSON.toJSONString(drugMapper.getDrugByCid(id));
    }

    @Override
    public String getDrugByPrice(Map<String, Object> map) {
        return JSON.toJSONString(drugMapper.getDrugByPrice(map));
    }

    @Override
    public List<Map<String, Double>> getSortList() {
        List<Map<String, Double>> list = new ArrayList(10);
        ZSetOperations zset = redisTemplateSort.opsForZSet();
        Set<ZSetOperations.TypedTuple<Object>> tuples = zset.reverseRangeByScoreWithScores(StaticData.DRUG_SORT, 0, Integer.MAX_VALUE, 0, 10);
        for (ZSetOperations.TypedTuple<Object> t : Objects.requireNonNull(tuples)) {
            Map<String, Double> map = new HashMap<>();
            map.put("id", Double.parseDouble((String) Objects.requireNonNull(t.getValue())));
            map.put("score", t.getScore());
            list.add(map);
        }
        return list;
    }

    @Override
    public void addSort(String id) {
        ZSetOperations zSetOperations = redisTemplateSort.opsForZSet();
        // 被访问了一次，权值加一
        zSetOperations.incrementScore(StaticData.DRUG_SORT, id, 1);
    }

    @Override
    public String Search(String keyword) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>(10);
        SearchRequest searchRequest = new SearchRequest(StaticData.ELASTIC_SEARCH_INDEX);
        // 构建搜索条件
        SearchSourceBuilder searchSource = new SearchSourceBuilder();
        // 高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        // 前后标签和加标签的字段
        highlightBuilder.preTags("<span style='color:red;'>").postTags("</span>").field(StaticData.ELASTIC_SEARCH_NAME).requireFieldMatch(true);
        // 词条查询
        searchSource.query(QueryBuilders.termsQuery(StaticData.ELASTIC_SEARCH_NAME, keyword));

        searchSource.highlighter(highlightBuilder);
        searchRequest.source(searchSource);
        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = search.getHits();

        for (SearchHit hit : hits) {
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField title = highlightFields.get(StaticData.ELASTIC_SEARCH_NAME);
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();  //没高亮的数据
            if (title != null) {
                Text[] fragments = title.fragments();
                StringBuilder n_title = new StringBuilder();
                for (Text text : fragments) {
                    n_title.append(text);
                }
                sourceAsMap.put(StaticData.ELASTIC_SEARCH_NAME, n_title.toString());   //把高亮字段替换掉原本的内容即可
            }
            list.add(sourceAsMap);
        }
        return JSON.toJSONString(list);
    }

    @Override
    public String getSellSort(int page) {
        HashMap<String, Object> map = new HashMap(2);
        int start = (page - 1) * StaticData.PAGE_SIZE;
        int end = page * StaticData.PAGE_SIZE;
        map.put("start", start);
        map.put("end", end);
        return JSON.toJSONString(drugMapper.getSellSort(map));
    }

    @Override
    public Long addHistory(String username, String keyword) {
        ListOperations listOperations = redisTemplatehistory.opsForList();
        // 获取该用户的搜索历史
        List historyList = listOperations.range(username, 0, -1);
        // 如果搜索历史已经存在，则不插入
        if (historyList.indexOf(keyword) != -1) return 0L;
        // 右边插入
        Long count = listOperations.rightPush(username, keyword);
        // 最多12条历史，超出12条，从左边删除
        if (listOperations.size(username) >= StaticData.SEARCH_HISTORY_MAX_COUNT) {
            listOperations.leftPop(username);
        }
        return count;
    }

    @Override
    public String getHistory(String username) {
        ListOperations listOperations = redisTemplatehistory.opsForList();
        List<String> list = listOperations.range(username, 0, -1);
        return JSON.toJSONString(list);
    }

    @Override
    public String queryCache(String id) {
        ValueOperations valueOperations = redisTemplateCache.opsForValue();
        return (String) valueOperations.get(id);
    }

    @Override
    public void cacheDrug(String id, String cache) {
        ValueOperations valueOperations = redisTemplateCache.opsForValue();
        valueOperations.set(id, cache, StaticData.DRUG_CACHE_TIME, TimeUnit.DAYS);
    }
}
