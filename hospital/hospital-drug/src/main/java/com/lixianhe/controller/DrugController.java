package com.lixianhe.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lixianhe.constant.StaticData;
import com.lixianhe.load.CommandLineRunnerController;
import com.lixianhe.pojo.Drug;
import com.lixianhe.service.DrugServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Null;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@RestController
@Slf4j
public class DrugController {

    @Autowired
    private DrugServiceImpl drugService;

    /**
     * 获取所有的药品类别
     *
     * @return 所有的药品类别
     */
    @GetMapping(value = "/classifys")
    public String getClassify() {
        return drugService.getClassify();
    }

    /**
     * 根据页数查询药品
     *
     * @param page     也是
     * @param response 响应结果
     * @return 一页的所有药品
     * @throws IOException IO异常
     */
    @GetMapping(value = "/drugs/limit/{page}")
    public String getDrugLimit(@PathVariable int page, HttpServletResponse response) {
        int maxPage = (int) Math.ceil(1.0 * CommandLineRunnerController.drugMaxCount / 20);
        if (page > maxPage || page < 0) {
            return ForbidUtils.ForbidRequest(response);
        }
        return drugService.getDrugLimit(page);
    }

    /**
     * 根据药品id获取药品详情
     *
     * @param jsonObject 药品id
     * @return 药品详情
     */
    @PostMapping(value = "/drugs/id")
    public String getDrugById(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        int drugId;
        try {
            if (jsonObject.size() != 1 ||
                    (drugId = Integer.parseInt((String) jsonObject.get("drugId"))) <= 0
                    || drugId > CommandLineRunnerController.drugMaxCount) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return drugService.getDrugByID(jsonObject);
    }

    /**
     * 根据药品类别查询药品
     *
     * @param id 药品类的id
     * @return 该类的所有药品
     */
    @GetMapping(value = "/drugs/classify/{id}")
    public String getDrugsByClassify(@PathVariable int id, HttpServletResponse response) {
        if (id > CommandLineRunnerController.classifyMaxCount && id < 0) {
            return ForbidUtils.ForbidRequest(response);
        }
        return drugService.getDrugByClassify(id);
    }

    /**
     * 根据价格范围查询药品
     *
     * @param jsonObject 价格的最大最小值
     * @return 在该价格范围的药品
     */
    @PostMapping("/drugs/price")
    public String getDrugBtPrice(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        double MinPrince, MaxPrice;
        try {
            // 如果参数个数不等于2，或者最低价大于最高价
            if (jsonObject.size() != 2 || (MaxPrice = Double.parseDouble((String) jsonObject.get("max"))) <
                    (MinPrince = Double.parseDouble((String) jsonObject.get("min")))
                    || MaxPrice < 0) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        // 如果最低价小于0，转化为0
        if (MinPrince < 0) {
            MinPrince = 0;
            jsonObject.remove("min");
            jsonObject.put("min", MinPrince);
        }
        return drugService.getDrugByPrice(jsonObject);
    }

    /**
     * 获取热度排行榜
     *
     * @return 排行榜
     */
    @GetMapping("/sort")
    public String getSort() {
        return JSON.toJSONString(drugService.getSortList());
    }

    /**
     * 根据药品关键字搜索药品
     *
     * @param jsonObject 用户信息和搜索的信息
     * @return 搜素的结果
     */
    @PostMapping("/search")
    public String SearchDrug(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        String keyword;
        try {
            if (jsonObject.size() != 1 || (keyword = (String) jsonObject.get("keyword")) == null) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return drugService.Search(keyword);
    }

    /**
     * 获取用户搜索历史
     *
     * @param jsonObject 用户信息
     * @return 搜素结果
     */
    @PostMapping("/history")
    public String getHistory(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        String openid;
        try {
            if (jsonObject.size() != 1 || (openid = (String) jsonObject.get("openid")) == null ||
                    openid.length() == 0) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return drugService.getHistory(openid);
    }

    /**
     * 添加用户搜索历史
     *
     * @param jsonObject 用户信息
     * @return 添加结果
     */
    @PutMapping("/history")
    public String addHistory(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        String username, keyword;
        try {
            if (jsonObject.size() != 2 || (username = (String) jsonObject.get("username")) == null ||
                    username.length() == 0 || (keyword = (String) jsonObject.get("keyword")) == null || keyword.length() == 0) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(drugService.addHistory(username, keyword));
    }

    /**
     * 销售排行榜
     *
     * @param page 页数
     * @return 该页数的所有数据
     */
    @GetMapping("/sell/sort/{page}")
    public String sellSort(@PathVariable int page, HttpServletResponse response) {
        int maxPage = (int) Math.ceil(1.0 * CommandLineRunnerController.drugMaxCount / 20);
        if (page > maxPage || page < 0) {
            return ForbidUtils.ForbidRequest(response);
        }
        return drugService.getSellSort(page);
    }

    /**
     * 获取药品数量
     *
     * @return 药品最大值
     */
    @GetMapping("/drugs/count")
    public String getDrugCount() {
        return String.valueOf(drugService.getDrugMaxCount());
    }
}