package com.lixianhe.load;

import com.lixianhe.service.DrugServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

/**
 * @author 李显赫
 * @Date 2022-04-09 14:11
 */
@Controller
public class CommandLineRunnerController implements CommandLineRunner {

    public static int drugMaxCount;

    public static int classifyMaxCount;

    @Autowired
    private DrugServiceImpl drugService;

    @Override
    public void run(String... args) {
        // 项目加载的时候给药品查询药品数量的最大值
        drugMaxCount = drugService.getDrugMaxCount();

        // 项目加载的时候给药品查询药品种类的最大值
        classifyMaxCount = drugService.getClassifyMaxCount();
    }
}
