package com.lixianhe.dao;

import com.lixianhe.pojo.Classify;
import com.lixianhe.pojo.Drug;
import com.lixianhe.pojo.Search;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-01 14:14
 */
@Mapper
public interface DrugMapper {

    /**
     * 获取所有的药品类别
     * @return 类别对象的列表集合
     */
    List<Classify> getClassify();

    /**
     * 查询某一页的药品
     *
     * @return 这页所有药品的List集合
     */
    List<Drug> getDrugLimit(Map<String,Object> map);

    /**
     * 根据药品id查询药品
     *
     * @param map 药品id和用户信息
     * @return Map集合封装药品对象的属性
     */
    Drug getDrugByID(Map<String,Object> map);

    /**
     * 根据id查询类型
     *
     * @param id 药品类型
     * @return 类别对象列表
     */
    List<Drug> getDrugByCid(@Param("id") int id);

    /**
     * 根据价格查询药品
     *
     * @param map 药品的最大价格和最小价格
     * @return 符合条件的药品信息
     */
    List<Drug> getDrugByPrice(Map<String, Object> map);


    List<Drug> getSellSort(Map<String,Object> map);

    @Select("select count(1) from drug where d_is_delete = 0")
    int getDrugMaxCount();

    @Select("select count(1) from classify where c_is_delete = 0")
    int getClassifyMaxCount();
}
