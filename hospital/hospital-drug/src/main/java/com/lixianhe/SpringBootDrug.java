package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李显赫
 * @Date 2022-04-23 23:26
 */
@SpringBootApplication
public class SpringBootDrug {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootDrug.class, args);
    }
}
