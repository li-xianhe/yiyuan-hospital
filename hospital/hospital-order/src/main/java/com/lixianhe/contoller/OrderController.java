package com.lixianhe.contoller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.constant.StaticData;
import com.lixianhe.service.OrderServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import com.lixianhe.utils.RandomUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@RestController
@Slf4j
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostMapping
    public String getOrderByPid(@RequestBody JSONObject jsonObject) {
        return orderService.getOrderByPid(jsonObject);
    }

    @PostMapping("/message")
    public String TestRabbitMQProd(@RequestBody JSONObject jsonObject){
        jsonObject.put("oId", RandomUtils.getCode(18));
        String message = JSONObject.toJSONString(jsonObject);
        System.out.println("生产者发出消息:"+ message);
        rabbitTemplate.convertAndSend(StaticData.DELAY_EXCHANGE,StaticData.DELAYED_ROUTING_KEY,
                message, msg -> {
                msg.getMessageProperties().setDelay(StaticData.DEFAULT_DELAY_TIME);
                return msg;
            });
        addOrder(jsonObject);
        return message;
    }

    @RabbitListener(queues = StaticData.DELAYED_QUEUE)
    public void TestRabbitMQComuser(Message message){
        String msg =  new String(message.getBody());
        log.info("消费者消费消息{}",msg);
        System.out.println(addRes(JSONObject.parseObject(msg, Map.class)));
    }

    public int addOrder(Map<String,Object> map){
        return orderService.addOrder(map);
    }

    public int addRes(Map<String,Object> map){
        return orderService.addRes(map);
    }

    @PostMapping("/results")
    public String getResultByPid(@RequestBody JSONObject jsonObject,HttpServletResponse response){
        try {
            if (jsonObject.size() != 1) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return orderService.getResultByPid(jsonObject);
    }

    @PostMapping("/times")
    public String getTimes(@RequestBody JSONObject jsonObject, HttpServletResponse response){
        try {
            if (jsonObject.size() != 1) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return orderService.getTimes(jsonObject);
    }

    @PostMapping("/orderMsg")
    public String getMsg(@RequestBody JSONObject jsonObject,HttpServletResponse response){
        try {
            if (jsonObject.size() != 1) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return  orderService.getMsg(jsonObject);
    }
}
