package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李显赫
 * @Date 2022-04-24 15:53
 */
@SpringBootApplication
public class SpringBootOrder {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootOrder.class, args);
    }
}
