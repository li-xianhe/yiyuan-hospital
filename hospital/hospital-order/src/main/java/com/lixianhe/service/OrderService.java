package com.lixianhe.service;

import com.lixianhe.pojo.Order;

import java.util.Map;

public interface OrderService {
    String getOrderByPid(Map<String,Object> map);
    int addOrder(Map<String,Object> map);
    int addRes(Map<String,Object> map);
    String getResultByPid(Map<String,Object> map);
    String getTimes(Map<String,Object> map);
    String getMsg(Map<String,Object> map);
}
