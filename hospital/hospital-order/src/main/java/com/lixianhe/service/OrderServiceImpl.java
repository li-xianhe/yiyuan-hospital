package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.OrderMapper;
import com.lixianhe.pojo.Order;
import com.lixianhe.pojo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public String getOrderByPid(Map<String,Object> map) {
        return JSON.toJSONString(orderMapper.getOrderByPid(map));
    }

    @Override
    public int addOrder(Map<String, Object> map) {
        return orderMapper.addOrder(map);
    }

    @Override
    public int addRes(Map<String, Object> map) {
        return orderMapper.addRes(map);
    }

    @Override
    public String getResultByPid(Map<String,Object> map) {
        List<Result> list = orderMapper.getResultByPid(map);
        return JSON.toJSONString(list);
    }

    public String getTimes(Map<String,Object> map){
        return JSON.toJSONString(orderMapper.getTimes(map));
    }

    @Override
    public String getMsg(Map<String,Object> map) {
        return JSON.toJSONString(orderMapper.getMsg(map));
    }
}
