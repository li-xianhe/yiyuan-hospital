package com.lixianhe.dao;

import com.lixianhe.pojo.Order;
import com.lixianhe.pojo.Result;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Mapper
public interface OrderMapper {
    /**
     * 根据用户的openid查询用户的所有订单
     * @param map 用户openid信息
     * @return 订单
     */
    List<Order> getOrderByPid(Map<String,Object> map);

    int addOrder(Map<String,Object> map);

    int addRes(Map<String,Object> map);

    /**
     * 根据用户的openid查询用户的订单
     * @return 结果对象
     */
    List<Result> getResultByPid(Map<String,Object> map);

    @Select("select create_time from patient where p_openid = #{openid}")
    List<String> getTimes(Map<String,Object> map);

    Result getMsg(Map<String,Object> map);
}
