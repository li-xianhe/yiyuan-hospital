package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author 李显赫
 * @Date 2022-04-24 14:50
 */
@SpringBootApplication
public class SpringBootUser {
    public static StringRedisTemplate redisTemplateDefault = null;
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SpringBootUser.class, args);
        redisTemplateDefault = (StringRedisTemplate) run.getBean("redisTemplateDefault");
    }
}