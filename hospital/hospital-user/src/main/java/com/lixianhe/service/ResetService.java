package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 16:14
 */
public interface ResetService {
    Integer resetPassword(Map<String, Object> map);
}
