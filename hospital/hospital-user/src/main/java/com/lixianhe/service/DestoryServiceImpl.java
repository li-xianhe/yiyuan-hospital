package com.lixianhe.service;

import com.lixianhe.dao.DestoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 17:13
 */
@Service
public class DestoryServiceImpl implements DestoryService {

    @Autowired
    private DestoryMapper destoryMapper;

    @Override
    public Integer destoryUser(Map<String, Object> map) {
        String status = (String) map.get("status");
        if("1".equals(status)){
            return destoryMapper.destoryDoctor(map);
        }else {
            return destoryMapper.destoryPatient(map);
        }
    }
}
