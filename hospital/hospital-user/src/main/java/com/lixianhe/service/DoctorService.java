package com.lixianhe.service;


import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-03 11:08
 */
public interface DoctorService {
    String doctorById(int d_id);
    String doctorByWorkId(int w_id);
    String prefectDoctor(Map<String,Object> map);


}
