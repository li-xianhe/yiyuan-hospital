package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-19 14:35
 */
public interface CollectService {
    String getCollects(Map<String,Object> map);
    Integer addCollect(Map<String,Object> map);
    Integer delCollect(Map<String,Object> map);
    String isCollect(Map<String,Object> map);
}
