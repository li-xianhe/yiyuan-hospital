package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.CollectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-19 14:35
 */
@Service
public class CollectServiceImpl implements CollectService{

    @Autowired
    private CollectMapper collectMapper;

    @Override
    public String getCollects(Map<String, Object> map) {
        return JSON.toJSONString(collectMapper.getCollects(map));
    }

    @Override
    public Integer addCollect(Map<String, Object> map) {
        return collectMapper.addCollect(map);
    }

    @Override
    public Integer delCollect(Map<String, Object> map) {
        return collectMapper.delCollect(map);
    }

    @Override
    public String isCollect(Map<String, Object> map) {
        return JSON.toJSONString(collectMapper.isCollect(map));
    }
}
