package com.lixianhe.service;

import com.lixianhe.dao.RegisterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:47
 */
@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private RegisterMapper registerMapper;

    @Override
    public Integer register(Map<String, Object> map) {
        String status = (String) map.get("status");
        if("1".equals(status)){
            return registerMapper.doctorRegister(map);
        }else {
            return registerMapper.patientRegister(map);
        }
    }
}
