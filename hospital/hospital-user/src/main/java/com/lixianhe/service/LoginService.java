package com.lixianhe.service;


import com.lixianhe.pojo.Person;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:59
 */
public interface LoginService {
    Person Login(Map<String, Object> map);
    Integer wxLogin(Map<String,Object> map);
    String LoginTime(Map<String,Object> map);
}
