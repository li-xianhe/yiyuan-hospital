package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 17:13
 */
public interface DestoryService {
    Integer destoryUser(Map<String, Object> map);
}
