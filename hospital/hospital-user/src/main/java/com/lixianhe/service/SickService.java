package com.lixianhe.service;

import java.util.Map;

public interface SickService {
    String getSick(String p_id);
    Integer addSick(Map<String,Object> map);
    String modifyDefault(Map<String, Object> map);
    Integer modifySick(Map<String,Object> map);
    Integer delSick(Map<String,Object> map);
}
