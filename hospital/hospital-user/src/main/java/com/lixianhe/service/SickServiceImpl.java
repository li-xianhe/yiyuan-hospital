package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.SickMapper;
import com.lixianhe.pojo.Sick;
import com.lixianhe.utils.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SickServiceImpl implements SickService {

    @Autowired
    private SickMapper sickMapper;

    @Override
    public String getSick(String p_id) {
        return JSON.toJSONString(sickMapper.getSick(p_id));
    }

    @Override
    public Integer addSick(Map<String,Object> map) {
        System.out.println(map);
        Boolean isDefault = (Boolean)map.get("isDefault");
        if(isDefault) sickMapper.modifyDefault(map);
        return sickMapper.addSick(map);
    }

    @Override
    public String modifyDefault(Map<String, Object> map) {
        return String.valueOf(sickMapper.modifyDefault(map));
    }

    @Override
    public Integer modifySick(Map<String, Object> map) {
        boolean isDefault = (boolean)map.get("isDefault");
        if(isDefault) sickMapper.modifyDefault(map);
        return sickMapper.modifySick(map);
    }

    @Override
    public Integer delSick(Map<String,Object> map) {
        return sickMapper.delSick(map);
    }
}
