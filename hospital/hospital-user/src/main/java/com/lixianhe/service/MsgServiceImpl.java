package com.lixianhe.service;

import com.lixianhe.constant.StaticData;
import com.lixianhe.utils.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;
import java.util.concurrent.TimeUnit;

@Service
public class MsgServiceImpl implements MsgService{

    @Autowired
    private RedisTemplate redisTemplate;

    @Resource
    private JavaMailSender javaMailSender;

    @Override
    public String send_msg(String email) throws Exception {
        String key = "msg_" + email;
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String already_have = (String) valueOperations.get(key);
        if(already_have == null){
            String code = RandomUtils.getCode(6);
            SimpleMailMessage message = new SimpleMailMessage();
            message.setSubject("医院小程序邮箱验证码");
            message.setText("尊敬的用户您好!\n\n感谢您使用XX医院。\n\n尊敬的: "+email+"您的校验验证码为: "+code+",有效期2分钟，请不要把验证码信息泄露给其他人,如非本人请勿操作");
            message.setTo(email);
            message.setFrom(new InternetAddress(MimeUtility.encodeText("医院官方")+"<2774668116@qq.com>").toString());
            javaMailSender.send(message);
            valueOperations.set(key,code, StaticData.EMAIL_OUT_OF_TIME, TimeUnit.MINUTES);
            return code;
        }else{
            return already_have;
        }
    }
}
