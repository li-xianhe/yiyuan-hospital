package com.lixianhe.service;

import com.lixianhe.dao.PatientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PatientServiceImpl implements PatientService{

    @Autowired
    private PatientMapper patientMapper;

    @Override
    public String completePatient(Map<String, Object> map) {
        return patientMapper.completePatient(map);
    }

}
