package com.lixianhe.service;

import com.lixianhe.dao.ForgetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:36
 */
@Service
public class ForgetServiceImpl implements ForgetService{

    @Autowired
    private ForgetMapper forgetMapper;

    @Override
    public Integer ForgetPassword(Map<String, Object> map) {
        String status = (String) map.get("status");
        if("1".equals(status)){
            return forgetMapper.forgetPasswordDoctor(map);
        }else {
            return forgetMapper.forgetPasswordPatient(map);
        }
    }
}
