package com.lixianhe.service;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface PatientService {
    String completePatient(Map<String,Object> map);
}
