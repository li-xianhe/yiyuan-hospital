package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.WorkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkServiceImp implements WorkService{

    @Autowired
    private WorkMapper workMapper;

    @Override
    public String getWork() {
        return JSON.toJSONString(workMapper.getWork());
    }
}
