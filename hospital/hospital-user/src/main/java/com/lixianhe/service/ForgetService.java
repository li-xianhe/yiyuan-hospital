package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:36
 */
public interface ForgetService {
    Integer ForgetPassword(Map<String,Object> map);
}
