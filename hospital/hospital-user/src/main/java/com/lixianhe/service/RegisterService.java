package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:47
 */
public interface RegisterService {
   Integer register(Map<String, Object> map);
}
