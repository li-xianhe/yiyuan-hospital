package com.lixianhe.service;

import com.lixianhe.dao.ResetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 16:15
 */
@Service
public class ResetServiceImpl implements ResetService {

    @Autowired
    private ResetMapper resetMapper;

    @Override
    public Integer resetPassword(Map<String,Object> map) {
        String status = (String)map.get("status");
        if(status.equals("1")){
            return resetMapper.rsetPasswordDoctor(map);
        } else {
            System.out.println(map);
            return resetMapper.rsetPasswordPatient(map);
        }
    }
}
