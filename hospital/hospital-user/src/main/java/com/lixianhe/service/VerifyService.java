package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-02 21:58
 */
public interface VerifyService {

    /**
     * 校验用户名是否已经被注册
     * @param username 用户输入的用户名
     * @return 校验结果
     */
    Integer verifyUserNameRegister(String username);

    /**
     * 校验密码是否合法
     * @param password 用户输入的密码
     * @return 校验结果
     */
    Integer verifyPassWordIllegal(String password);

    /**
     * 校验两次输入的密码是否相同
     * @param password1 第一次输入的密码
     * @param password2 第二次输入的密码
     * @return 校验结果
     */
    Integer verifyPassWordSame(String password1, String password2);

    /**
     * 校验邮箱是否已经被注册
     * @param email 用户输入的邮箱
     * @return 校验结果
     */
    Integer verifyEmailRegister(String email);

    /**
     * 校验用户输入的验证码是否正确
     * @param email 用户输入的邮箱
     * @param code 用户输入的验证码
     * @return 校验结果
     */
    Integer verifyCode(String email, String code);

    /**
     * 用户校验密码是否存在
     * @param map 用户输入的密码
     * @return 校验结果
     */
    Integer verifyPasswordRegister(Map<String,Object> map);

    /**
     * 校验身份证是否合法
     * @param map 用户输入的身份证信息
     * @return 校验结果
     */
    Integer verifyLicence(Map<String,Object> map);

    /**
     * 校验手机号是否合法
     * @param map 用户输入的手机号信息
     * @return 校验结果
     */
    Integer verifyPhone(Map<String,Object> map);
}
