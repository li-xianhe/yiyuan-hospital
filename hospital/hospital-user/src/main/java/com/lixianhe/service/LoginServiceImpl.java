package com.lixianhe.service;

import com.lixianhe.dao.LoginMapper;
import com.lixianhe.pojo.Doctor;
import com.lixianhe.pojo.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:59
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Override
    public Person Login(Map<String, Object> map) {
        String status = (String) map.get("status");
        if("1".equals(status)){
            return loginMapper.loginDoctor(map);
        }else {
            return loginMapper.loginPatient(map);
        }
    }

    @Override
    public Integer wxLogin(Map<String, Object> map) {
        String status = (String) map.get("status");
        if("1".equals(status)){
            return loginMapper.wxloginDoctor(map);
        }else {
            return loginMapper.wxloginPatient(map);
        }
    }

    @Override
    public String LoginTime(Map<String, Object> map) {
        System.out.println(loginMapper.LoginTime(map));
        return loginMapper.LoginTime(map);
    }
}
