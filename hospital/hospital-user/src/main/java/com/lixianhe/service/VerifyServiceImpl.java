package com.lixianhe.service;

import com.lixianhe.constant.StaticData;
import com.lixianhe.dao.VerifyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-02 21:58
 */
@Service
public class VerifyServiceImpl implements VerifyService{

    @Autowired
    private VerifyMapper verifyMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public Integer verifyUserNameRegister(String username) {
        Integer patientCount = verifyMapper.verifyUserNameRegisterPatient(username);
        Integer doctorCount = verifyMapper.verifyUserNameRegisterDoctor(username);
        return patientCount + doctorCount;
    }

    @Override
    public Integer verifyEmailRegister(String email) {
        if (!email.matches(StaticData.LEGAL_EMAIL)) {
            return -1;
        }
        int patientCount = verifyMapper.verifyEmailRegisterPatient(email);
        int doctorCount = verifyMapper.verifyEmailRegisterDoctor(email);

        /*
          1 邮箱已经被注册
          0 邮箱未被注册且合法
          -1 邮箱不合法
         */
        return patientCount + doctorCount;
    }

    @Override
    public Integer verifyPassWordIllegal(String password) {
        if (password.length() < StaticData.MIN_PASSWORD_LENGTH) {
            return -2;
        }
        int result = 0;
        // 是否含有大写字母
        boolean upper = false;
        // 是否含有小写字母
        boolean low = false;
        // 是否含有数字
        boolean num = false;
        for (int i = 0; i < password.length(); i++) {
            if ((password.charAt(i) >= 'A' && password.charAt(i) <= 'Z')) {
                upper = true;
            } else if (password.charAt(i) >= 'a' && password.charAt(i) <= 'z') {
                low = true;
            } else if (password.charAt(i) >= '0' && password.charAt(i) <= '9') {
                num = true;
            } else if((password.charAt(i) <= 47 && password.charAt(i) >= 33)) {
            } else if(password.charAt(i) <= 64 && password.charAt(i) >= 58){
            }else if(password.charAt(i) <= 91 && password.charAt(i) >= 96) {
            } else {
                return -1;
            }
        }
        if(num && low && upper){
            result = 1;
        }

        /*
          -2 密码小于6位
          -1 代表密码含有非法字符
          0 代码密码不是由大写字母、小写字母、数字组成
          1 密码合法
         */
        return result;
    }

    @Override
    public Integer verifyPassWordSame(String password1, String password2) {
        int result = 0;
        if (password1.equals(password2)) {
            result = 1;
        }

        /*
          1 代表密码相同
          0 密码不相同
         */
        return result;
    }

    @Override
    public Integer verifyCode(String email, String code) {
        int result = 1;
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String msgKey = "msg_" + email;
        Object value = valueOperations.get(msgKey);
        if(value == null){
            result = -1;
        }else if(!code.equals(value)) {
            result = 0;
        }
        // 如果验证码正确，则删除从redis
        if(result == 1){
            redisTemplate.delete(msgKey);
        }
        /*
          1 验证码正确
          0 验证码错误
          -1 验证码过期
         */
        return result;
    }

    @Override
    public Integer verifyPasswordRegister(Map<String,Object> map) {
        Object status = map.get("status");
        if (status.equals("1")){
            return verifyMapper.verifyPasswordRegisterDoctor(map);
        }else {
            return verifyMapper.verifyPasswordRegisterPatient(map);
        }
    }

    /**
     * 校验身份证是否合法
     * @param map 用户输入的身份证信息
     * @return 校验结果
     */
    @Override
    public Integer verifyLicence(Map<String, Object> map) {
        String licence = (String) map.get("licence");
        if(licence.matches(StaticData.LEGAL_LICENCE)) return 1;
        else return 0;
    }

    /**
     * 校验手机号是否合法
     * @param map 用户输入的手机号信息
     * @return 校验结果
     */
    @Override
    public Integer verifyPhone(Map<String, Object> map) {
        String phone = (String) map.get("phone");
        if(phone.matches(StaticData.LEGAL_PHONE)) return 1;
        else return 0;
    }
}
