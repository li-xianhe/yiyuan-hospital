package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-05-06 9:19
 */
public interface LoadImgService {
    int addUserImage(Map<String,Object> map);
}
