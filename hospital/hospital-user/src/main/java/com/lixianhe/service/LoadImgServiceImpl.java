package com.lixianhe.service;

import com.lixianhe.dao.LoadImgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-05-06 9:19
 */
@Service
public class LoadImgServiceImpl implements LoadImgService{

    @Autowired
    private LoadImgMapper loadImgMapper;

    @Override
    public int addUserImage(Map<String,Object> map) {
        return loadImgMapper.addUserImage(map);
    }
}
