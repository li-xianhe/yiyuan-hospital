package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.DoctorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DoctorServiceImpl implements DoctorService{

    @Autowired
    private DoctorMapper doctorMapper;

    @Override
    public String doctorById(int d_id) {
        return JSON.toJSONString(doctorMapper.doctorByID(d_id));
    }

    @Override
    public String doctorByWorkId(int w_id) {
        return JSON.toJSONString(doctorMapper.doctorByWorkID(w_id));
    }

    @Override
    public String prefectDoctor(Map<String, Object> map) {
        return String.valueOf(doctorMapper.prefectDoctor(map));
    }

}
