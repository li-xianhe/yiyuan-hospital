package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:49
 */
@Mapper
public interface RegisterMapper {
    /**
     * 病人用户名密码注册
     * @param map 用户用户名密码注册信息
     * @return 注册是否成功
     */
    Integer patientRegister(Map<String, Object> map);

    /**
     * 医生用户名密码注册
     * @param map 医生用户名密码信息
     * @return 注册结果
     */
    Integer doctorRegister(Map<String,Object> map);
}
