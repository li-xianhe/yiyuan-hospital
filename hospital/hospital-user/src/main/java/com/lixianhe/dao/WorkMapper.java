package com.lixianhe.dao;

import com.lixianhe.pojo.Work;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Mapper
public interface WorkMapper {
    /**
     * 获取所有部门
     * @return 所有部门对象的List集合
     */
    List<Work> getWork();
}
