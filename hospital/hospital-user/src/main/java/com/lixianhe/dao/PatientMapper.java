package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Mapper
public interface PatientMapper {
    /**
     * 完善用户信息
     * @param map 前端发送的用户信息
     * @return 完善结果
     */
    String completePatient(Map<String,Object> map);
}
