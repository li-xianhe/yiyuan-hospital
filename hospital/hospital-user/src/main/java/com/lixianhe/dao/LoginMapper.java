package com.lixianhe.dao;

import com.lixianhe.pojo.Doctor;
import com.lixianhe.pojo.Patient;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 19:00
 */
@Mapper
public interface LoginMapper {
    /**
     * 病人登录
     * @param map 病人登录信息
     * @return 返回病人对象
     */
    Patient loginPatient(Map<String,Object> map);

    /**
     * 医生登录
     * @param map 医生登录信息
     * @return 返回医生对象
     */
    Doctor loginDoctor(Map<String,Object> map);

    /**
     * 病人微信登录
     * @param map 病人微信登录信息
     * @return 返回病人对象
     */
    Integer wxloginPatient(Map<String,Object> map);

    /**
     * 医生微信登录
     * @param map 医生微信登录信息
     * @return 返回医生对象
     */
    Integer wxloginDoctor(Map<String,Object> map);

    String LoginTime(Map<String,Object> map);
}
