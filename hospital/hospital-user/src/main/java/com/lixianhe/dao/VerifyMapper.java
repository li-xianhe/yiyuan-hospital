package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-02 11:50
 */
@Mapper
public interface VerifyMapper {

    /**
     * 查询数据库中这个用户名是否已经病人被注册
     * @param username 被校验病人的用户名
     * @return 查询结果
     */
    @Select("select count(*) from hospital.patient where p_openid = #{username}")
    Integer verifyUserNameRegisterPatient(@Param("username") String username);

    /**
     * 查询数据库中这个邮箱是否已经病人被注册
     * @param email 被校验病人的邮箱
     * @return 查询结果
     */
    @Select("select count(*) from hospital.patient where p_email = #{email}")
    Integer verifyEmailRegisterPatient(@Param("email") String email);

    /**
     * 查询数据库中这个用户名是否已经医生被注册
     * @param username 被校验医生的用户名
     * @return 查询结果
     */
    @Select("select count(*) from hospital.doctor where doc_open_id = #{username}")
    Integer verifyUserNameRegisterDoctor(@Param("username") String username);

    /**
     * 查询数据库中这个邮箱是否已经医生被注册
     * @param email 被校验医生的邮箱
     * @return 查询结果
     */
    @Select("select count(*) from hospital.doctor where doc_email = #{email}")
    Integer verifyEmailRegisterDoctor(@Param("email") String email);

    /**
     * 校验病人的密码是否已经存在
     * @param map 病人的信息
     * @return 校验结果
     */
    Integer verifyPasswordRegisterPatient(Map<String,Object> map);

    /**
     * 校验医生的密码是否已经存在
     * @param map 医生的信息
     * @return 校验结果
     */
    Integer verifyPasswordRegisterDoctor(Map<String,Object> map);
}
