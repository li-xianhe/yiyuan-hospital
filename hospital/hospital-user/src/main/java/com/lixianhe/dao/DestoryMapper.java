package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 17:15
 */
@Mapper
public interface DestoryMapper {

    /**
     * 销毁用户
     * @param map 用户信息
     * @return 注销结果
     */
    Integer destoryPatient(Map<String,Object> map);

    /**
     * 注销医生账户
     * @param map 医生信息
     * @return 注销结果
     */
    Integer destoryDoctor(Map<String, Object> map);
}
