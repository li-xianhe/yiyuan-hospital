package com.lixianhe.dao;

import com.lixianhe.pojo.Doctor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Mapper
public interface DoctorMapper {

    /**
     * 根据医生id获取医生对象
     * @param d_id 医生id
     * @return 医生对象
     */
    Doctor doctorByID(@Param("id")int d_id);

    /**
     * 根据部门id获取，该部门的所有医生
     * @param w_id 部门id
     * @return 所有医生的对象
     */
    List<Doctor> doctorByWorkID(@Param("id") int w_id);

    int prefectDoctor(Map<String,Object> map);
}
