package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;
import com.lixianhe.pojo.*;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-19 14:28
 */
@Mapper
public interface CollectMapper {
    /**
     * 获取用户的收藏
     * @param map 用户信息
     * @return 该用户的收藏信息
     */
    List<Collect> getCollects(Map<String,Object> map);

    /**
     * 添加用户的收藏
     * @param map 用户的信息
     * @return 添加结果
     */
    Integer addCollect(Map<String,Object> map);

    /**
     * 删除用户收藏
     * @param map 用户信息
     * @return 删除结果
     */
    Integer delCollect(Map<String,Object> map);

    Collect isCollect(Map<String,Object> map);
}
