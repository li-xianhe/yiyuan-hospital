package com.lixianhe.dao;

import com.lixianhe.pojo.Sick;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Mapper
public interface SickMapper {
    /**
     * 根据openid查询微信用户的所有就诊人
     * @return 所有就诊人对象的list
     */
    List<Sick> getSick(@Param("openid") String p_id);

    /**
     * 微信用户添加就诊人
     * @param map 就诊人信息
     * @return 影响数据库的条数字
     */
    Integer addSick(Map<String,Object> map);

    /**
     * 修改就诊人的信息
     * @param map 就诊人信息
     * @return 影响数据库的条数
     */
    Integer modifySick(Map<String,Object> map);

    /**
     * 切换默认就诊人
     * @param map 要添加的就诊人信息
     * @return 修改数据库的条数
     */
    Integer modifyDefault(Map<String, Object> map);

    /**
     * 删除就诊人
     * @param map 要删除就诊人的信息
     * @return 删除结果
     */
    Integer delSick(Map<String,Object> map);
}
