package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-05-06 9:20
 */
@Mapper
public interface LoadImgMapper {
    @Update("update patient set p_image = #{url} where p_openid = #{openid}")
    int addUserImage(Map<String,Object> map);
}
