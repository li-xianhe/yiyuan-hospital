package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 19:11
 */
@Mapper
public interface ResetMapper {

    /**
     * 医生修改密码
     * @param map 医生输入信息
     * @return 修改结果
     */
    Integer rsetPasswordDoctor(Map<String,Object> map);


    /**
     * 病人修改密码
     * @param map 病人输入信息
     * @return 修改结果
     */
    Integer rsetPasswordPatient(Map<String,Object> map);
}
