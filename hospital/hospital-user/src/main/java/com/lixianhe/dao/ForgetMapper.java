package com.lixianhe.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-09 18:38
 */
@Mapper
public interface ForgetMapper {
    Integer forgetPasswordPatient(Map<String,Object> map);
    Integer forgetPasswordDoctor(Map<String,Object> map);
}
