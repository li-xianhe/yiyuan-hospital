package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.VerifyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 李显赫
 * @Date 2022-04-06 10:05
 */
@RestController
@RequestMapping("/verify")
public class VerifyController {

    @Autowired
    private VerifyServiceImpl verifyService;

    /**
     * 校验用户名是否已经被注册
     * @param jsonObject 用户输入的用户名
     * @return 查询结果
     */
    @PostMapping("/username/register")
    public String verifyUserNameRegister(@RequestBody JSONObject jsonObject){
        String username = (String) jsonObject.get("username");
        return String.valueOf(verifyService.verifyUserNameRegister(username));
    }

    /**
     * 校验密码是否合法
     * @param jsonObject 用户输入的密码
     * @return 校验结果
     */
    @PostMapping("/password/legal")
    public String verifyPasswordIllegal(@RequestBody JSONObject jsonObject) {
        String password = (String) jsonObject.get("password");
        return String.valueOf(verifyService.verifyPassWordIllegal(password));
    }

    /**
     * 检验两次输入密码是否相同
     * @param jsonObject 用户第一次和第二次输入的密码
     * @return 校验结果
     */
    @PostMapping("/password/same")
    public String verifyPasswordSame(@RequestBody JSONObject jsonObject) {
        String password1 = (String) jsonObject.get("password1");
        String password2 = (String) jsonObject.get("password2");
        return String.valueOf(verifyService.verifyPassWordSame(password1,password2));
    }

    /**
     * 校验邮箱是否已经被注册
     * @param jsonObject 用户输入的邮箱
     * @return 查询结果
     */
    @PostMapping("/email/register")
    public String verifyEmailRegister(@RequestBody JSONObject jsonObject){
        String email = (String) jsonObject.get("email");
        return String.valueOf(verifyService.verifyEmailRegister(email));
    }

    /**
     * 校验邮件验证码是否正确
     * @param jsonObject 输入的邮箱和验证码
     * @return 校验结果
     */
    @PostMapping("/email/code")
    public String verifyCode(@RequestBody JSONObject jsonObject) {
        String code = (String) jsonObject.get("code");
        String email = (String) jsonObject.get("email");
        return String.valueOf(verifyService.verifyCode(email,code));
    }

    /**
     * 校验密码是否已经被注册
     * @param jsonObject 用户输入的密码和openid等信息
     * @return 校验结果
     */
    @PostMapping("/password/register")
    public String verifyPasswordRegister(@RequestBody JSONObject jsonObject){
        System.out.println(jsonObject);
        return String.valueOf(verifyService.verifyPasswordRegister(jsonObject));
    }

    /**
     * 校验身份证是否合法
     * @param jsonObject 用户输入的身份证信息
     * @return 校验结果
     */
    @PostMapping("/licence")
    public String verifyLicence(@RequestBody JSONObject jsonObject){
        return String.valueOf(verifyService.verifyLicence(jsonObject));
    }

    /**
     * 校验手机号是否合法
     * @param jsonObject 用户输入的手机号信息
     * @return 校验结果
     */
    @PostMapping("/phone")
    public String verifyPhone(@RequestBody JSONObject jsonObject){
        return String.valueOf(verifyService.verifyPhone(jsonObject));
    }
}
