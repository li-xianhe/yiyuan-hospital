package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.CollectServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import com.lixianhe.utils.RandomUtils;
import com.lixianhe.utils.RandomValidateCodeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-19 14:40
 */
@RestController
@Slf4j
@RequestMapping("/collects")
public class CollectController {
    @Autowired
    private CollectServiceImpl collectService;

    @PostMapping("/_all")
    public String getCollect(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        String openid = null;
        if (jsonObject.size() != 1 || (openid = (String) jsonObject.get("openid")) == null) {
            log.warn("请求失败openid = {}", openid);
            return ForbidUtils.ForbidRequest(response);
        }
        return collectService.getCollects(jsonObject);
    }

    @PostMapping
    public String addCollect(@RequestBody JSONObject jsonObject) {
        return String.valueOf(collectService.addCollect(jsonObject));
    }

    @DeleteMapping
    public String delCollect(@RequestBody JSONObject jsonObject) {
        return String.valueOf(collectService.delCollect(jsonObject));
    }

    @PostMapping("/is")
    public String isCollect(@RequestBody JSONObject jsonObject) {
        return collectService.isCollect(jsonObject);
    }
}
