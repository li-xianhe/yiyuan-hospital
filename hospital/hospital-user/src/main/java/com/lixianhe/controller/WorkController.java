package com.lixianhe.controller;

import com.lixianhe.service.WorkServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@RestController
@RequestMapping("/works")
public class WorkController {

    @Autowired
    private WorkServiceImp workServiceImp;

    @GetMapping
    public String getAllWork(){
        return workServiceImp.getWork();
    }
}
