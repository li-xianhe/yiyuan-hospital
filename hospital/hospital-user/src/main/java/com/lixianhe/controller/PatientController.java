package com.lixianhe.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.PatientServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:50
 */
@RestController
public class PatientController {

    @Autowired
    private PatientServiceImpl patientService;

    @PutMapping("/complete")
    public String completePatient(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        System.out.println(jsonObject);
        Map map = JSON.parseObject((String) jsonObject.get("data"), Map.class);

        if (map.size() != 9) {
            ForbidUtils.ForbidRequest(response);
            return null;
        }

        Integer age = null;

        try {
            age = Integer.parseInt((String) map.get("age"));
        } catch (Exception e) {
            ForbidUtils.ForbidRequest(response);
            return null;
        }

        if (age < 0 || age > 200) {
            ForbidUtils.ForbidRequest(response);
            return null;
        }

        return patientService.completePatient(map);
    }



}
