package com.lixianhe.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lixianhe.pojo.Health;
import com.lixianhe.utils.QrCodeUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
/**
 * @author 李显赫
 * @Date 2022-05-05 11:19
 */
@RestController
public class BbsController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @GetMapping("/aaa")
    public String Test() throws Exception {

        Resource resource = new ClassPathResource("static/1.txt");
        //获1.txt的取相对路径
        String path = resource.getFile().getPath();

        System.out.println(path);

//        String text = "https://blog.csdn.net/weixin_43763430";
//        String logoPath = "D:\\qrCode\\logo.jpg";
//        String destPath = "D:\\qrCode\\csdn.jpg";
//        QrCodeUtils.encode(text,logoPath,destPath,true);
        return "ok";
    }

    @PostMapping("/binCode")
    @ApiOperation(value = "获取二维码")
    public void qrCodeTest(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws Exception {
        Integer index = (Integer) jsonObject.get("id");
        String text = "http://liuhuiying.natapp1.cc/health/health"+index+".html";
        String logoPath = "D:\\qrCode\\logo.jpg";
        //String destPath = "D:\\qrCode\\csdn.jpg";
        QrCodeUtils.encode(text,logoPath,response.getOutputStream(),true);
    }

    @GetMapping("/title")
    public String getAllTitle(){
        return JSON.toJSONString(mongoTemplate.findAll(Health.class));
    }
}
