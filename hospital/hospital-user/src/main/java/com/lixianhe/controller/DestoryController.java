package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.DestoryServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-09 16:59
 */
@RestController
public class DestoryController {
    @Autowired
    private DestoryServiceImpl destoryService;

    /**
     * 注销用户
     * @param jsonObject 要注销的用户信息
     * @return 注销结果
     */
    @PostMapping("/destory")
    private Integer destory(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        if(jsonObject.size() != 3){
            ForbidUtils.ForbidRequest(response);
            return null;
        }
        return destoryService.destoryUser(jsonObject);
    }

}
