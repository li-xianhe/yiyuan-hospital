package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.pojo.Person;
import com.lixianhe.service.LoginServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-09 13:42
 */
@RestController
public class LoginController {

    @Autowired
    private LoginServiceImpl loginService;

    /**
     * 用户名密码登录
     * @param jsonObject 登录信息
     * @return 该对象的所有信息
     */
    @PostMapping("/login")
    private Person login(@RequestBody JSONObject jsonObject) {
        return loginService.Login(jsonObject);
    }

    @PostMapping("/wxlogin")
    private String wxlogin(@RequestBody JSONObject jsonObject) {
        return String.valueOf(loginService.wxLogin(jsonObject));
    }

    @PostMapping("/login/time")
    public String wxLoginTime(@RequestBody JSONObject jsonObject){
        return loginService.LoginTime(jsonObject);
    }

}
