package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.ForgetServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-09 13:50
 */
@RestController
public class ForgetController {

    @Autowired
    private ForgetServiceImpl forgetService;

    /**
     * 找回密码
     * @param jsonObject 用户输入的找回信息
     * @return 修改密码的结果
     */
    @PostMapping("/forget")
    private Integer forget(@RequestBody JSONObject jsonObject) {
        return forgetService.ForgetPassword(jsonObject);
    }
}
