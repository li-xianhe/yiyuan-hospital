package com.lixianhe.controller;

import com.lixianhe.service.LoadImgServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * @author 李显赫
 * @Date 2022-05-05 23:15
 */
@RestController
public class UploadController {

    @Autowired
    private LoadImgServiceImpl loadImgService;

    @RequestMapping(value = "/load", method = RequestMethod.POST, consumes ="multipart/form-data")
    public String getFile(@RequestParam("file") MultipartFile file,@RequestParam("openid") String openid) throws IOException {

        System.out.println("收到请求");
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        String fileName=file.getOriginalFilename();
        //String fileName = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        System.out.println("文件名为："+fileName);
        assert fileName != null;
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        System.out.println("文件的后缀名为：" + suffixName);
        String newfileName= UUID.randomUUID().toString()
                .replaceAll("-", "")
                .toUpperCase()
                +System.currentTimeMillis()
                +suffixName;
        System.out.println("新的名称->"+newfileName);

        ClassPathResource resource = new ClassPathResource("static");

        String path = resource.getFile().getAbsolutePath();

        File dest = new File(path+ "\\" + newfileName);
        System.out.println(dest);
        try {
            file.transferTo(dest);
            System.out.println("上传成功");
            String ip = "http://10.3.21.140:8086/hospital/static/";
            Map<String,Object> map = new HashMap<>(2);
            map.put("url",ip+newfileName);
            map.put("openid",openid);
            System.out.println(loadImgService.addUserImage(map));
            return newfileName;
        } catch (IOException e) {
            System.out.println("上传失败");
            System.out.println(e.getMessage());
        }
        return "上传失败！";
    }
}
