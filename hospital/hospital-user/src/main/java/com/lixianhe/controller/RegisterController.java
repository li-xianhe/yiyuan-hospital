package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.RegisterServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-09 13:49
 */
@RestController
public class RegisterController {
    @Autowired
    private RegisterServiceImpl registerService;

    @PostMapping("/register")
    private Integer register(@RequestBody JSONObject jsonObject) {
        return registerService.register(jsonObject);
    }
}
