package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.SpringBootUser;
import com.lixianhe.utils.RandomValidateCodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/login")
public class PicVerifyAction {

    private final static Logger logger = LoggerFactory.getLogger(PicVerifyAction.class);

    /**
     * 生成验证码
     */
    @PostMapping(value = "/getVerify")
    public void getVerify(@RequestBody JSONObject jsonObject ,HttpServletResponse response) {

        String uuid = (String) jsonObject.get("uuid");

        try {

            //设置相应类型,告诉浏览器输出的内容为图片
            response.setContentType("image/jpeg");

            //设置响应头信息，告诉浏览器不要缓存此内容
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expire", 0);

            RandomValidateCodeUtil randomValidateCode = new RandomValidateCodeUtil();

            Cookie cookie = new Cookie("uuid",uuid);

            cookie.setMaxAge(60 * 2);

            response.addCookie(cookie);

            randomValidateCode.getRandomCode(uuid, response);//输出验证码图片方法

        } catch (Exception e) {
            logger.error("获取验证码失败>>>>   ", e);
        }
    }

    /**
     * 校验验证码
     */
    @RequestMapping(value = "/checkVerify", method = RequestMethod.POST, headers = "Accept=application/json")
    public int checkVerify(@RequestBody JSONObject jsonObject) {

        int status = 0;

        String uuid = (String) jsonObject.get("uuid");

        String code = (String) jsonObject.get("code");

        try {

            ValueOperations<String, String> stringOperations = SpringBootUser.redisTemplateDefault.opsForValue();
            String res = stringOperations.get(uuid);
            if (res != null && res.equals(code)) {
                status = 1;
            }

        } catch (Exception e) {
            logger.error("验证码校验失败", e);
            return status;
        }
        /*
             1 成功
             0 失败
         */
        return status;
    }
}

