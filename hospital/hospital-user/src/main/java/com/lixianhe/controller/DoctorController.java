package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.DoctorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 李显赫
 * @Date 2022-04-06 11:08
 */
@RestController
public class DoctorController {

    @Autowired
    private DoctorServiceImpl doctorService;

    @GetMapping("/doctors/{id}")
    public String doctorByID(@PathVariable("id") int d_id) {
        return doctorService.doctorById(d_id);
    }

    @RequestMapping("/doctors_by_work/{id}")
    public String doctorByWorkID(@PathVariable("id") int w_id) {
        return doctorService.doctorByWorkId(w_id);
    }

    @PutMapping("/prefect/doctor")
    public String prefectDoctor(@RequestBody JSONObject jsonObject){
        System.out.println(jsonObject);
        return doctorService.prefectDoctor(jsonObject);
    }


}
