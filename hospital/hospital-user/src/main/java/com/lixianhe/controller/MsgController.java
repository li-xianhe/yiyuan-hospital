package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.MsgServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@RestController
@RequestMapping("/msg")
public class MsgController {

    @Autowired
    private MsgServiceImpl msgService;

    /**
     * 接收用户发送的目标email
     * @param jsonObject 目标邮箱的json对象
     * @return 6位数的验证码
     */
    @PostMapping("/sendEmail")
    public String send_email(@RequestBody JSONObject jsonObject) throws Exception {
        String email = (String) jsonObject.get("email");
        return msgService.send_msg(email);
    }
}
