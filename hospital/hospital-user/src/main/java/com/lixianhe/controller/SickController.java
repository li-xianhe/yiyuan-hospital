package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.constant.StaticData;
import com.lixianhe.service.SickServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@RestController
@Slf4j
@RequestMapping("/sicks")
public class SickController {

    @Autowired
    private SickServiceImpl sickService;

    @GetMapping("/{pid}")
    public String getSickByPid(@PathVariable("pid") String id) {
        return sickService.getSick(id);
    }

    @DeleteMapping
    public String delSick(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        try {
            if (jsonObject.size() != 2 || !((String) jsonObject.get("licence")).matches(StaticData.LEGAL_LICENCE)) {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(sickService.delSick(jsonObject));
    }

    @PostMapping
    public String addSick(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        try{
            String licence = (String) jsonObject.get("licence");
            String phone = (String) jsonObject.get("phone");
            if (jsonObject.size() != 6 || !licence.matches(StaticData.LEGAL_LICENCE) ||
                    !phone.matches(StaticData.LEGAL_PHONE)) {
                throw new RuntimeException();
            }
        }catch (RuntimeException e){
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(sickService.addSick(jsonObject));
    }

    @PutMapping
    public String modifySick(@RequestBody JSONObject jsonObject) {
        return String.valueOf(sickService.modifySick(jsonObject));
    }

    @PutMapping("/default")
    public String modifyDefault(@RequestBody JSONObject jsonObject) {
        return sickService.modifyDefault(jsonObject);
    }
}
