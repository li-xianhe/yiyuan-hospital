package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.ResetServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-09 16:13
 */
@RestController
public class ResetController {

    @Autowired
    private ResetServiceImpl resetService;

    @PostMapping("/reset")
    Object resetPassword(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        if(jsonObject.size() != 4 ){
            return ForbidUtils.ForbidRequest(response);
        }
        return resetService.resetPassword(jsonObject);
    }

}
