package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-28 18:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonMessage<Object> {
    public boolean Success;
    public String Code;
    public String Message;
}
