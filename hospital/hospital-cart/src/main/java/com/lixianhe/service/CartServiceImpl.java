package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.CartMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-17 14:31
 */
@Service
public class CartServiceImpl implements CartService{

    @Autowired
    private CartMapper cartMapper;

    @Override
    public String getCarts(Map<String, Object> map) {
        return JSON.toJSONString(cartMapper.getCarts(map));
    }

    @Override
    public Integer addCart(Map<String, Object> map) {
        return cartMapper.addCart(map);
    }

    @Override
    public Integer delCart(Map<String, Object> map) {
        return cartMapper.delCart(map);
    }

    @Override
    public Integer modCount(Map<String, Object> map) {
        return cartMapper.modCount(map);
    }
}
