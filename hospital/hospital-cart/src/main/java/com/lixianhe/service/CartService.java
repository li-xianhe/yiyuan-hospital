package com.lixianhe.service;

import com.lixianhe.pojo.Cart;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-17 14:31
 */
public interface CartService {
    String getCarts(Map<String,Object> map);
    Integer addCart(Map<String,Object> map);
    Integer delCart(Map<String,Object> map);
    Integer modCount(Map<String,Object> map);
}
