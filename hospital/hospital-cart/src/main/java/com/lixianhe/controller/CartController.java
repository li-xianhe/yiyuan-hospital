package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.pojo.Cart;
import com.lixianhe.service.CartServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.recycler.Recycler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 李显赫
 * @Date 2022-04-17 14:33
 */
@RestController
@Slf4j
@RequestMapping("/carts")
public class CartController {

    @Autowired
    private CartServiceImpl cartService;

    @PostMapping("/_all")
    public String getCarts(@RequestBody JSONObject jsonObject, HttpServletResponse response) {

        try {
            if (jsonObject.size() != 1) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return cartService.getCarts(jsonObject);
    }

    @PostMapping
    public String addCart(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 3) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(cartService.addCart(jsonObject));
    }

    @DeleteMapping
    public String delCart(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 2) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(cartService.delCart(jsonObject));
    }

    @PutMapping
    public String modCount(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 3) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(cartService.modCount(jsonObject));
    }
}
