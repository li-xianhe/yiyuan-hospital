package com.lixianhe.dao;

import com.lixianhe.pojo.Cart;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-17 14:19
 */
@Mapper
public interface CartMapper {
    List<Cart> getCarts(Map<String,Object> map);
    Integer addCart(Map<String,Object> map);
    Integer delCart(Map<String,Object> map);
    Integer modCount(Map<String,Object> map);
}
