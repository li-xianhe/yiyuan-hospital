package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李显赫
 * @Date 2022-04-24 16:20
 */
@SpringBootApplication
public class SpringBootCart {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootCart.class, args);
    }
}
