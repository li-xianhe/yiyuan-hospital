package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.IndexServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@RestController
public class IndexController {

    @Autowired
    private IndexServiceImpl indexService;

    /**
     * 获取首页的所有轮播图
     * @return 所有轮播图对象
     */
    @GetMapping("/index")
    public String getIndex(){
        return indexService.getIndex();
    }

    /**
     * 根据用户得到的分数获取检测结果
     * @param jsonObject 用户分数
     * @return 返回的结果信息
     */
    @PostMapping("/standards")
    public String getStandard(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {
        int score = Integer.parseInt((String)jsonObject.get("score"));
        if(jsonObject.size() != 1 || !(score <= 100 && score >= 0)){
            ForbidUtils.ForbidRequest(response);
            return null;
        }
        return indexService.getStandard(jsonObject);
    }

    /**
     * 获取测试题
     * @return 所有测试题结果
     */
    @GetMapping("/tests")
    public String getTests(){
        return indexService.getTests();
    }
}
