package com.lixianhe.dao;

import com.lixianhe.pojo.Index;
import com.lixianhe.pojo.Standard;
import com.lixianhe.pojo.Test;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Mapper
public interface IndexMapper {
    /**
     * 获取首页的轮播图和公告信息
     * @return 所有轮播图和公告信息对象的List
     */
    @Select("select * from hospital.index")
    List<Index> getIndex();

    /**
     * 根据成绩查询它的结果
     * @param map 分数
     * @return 结果
     */
    @Select("select r_desc,r_text,r_min,r_max from standard where #{score} between r_min and r_max")
    Standard getStandard(Map<String,Object> map);

    /**
     * 获取所有的测试题
     * @return 所有测试题的List集合
     */
    @Select("select test_name,test_score from test")
    List<Test> getTests();
}
