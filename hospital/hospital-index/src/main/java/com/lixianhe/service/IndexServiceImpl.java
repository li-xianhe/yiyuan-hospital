package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.IndexMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class IndexServiceImpl implements IndexService{
    @Autowired
    private IndexMapper indexMapper;

    @Override
    public String getIndex() {
        return JSON.toJSONString(indexMapper.getIndex());
    }

    @Override
    public String getStandard(Map<String, Object> map) {
        return JSON.toJSONString(indexMapper.getStandard(map));
    }

    @Override
    public String getTests() {
        return JSON.toJSONString(indexMapper.getTests());
    }
}
