package com.lixianhe.service;

import java.util.Map;

public interface IndexService {
    String getIndex();
    String getStandard(Map<String,Object> map);
    String getTests();
}
