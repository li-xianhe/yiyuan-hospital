package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李显赫
 * @Date 2022-04-24 10:32
 */
@SpringBootApplication
public class SpringBootIndex {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootIndex.class, args);
    }
}
