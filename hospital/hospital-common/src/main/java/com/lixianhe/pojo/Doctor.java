package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Doctor extends Person{
    private String doc_open_id;
    private String doc_name;
    private String doc_gender;
    private Integer doc_age;
    private String doc_img;
    private String doc_email;
    private String doc_desc;
    private String doc_best;
    private Boolean doc_is_delete;
    private Integer w_id;
    private String w_name;
    private Integer d_id;
    private String d_name;
    private String doc_password;
}
