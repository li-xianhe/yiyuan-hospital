package com.lixianhe.pojo;

import lombok.*;

import java.util.Date;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Patient extends Person {
    private String p_openid;
    private String p_password;
    private String p_name;
    private Integer p_gender;
    private String p_image;
    private String p_email;
    private Integer p_age;
    private String p_province;
    private String p_city;
    private String p_area;
    private String p_ethnic;
    private String p_is_delete;
    private Date create_time;
}
