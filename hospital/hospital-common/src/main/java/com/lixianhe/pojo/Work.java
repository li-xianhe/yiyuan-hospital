package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Work {
    private Integer w_id;
    private String w_name;
}
