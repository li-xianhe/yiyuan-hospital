package com.lixianhe.pojo;
import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-22 16:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Label {
    private Integer l_id;
    private String l_name;
    private String l_pid;
    private Boolean l_is_delete;
}
