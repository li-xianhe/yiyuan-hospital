package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-19 14:26
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Collect {
    private Drug drug;
    private Boolean c_is_delete;
}
