package com.lixianhe.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李显赫
 * @Date 2022-04-15 15:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    private String a_id;
    private String a_pid;
    private String a_name;
    private String a_province;
    private String a_city;
    private String a_area;
    private String a_detail;
    private Boolean a_is_default;
    private Boolean a_is_delete;
    private String a_phone;
    private Label label;
}
