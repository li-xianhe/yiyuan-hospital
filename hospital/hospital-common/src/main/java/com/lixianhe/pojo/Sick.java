package com.lixianhe.pojo;

import lombok.*;

import java.io.Serializable;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sick {
    private Integer s_id;
    private String s_pid;
    private String s_licence;
    private String s_gender;
    private String s_phone;
    private String s_name;
    private Boolean s_default;
}
