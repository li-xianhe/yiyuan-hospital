package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-22 23:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Test {
    private String test_name;
    private String test_score;
}
