package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-05-05 18:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Health {
    private int _id;
    private String title;
}
