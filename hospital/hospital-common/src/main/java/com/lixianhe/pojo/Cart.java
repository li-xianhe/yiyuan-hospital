package com.lixianhe.pojo;
import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-17 14:17
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    private Drug drug;
    private Integer c_count;
}
