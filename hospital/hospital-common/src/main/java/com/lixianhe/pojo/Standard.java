package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-22 23:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Standard {
    private Integer r_min;
    private Integer r_max;
    private String r_desc;
    private String r_text;
}
