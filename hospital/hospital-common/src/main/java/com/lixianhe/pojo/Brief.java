package com.lixianhe.pojo;

import lombok.*;

import java.util.Date;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Brief {
    private Integer b_id;
    private String b_text;
    private String b_phone;
    private Date b_create_date_time;
    private String b_map;
    private String b_address;
    private String b_image;
}
