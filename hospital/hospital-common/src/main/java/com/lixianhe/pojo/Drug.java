package com.lixianhe.pojo;

import lombok.*;

import java.util.Date;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Drug {
    private Integer d_id;
    private String d_name;
    private Double d_price;
    private String d_desc;
    private String d_img;
    private Integer d_count;
    private Double d_old_price;
    private Date create_time;
}
