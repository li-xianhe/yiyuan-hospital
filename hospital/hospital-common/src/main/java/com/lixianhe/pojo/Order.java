package com.lixianhe.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.*;

import java.util.Date;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private String o_id;
    @JSONField(serialzeFeatures= SerializerFeature.DisableCircularReferenceDetect)
    private Sick sick;
    private String o_time;
    private Boolean o_is_h;
}
