package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Index {
    private String i_image;
    private String i_announcement;
}
