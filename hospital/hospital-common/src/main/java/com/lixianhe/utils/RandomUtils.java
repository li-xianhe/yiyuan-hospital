package com.lixianhe.utils;

import java.util.Random;
import java.util.UUID;

/**
 * @author 李显赫
 * @Date 2022-04-06 11:58
 */
public class RandomUtils {
    public static String getCode(long length){
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }
}
