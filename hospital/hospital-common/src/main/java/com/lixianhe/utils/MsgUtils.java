package com.lixianhe.utils;


import org.springframework.mail.SimpleMailMessage;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
public class MsgUtils {
    /**
     * 生成6位数的验证码
     * @return 验证码
     */
    public static String getCode(){
       return RandomUtils.getCode(6);
    }

    /**
     * 发送邮箱验证码的信息
     * @param email 目标邮箱
     * @param code 验证码
     * @return 邮箱信息对象
     */
    public static SimpleMailMessage getMessage(String email, String code) throws Exception {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("医院小程序邮箱验证码");
        message.setText("尊敬的用户您好!\n\n感谢您使用XX医院。\n\n尊敬的: "+email+"您的校验验证码为: "+code+",有效期5分钟，请不要把验证码信息泄露给其他人,如非本人请勿操作");
        message.setTo(email);
        message.setFrom(new InternetAddress(MimeUtility.encodeText("医院官方")+"<2774668116@qq.com>").toString());
        return message;
    }
}
