package com.lixianhe.utils;

import com.lixianhe.constant.StaticData;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
public class ForbidUtils {
    /**
     * 请求参数的校验没通过
     * @param response 响应对戏
     */
    public static String ForbidRequest(HttpServletResponse response)  {
        response.setStatus(StaticData.Forbidden);
        try {
            response.getWriter().append("request error");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
