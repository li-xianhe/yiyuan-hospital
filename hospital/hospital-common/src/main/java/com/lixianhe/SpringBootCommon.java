package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李显赫
 * @Date 2022-04-23 23:16
 */
@SpringBootApplication
public class SpringBootCommon {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootCommon.class, args);
    }
}
