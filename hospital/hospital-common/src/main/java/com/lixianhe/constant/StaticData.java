package com.lixianhe.constant;

/**
 * @author 李显赫
 * @Date 2022-04-01 14:14
 */
@SuppressWarnings("all")
public class StaticData {

    /**
     * 药品分页时每页的药品数量
     */
    public static final int PAGE_SIZE = 20;

    /**
     * 排序的zSet集合的名字
     */
    public static final String DRUG_SORT = "drug";

    /**
     * 药品缓存的时间 五天
     */
    public static final int DRUG_CACHE_TIME = 1;

    /**
     * 求参数错误，禁止请求的响应状态码
     */
    public static final int Forbidden = 403;

    /**
     * 校验邮箱是否合法的正则表达式
     */
    public static final String LEGAL_EMAIL = "^(?:\\w+\\.?)\\w+@(?:\\w+\\.)+\\w+$";

    /**
     * 校验身份证号是否合法非正则表达式
     */
    public static  final  String LEGAL_LICENCE = "^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$|^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X)$";

    /**
     * 校验手机号是否合法正则表达式
     */
    public static final  String LEGAL_PHONE = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$";

    /**
     * 延迟队列名称
     */
    public static final String DELAYED_QUEUE = "Queue";

    /**
     * 延迟交换机名称
     */
    public static final String DELAY_EXCHANGE = "Exchange";

    /**
     * routing key
     */
    public static final String DELAYED_ROUTING_KEY = "OrderDelayedQueueRoutingKey";

    /**
     * 默认订单延迟时间  一天
     */
    public static final int DEFAULT_DELAY_TIME = 1000 * 60 * 60 * 24;
//    public static final int DEFAULT_DELAY_TIME = 1000 * 60;

    /**
     * elasticsearch的索引名称
     */
    public static final String ELASTIC_SEARCH_INDEX = "hospital";

    /**
     * elasticsearch的字段名
     */
    public static final String ELASTIC_SEARCH_NAME = "name";

    /**
     * elasticsearch的字段名
     */
    public static final String ELASTIC_SEARCH_ID = "id";

    /**
     * MySQL加密秘钥
     */
    public static final String MYSQL_ENCRYPT_KEY = "miyaokey";

    /**
     * 邮箱验证码过期时间2分钟
     */
    public static final long EMAIL_OUT_OF_TIME = 2L;

    /**
     * 密码的最小位数
     */
    public static final int MIN_PASSWORD_LENGTH = 6;

    /**
     * 验证码的位数
     */
    public static final int EMAIL_CODE_LENGTH = 6;

    /**
     * 搜索历史最大条数
     */
    public static final int SEARCH_HISTORY_MAX_COUNT = 12;
}