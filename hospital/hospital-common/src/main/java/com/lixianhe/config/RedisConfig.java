package com.lixianhe.config;


import io.lettuce.core.resource.DefaultClientResources;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.ObjectUtils;

import java.time.Duration;

/**
 * @author 李显赫
 * @Date 2022-04-14 10:21
 */
@Configuration
public class RedisConfig {

    @Value("${redis.database.default}")
    private int defaultDataBase;

    @Value("${redis.database.cacheDrug}")
    private int drugCacheDataBase;

    @Value("${redis.database.sort}")
    private int sortDataBase;

    @Value("${redis.database.history}")
    private int historyDataBase;

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    @Value("${spring.redis.password}")
    private String password;

    @Value("${spring.redis.lettuce.pool.max-active}")
    private int maxActive;

    @Value("${spring.redis.lettuce.pool.max-wait}")
    private int maxWait;

    @Value("${spring.redis.lettuce.pool.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.lettuce.pool.min-idle}")
    private int minIdle;

    @Value("${spring.redis.timeout}")
    private long timeout;

    @Bean
    public GenericObjectPoolConfig poolConfig() {
        // 配置redis连接池
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxTotal(maxActive);
        poolConfig.setMaxIdle(maxIdle);
        poolConfig.setMinIdle(minIdle);
        poolConfig.setMaxWaitMillis(maxWait);
        return poolConfig;
    }

    @Bean("defaultClientResources")
    public  DefaultClientResources getDefaultClientResources(){
        return DefaultClientResources.create();
    }

    @Bean(name="redisTemplateDefault")
    public StringRedisTemplate redisTemplateDefault(GenericObjectPoolConfig poolConfig,
                    @Qualifier("defaultClientResources") DefaultClientResources defaultClientResources) {
        return getStringRedisTemplate(defaultDataBase, poolConfig,defaultClientResources);
    }

    @Bean(name="redisTemplateSort")
    public StringRedisTemplate redisTemplateSort(GenericObjectPoolConfig poolConfig,
                                                 @Qualifier("defaultClientResources") DefaultClientResources defaultClientResources) {
        return getStringRedisTemplate(sortDataBase, poolConfig,defaultClientResources);
    }

    @Bean(name="redisTemplateDrugCache")
    public StringRedisTemplate redisTemplateCacheDrug(GenericObjectPoolConfig poolConfig,
                                             @Qualifier("defaultClientResources") DefaultClientResources defaultClientResources) {
        return getStringRedisTemplate(drugCacheDataBase, poolConfig,defaultClientResources);
    }

    @Bean(name="redisTemplateHistory")
    public StringRedisTemplate redisTemplateHistory(GenericObjectPoolConfig poolConfig,
                                                      @Qualifier("defaultClientResources") DefaultClientResources defaultClientResources) {
        return getStringRedisTemplate(historyDataBase, poolConfig,defaultClientResources);
    }

    private StringRedisTemplate getStringRedisTemplate(int database, GenericObjectPoolConfig poolConfig, DefaultClientResources defaultClientResources) {
        // 构建工厂对象
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
        config.setHostName(host);
        config.setPort(port);
        if (!ObjectUtils.isEmpty(password)) {
            RedisPassword redisPassword = RedisPassword.of(password);
            config.setPassword(redisPassword);
        }
        LettucePoolingClientConfiguration clientConfig = LettucePoolingClientConfiguration.builder()
                .commandTimeout(Duration.ofSeconds(timeout))
                .poolConfig(poolConfig)
                .clientResources(defaultClientResources)
                .build();
        LettuceConnectionFactory factory = new LettuceConnectionFactory(config, clientConfig);
        // 设置使用的redis数据库
        factory.setDatabase(database);
        // 重新初始化工厂
        factory.afterPropertiesSet();
        return new StringRedisTemplate(factory);
    }
}
