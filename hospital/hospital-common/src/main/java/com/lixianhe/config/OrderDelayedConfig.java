package com.lixianhe.config;

import com.lixianhe.constant.StaticData;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Configuration
public class OrderDelayedConfig {

    @Bean("Queue")
    public Queue delayedQueue() {
        return new Queue(StaticData.DELAYED_QUEUE);
    }
    /**
     * 自定义交换机 我们在这里定义的是一个延迟交换机
     */
    @Bean("Exchange")
    public CustomExchange delayedExchange() {
        Map<String, Object> args = new HashMap<>();
        //自定义交换机的类型
        args.put("x-delayed-type", "direct");
        /*
         * 1.交换机名称
         * 2.交换机的类型
         * 3.是否持久化
         * 4.是否自动删除
         * 5.其他参数
         */
        return new CustomExchange(StaticData.DELAY_EXCHANGE, "x-delayed-message", true,
                                false, args);
    }

    @Bean("Binding")
    public Binding bindingDelayedQueue(@Qualifier("Queue") Queue queue,
                                       @Qualifier("Exchange") CustomExchange delayedExchange) {
        return BindingBuilder.bind(queue).to(delayedExchange).with(StaticData.DELAYED_ROUTING_KEY).noargs();
    }

}
