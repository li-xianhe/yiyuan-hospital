package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.AddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

/**
 * @author 李显赫
 * @Date 2022-04-15 16:26
 */
@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressMapper addressMapper;

    @Override
    public String getAddresses(Map<String, Object> map) {
        String s = JSON.toJSONString(addressMapper.getAddresses(map));
        System.out.println(s);
        return s;
    }

    @Override
    public String addAddress(Map<String, Object> map) {
        System.out.println(map);
        String id = UUID.randomUUID().toString().replace("-","");
        boolean isDefault = (boolean) map.get("isDefault");
        map.put("id", id);
        addressMapper.addAddress(map);
        if(isDefault) addressMapper.modDefault(map);
        return id;
    }

    @Override
    public Integer delAddress(Map<String, Object> map) {
        return addressMapper.delAddress(map);
    }

    @Override
    public Integer modAddress(Map<String, Object> map) {
        if((Boolean) map.get("isDefault"))  modDefault(map);


        return addressMapper.modAddress(map);
    }

    @Override
    public Integer modDefault(Map<String, Object> map) {
        return addressMapper.modDefault(map);
    }
}
