package com.lixianhe.service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-22 16:58
 */
public interface LabelService {
    String getLabel(Map<String,Object> map);
    Integer addLabel(Map<String,Object> map);
    Integer delLabel(Map<String,Object> map);
}
