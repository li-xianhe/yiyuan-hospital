package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.LabelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-22 16:59
 */
@Service
public class LabelServiceImpl implements LabelService{

    @Autowired
    private LabelMapper labelMapper;

    @Override
    public String getLabel(Map<String, Object> map) {
        return JSON.toJSONString(labelMapper.getLabel(map));
    }

    @Override
    public Integer addLabel(Map<String, Object> map) {
        return labelMapper.addLabel(map);
    }

    @Override
    public Integer delLabel(Map<String, Object> map) {
        return labelMapper.delLabel(map);
    }
}
