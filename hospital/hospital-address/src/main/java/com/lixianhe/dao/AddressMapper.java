package com.lixianhe.dao;

import com.lixianhe.pojo.Address;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-15 15:11
 */
@Mapper
public interface AddressMapper {
    /**
     * 获取用户的所有收货地址
     * @param map 用户openid
     * @return 用户所有地址对象的List
     */
    List<Address> getAddresses(Map<String,Object> map);

    /**
     * 添加收货地址
     * @param map 用户填入的信息
     * @return 添加结果
     */
    Integer addAddress(Map<String,Object> map);

    /**
     * 删除收货地址
     * @param map 用户的openid和收货地址id
     * @return 删除结果
     */
    Integer delAddress(Map<String,Object> map);

    /**
     * 修改用户的地址信息
     * @param map 用户修改的信息
     * @return 修改结果
     */
    Integer modAddress(Map<String,Object> map);

    /**
     * 修改默认收货地址
     * @param map 修改信息
     * @return 修改结果
     */
    Integer modDefault(Map<String,Object> map);
}
