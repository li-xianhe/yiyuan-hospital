package com.lixianhe.dao;

import com.lixianhe.pojo.Label;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-22 16:44
 */
@Mapper
public interface LabelMapper {
    List<Label> getLabel(Map<String,Object> map);
    Integer addLabel(Map<String,Object> map);
    Integer delLabel(Map<String,Object> map);
}
