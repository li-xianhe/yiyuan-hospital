package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.LabelServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 李显赫
 * @Date 2022-04-22 17:01
 */
@RestController
@Slf4j
@RequestMapping("/labels")
public class LabelController {

    @Autowired
    private LabelServiceImpl labelService;

    @PostMapping("/_all")
    public String getLabel(@RequestBody JSONObject jsonObject, HttpServletResponse response){
        try {
            if (jsonObject.size() != 1) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return labelService.getLabel(jsonObject);
    }

    @PostMapping
    public String addLabel(@RequestBody JSONObject jsonObject,HttpServletResponse response){
        try {
            if (jsonObject.size() != 2) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return  String.valueOf(labelService.addLabel(jsonObject));
    }

    @DeleteMapping
    public String delLabel(@RequestBody JSONObject jsonObject,HttpServletResponse response){
        try {
            if (jsonObject.size() != 2) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        System.out.println(jsonObject);
        return  String.valueOf(labelService.delLabel(jsonObject));
    }

}
