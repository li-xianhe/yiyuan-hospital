package com.lixianhe.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixianhe.service.AddressServiceImpl;
import com.lixianhe.utils.ForbidUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 李显赫
 * @Date 2022-04-15 16:30
 */
@RestController
@Slf4j
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressServiceImpl addressService;

    @PostMapping("/_all")
    public String getAddresses(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 1) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return addressService.getAddresses(jsonObject);
    }

    @PostMapping
    public String addAddress(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 9) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return addressService.addAddress(jsonObject);
    }

    @DeleteMapping
    public String delAddress(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 2) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(addressService.delAddress(jsonObject));
    }

    @PutMapping
    public String modAddress(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 10) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(addressService.modAddress(jsonObject));
    }

    @PutMapping("/default")
    public String modDefault(@RequestBody JSONObject jsonObject, HttpServletResponse response) {
        try {
            if (jsonObject.size() != 2) throw new RuntimeException();
        } catch (RuntimeException e) {
            log.error("传入参数错误");
            return ForbidUtils.ForbidRequest(response);
        }
        return String.valueOf(addressService.modDefault(jsonObject));
    }
}
