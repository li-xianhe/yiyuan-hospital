package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李显赫
 * @Date 2022-04-24 0:12
 */
@SpringBootApplication
public class SpringBootAddress {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootAddress.class, args);
    }
}
