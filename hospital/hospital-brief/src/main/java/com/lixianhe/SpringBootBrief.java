package com.lixianhe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 李显赫
 * @Date 2022-04-24 15:14
 */
@SpringBootApplication
public class SpringBootBrief {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootBrief.class, args);
    }
}
