package com.lixianhe.dao;

import com.lixianhe.pojo.Brief;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@Mapper
public interface BriefMapper {
    /**
     * 获取医院简介信息
     * @return 返回简介信息对象
     */
    @Select("select * from brief b")
    Brief getText();
}
