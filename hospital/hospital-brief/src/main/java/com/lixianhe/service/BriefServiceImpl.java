package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.BriefMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 李显赫
 * @Date 2022-04-06 11:58
 */
@Service
public class BriefServiceImpl implements BriefService {

    @Resource
    private BriefMapper briefMapper;

    @Override
    public String getText() {
        return JSON.toJSONString(briefMapper.getText());
    }
}
