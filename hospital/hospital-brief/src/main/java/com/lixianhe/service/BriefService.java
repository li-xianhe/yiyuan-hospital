package com.lixianhe.service;

/**
 * @author 李显赫
 * @Date 2022-04-06 11:58
 */
public interface BriefService {
    /**
     * 获取医院简介信息
     * @return 返回简介信息对象的JSON
     */
    String getText();
}
