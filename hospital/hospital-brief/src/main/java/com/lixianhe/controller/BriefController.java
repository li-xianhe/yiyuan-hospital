package com.lixianhe.controller;

import com.lixianhe.service.BriefServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 李显赫
 * @Date 2022-04-06 21:58
 */
@RestController
@RequestMapping("/briefs")
public class BriefController {

    @Resource
    private BriefServiceImpl briefService;

    /**
     * 获取医院简介信息
     * @return 医院简介信息对象的JSON
     */
    @GetMapping
    public String getText(){
        return briefService.getText();
    }
}
