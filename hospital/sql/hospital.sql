/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 26/08/2022 23:52:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `a_id` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `a_pid` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `a_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `a_province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `a_city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `a_area` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `a_is_delete` tinyint(1) NULL DEFAULT 0,
  `a_detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `a_is_default` tinyint(1) NULL DEFAULT NULL,
  `a_phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `a_label` int NULL DEFAULT NULL,
  PRIMARY KEY (`a_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('0c4250c4780046f88e626acd4314097c', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '李四', '辽宁省 ', '大连市', '金州区', 1, '解决好干活干活干活', 0, '18241123333', 3);
INSERT INTO `address` VALUES ('351f17ba1a3b46a7a1ebe3061489de78', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '张三', '辽宁省 ', '大连市', '金州区', 1, '就会比较会比较好好吧好吧', 0, '18241121122', 1);
INSERT INTO `address` VALUES ('3e64b24ac95f49908aa9c3798e15f811', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '李四', '吉林省 ', '长春市', '南关区', 1, '华景街道新苑小区5单元5栋408', 0, '13242233112', 1);
INSERT INTO `address` VALUES ('64859b36338742a8b6ffe596889895ef', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '张三', '辽宁省 ', '大连市', '金州区', 0, '大连民族大学金石滩校区', 1, '18241122111', 3);
INSERT INTO `address` VALUES ('68eb190bbc17432b9c301e7437cc35b4', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '张三', '辽宁省 ', '大连市', '金州区', 0, '调查都不吃的不错的123', 0, '18241123112', 10);

-- ----------------------------
-- Table structure for brief
-- ----------------------------
DROP TABLE IF EXISTS `brief`;
CREATE TABLE `brief`  (
  `b_text` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `b_phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0',
  `b_create_date_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `b_map` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `b_address` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `b_image` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of brief
-- ----------------------------
INSERT INTO `brief` VALUES ('  &ensp;&ensp;意源医院始建于2022年。并于该年成为国家三级甲等医院。是国家级爱婴医院、全国百姓放心示范医院、全国院务公开示范医院；省级文明单位标兵；是国家高级胸痛中心（标准版）、国家高级卒中中心建设单位；是国家卫生部国际急救中心网络医院；是我省四所国家高等医学院校的教学医院；承担着我市医疗质量“七个质控中心”的检查与指导任务。\r\n  &ensp;&ensp;意源医院是大连市城镇职工、城乡居民及大连林业局、管理局职工医保定点医疗单位。担负着大连周边地区的医疗、教学、科研、预防保健、急诊急救和康复等工作。\r\n  &ensp;&ensp;意源医院下设医院本部和11家分院。医院本部建筑面积6.8万平方米，开放床位1300张，年门急诊量60万余人次，年出院病人5.2万余人次，手术例数1.2万余例。\r\n  &ensp;&ensp;意源医院坚持“科教兴院、人才立院、质量强院、安全建院、品牌办院、文化塑院”的发展战略，在当地医疗行业中始终处于领航地位，人才一流，技术一流，设备一流，成为大连百姓看病就医首选医院。\r\n  &ensp;&ensp;拥有国家级、省级各专业委员会主任委员、副主任委员、常务委员、委员50名；医疗卫生技术人员1115人，其中高级医疗卫生技术人员164人，副高级医疗卫生技术人员243人，中级医疗卫生技术人员237人，医学博士3人，医学硕士70人。\r\n  医院在医疗设备上持续投入，最新引进国际超高端美国GE Pioneer 3.0T核磁共振、美国GE Revolution 512层CT、国际最新型荷兰飞利浦C型臂血管造影机（DSA）、瑞典医科达最新型直线加速器，大型医疗设备全球先进，国内、省内领先，设备总值达2.5亿元。\r\n  \\r\\n&ensp;&ensp;拥有国际先进超声诊断设备17台、日本富士能电子胃镜15条、电子肠镜8条、德国费森尤斯血液透析机36台、血滤机7台。外科手术内窥镜设备先进、齐全，拥有椎间孔镜、腹腔镜、关节镜、宫腔镜、膀胱镜、支撑喉镜、鼻窦内镜等，为外科精准诊断和微创手术的发展提供了设备支撑。\r\n  &ensp;&ensp;另外，医院还拥有东三省第二台加拿大莱博瑞蓝牙无线空气测压尿动力检查仪、惠康U200体外冲击碎石机，为目前国内最先进的尿动力学检查和碎石设备。拥有全市首台、国内最先进的光学相干断层扫描仪（OCT）；拥有全自动生化分析仪、全自动化学发光分析仪、全自动化学发光免疫分析仪、全自动血液分析仪等大型检验设备；拥有德国高端牙科品牌3D大视野口腔椎形束CBCT口腔机、DR数字化摄影机3台、乳腺钼靶机；拥有CV—170日本奥林巴斯带有窄谱成像功能的电子气管镜、美国飞利浦伟康A-40型无创呼吸机；；同时还拥有立体动态干扰电治疗仪、德国菲兹曼吞咽言语诊治仪等康复治疗先进设备，推动了医院各项诊疗技术的开展。\r\n  &ensp;&ensp;医院设有34个临床科室，10个医技科室。在“创建全省地市级先进医院”发展目标的指引下，各医疗专科不断引进高新技术，紧跟国内、省内先进技术发展。尤以心血管内科、骨科、神经内科、神经外科、消化内科、肿瘤内科为重点，率先在全市开展了心血管疾病介入诊疗手术、冠脉血流储备分数（FFR）、血管内超声（IVUS）等国内尖端技术；开展了椎间孔镜、关节镜下肩、膝、髋关节及踝关节疾病微创手术、颈椎人工间盘植入术、全膝关节置换术、肩关节置换术、椎体成形术等骨科先进医疗技术；开展了急性脑梗死的动脉接触溶栓术、球囊扩张术和支架取栓术、椎动脉狭窄和锁骨下动脉狭窄支架术、动脉瘤栓塞术等多项脑血管病介入治疗新技术；开展了鼻胆管引流、内支架、十二指肠乳头切开治疗、食道静脉曲张破裂出血的急诊内镜下硬化剂注射、内镜下治疗消化道出血、息肉切除、食道静脉曲张套扎治疗、内镜下消化道早癌治疗、内镜下取异物及胃石症碎石治疗等多项技术，年胃肠镜诊疗14000余例；开展了肿瘤的介入治疗、放射治疗、化疗等全方位、多靶点肿瘤治疗技术。\r\n  &ensp;&ensp;另外，以腹腔镜、宫腔镜、胸腔镜、支撑喉镜、超声乳化等为主体的微创外科手术技术也迅速发展，普外科、胸外科、泌尿外科、妇产科、耳鼻喉口腔科、眼科在诊治疑难重症，开展高难手术上取得了突破性进展。\r\n  &ensp;&ensp;呼吸内科支气管镜诊疗技术、糖尿病内分泌科急危重症与糖尿病的防控治疗、儿科多发病及重症诊疗、血液透析室急、慢性肾衰透析滤过治疗、透析导管置入技术都体现了强大的综合实力。重症监护病房（ICU）、急诊科在抢救急危重症病人，保障重症病人生命安全上发挥了重要的作用。\r\n  &ensp;&ensp;在新的历史时期，在新的机制体制下，意源医院“不忘初心，牢记使命”，坚持“办好医院、造福百姓、惠及员工、回馈社会”的办院宗旨，以满足人民群众健康需求为己任，加速建设现代化新门诊住院综合楼，改善就医环境，不断引进高新医疗设备，加强专科建设，提高医疗质量，提升科学管理水平，努力为全市人民提供优质的医疗服务，为大连百姓生命健康保驾护航！\r\n', '13131313131', '2022-05-04 18:32:31', '辽宁省大连市金州区', '乘车（公交）：1路、2路、3路、4路、8路、路、10路、11路、14路、17路、19路、20路、26路', 'http://10.3.21.140:8084/hospital/static/brief/brief.jpg');

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart`  (
  `c_pid` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `c_did` int NOT NULL,
  `c_count` int NOT NULL,
  `c_is_delete` tinyint NOT NULL DEFAULT 0,
  PRIMARY KEY (`c_pid`, `c_did`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('', 6, 1, 0);
INSERT INTO `cart` VALUES ('', 150, 1, 0);
INSERT INTO `cart` VALUES ('', 173, 1, 0);
INSERT INTO `cart` VALUES ('', 217, 1, 0);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1, 0, 1);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 3, 3, 0);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 6, 0, 1);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 14, 0, 1);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 31, 0, 1);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 150, 2, 0);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 160, 0, 1);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 165, 3, 0);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 188, 1, 0);
INSERT INTO `cart` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 212, 0, 1);

-- ----------------------------
-- Table structure for classify
-- ----------------------------
DROP TABLE IF EXISTS `classify`;
CREATE TABLE `classify`  (
  `c_id` tinyint NOT NULL,
  `c_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `c_is_delete` tinyint NOT NULL DEFAULT 0,
  PRIMARY KEY (`c_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of classify
-- ----------------------------
INSERT INTO `classify` VALUES (1, '男科用药', 0);
INSERT INTO `classify` VALUES (2, '心血管科', 0);
INSERT INTO `classify` VALUES (3, '风湿骨病', 0);
INSERT INTO `classify` VALUES (4, '消化系统', 0);
INSERT INTO `classify` VALUES (5, '呼吸系统', 0);
INSERT INTO `classify` VALUES (6, '皮肤科药', 0);
INSERT INTO `classify` VALUES (7, '滋润保健', 0);
INSERT INTO `classify` VALUES (8, '糖尿病科', 0);
INSERT INTO `classify` VALUES (9, '内分泌科', 0);
INSERT INTO `classify` VALUES (10, '日常用药', 0);
INSERT INTO `classify` VALUES (11, '女性用药', 0);
INSERT INTO `classify` VALUES (12, '儿科用药', 0);
INSERT INTO `classify` VALUES (13, '泌尿科药', 0);
INSERT INTO `classify` VALUES (14, '肝胆药科', 0);
INSERT INTO `classify` VALUES (15, '神经类药', 0);
INSERT INTO `classify` VALUES (16, '抗肿瘤药', 0);
INSERT INTO `classify` VALUES (17, '清热解毒', 0);

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect`  (
  `c_pid` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `c_did` int NOT NULL,
  `c_is_delete` tinyint NOT NULL DEFAULT 0,
  PRIMARY KEY (`c_pid`, `c_did`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1, 1);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 2, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 3, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 6, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 14, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 24, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 133, 1);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 150, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 165, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 173, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 188, 0);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 212, 1);
INSERT INTO `collect` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', 217, 1);

-- ----------------------------
-- Table structure for doctor
-- ----------------------------
DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor`  (
  `doc_open_id` varchar(28) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `doc_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `doc_gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `doc_age` tinyint NULL DEFAULT NULL,
  `doc_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `doc_desc` varchar(510) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `doc_best` varchar(510) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `doc_work_id` int UNSIGNED NULL DEFAULT NULL,
  `doc_is_delete` tinyint NULL DEFAULT 0,
  `doc_duty` int UNSIGNED NULL DEFAULT NULL,
  `doc_password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `doc_email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`doc_open_id`) USING BTREE,
  UNIQUE INDEX `index_email`(`doc_email`) USING BTREE,
  INDEX `foreign_work`(`doc_work_id`) USING BTREE,
  INDEX `foregin_duty`(`doc_duty`) USING BTREE,
  CONSTRAINT `foregin_duty` FOREIGN KEY (`doc_duty`) REFERENCES `duty` (`d_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foreign_work` FOREIGN KEY (`doc_work_id`) REFERENCES `work` (`w_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of doctor
-- ----------------------------
INSERT INTO `doctor` VALUES ('1', '封福新', '男', 32, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院耳鼻咽喉口腔科主任、主任医师。出诊时间：每周一、周三、周五、周日（轮）上午出诊。学术任职：黑龙江省医师协会委员、黑龙江省耳鼻咽喉专业委员。', '在本地区率先开展了过敏性鼻炎微创手术治疗、高选择性翼管神经分支切断术、筛前神经切断术及扁桃体炎、腺样体肥大微创等离子手术。擅长诊治耳鼻咽喉科常见、多发病，规范化治疗睡眠呼吸暂停低通气综合症（鼾症），包括行为矫治、正压通气、手术治疗等；擅长耳显微外科，如鼓膜成形术、鼓室成形术等治疗鼓膜穿孔、各种慢性中耳炎；显微镜下精细切除声带息肉、声带小结、会厌囊肿及其他喉良性肿瘤，做到微创治疗；擅长喉癌及下咽癌手术治疗，如水平、垂直半喉切除术、全喉切除术、喉次全切除术及各种喉下咽切除后修补术、颈淋巴清扫术；鼻内镜下各种鼻炎鼻窦炎手术，在眩晕疾病（如:耳石症）的诊治积累了丰富的临床经验。', 28, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('10', '张宁宁', '男', 29, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院妇产科主任、副主任医师。毕业于齐齐哈尔医学院妇产专业。曾到哈尔滨医科大学第二附属医院进修妇产科宫腹腔镜。', '擅长治疗妇科常见病、多发病及内分泌疾病的诊断和治疗。对妇科宫腔镜、腹腔镜、阴式等微创手术具有丰富的临床经验，擅长妇科恶性肿瘤的规范化治疗、各种急重症和难产产程的处理，熟知高危产科，遗传咨询及产前诊断的相关知识。', 52, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('1111', '王爱金', '男', 45, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', NULL, NULL, 15, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('12', '温泉江', '男', 39, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院门诊肛肠科主任医师。出诊时间:每周一至周五上午。 学术任职：中国医师协会肛肠分会委员，黑龙江省中医药协会肛肠专业委员会委员。', '擅长肛肠良性疾病的治疗，如:肛周脓肿、肛瘘、肛裂、直肠前突、混合痔、便秘、慢性肠炎等治疗，尤其是使用PH(自动痔疮套扎吻合器)微创治疗混合痔方面，填补了全市肛肠治疗空白。多次到中日友好医院及北京市肛肠医院进修学习，并于2018年成为北京中医药大学肛肠专科联盟单位。', 44, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('13', '孟凡刚', '男', 49, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院骨科一病房主任、主任医师，毕业于白求恩医科大学临床医学专业。出诊时间：每周四上午出诊。 学术任职：东北地区及黑龙江省手显外科学会委员、黑龙江省脊髓脊柱损伤协会理事、中国伤残医学杂志特邀编委、中国截瘫研究会黑龙江省分会第三届理事会理事、黑龙江省康复医学会人工关节专业委员会委员。', '擅长骨关节外科疾病治疗，具有系统扎实的理论基础和丰富的临床诊疗及手术经验。熟练开展创伤、肿瘤、结核、炎症及退变等疾病治疗；髋、膝等关节置换及翻修等人工关节外科疾病治疗；半月板损伤、前后交叉韧带损伤、关节滑膜炎症，关节内骨折、关节游离体等关节镜外科疾病治疗；颈腰椎外伤、退变、结核、肿瘤及四肢骨折及股骨颈骨折、股骨头坏死等疾病治疗。', 47, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('14', '王普亮', '男', 32, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院骨科副主任医师。出诊时间：每周一、周五全天。', '从事骨科临床工作20余年，擅长各种骨科疾病诊断及治疗。擅长腰间盘突出、颈椎病、膝关节疾病的诊断及治疗，熟练掌握创伤骨科的门诊手术治疗，对脊柱及关节疾病的诊疗有丰富的经验。\n', 40, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('15', '吴凤宇', '男', 41, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院骨科四病房主任，副主任医师、医学硕士。出诊时间：每周三上午。', '从事骨外科临床工作近20年，在骨科疾病和创伤的诊断与治疗方面积累了较为丰富的临床经验，尤其擅长于脊柱、关节疾病和运动创伤诊治，特别是股骨头坏死保髋治疗。熟练掌握四肢骨折的微创治疗、膝关节镜技术，能够完成颈椎前路植骨融合内固定术、复杂股骨转子间骨折内固定术、腰椎间盘突出症髓核摘除椎板减压内固定术、胸腰椎骨折拖尾切开复位椎弓根钉系统内固定，椎管狭窄症、椎间盘突出症、腰椎滑脱症等高难度手术。', 59, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('16', '魏颂武', '男', 48, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院骨科四病房副主任、主任医师。出诊时间：周三全天、周四下午、周六上午。', NULL, 44, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('17', '李东平', '男', 36, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', NULL, NULL, 45, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('18', '陶文武', '男', 40, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院骨二科副主任、副主任医师。哈尔滨医科大学临床医疗专业。出诊时间：每周三上午出诊', '擅长骨科常见病、多发病临床诊断和治疗，从事骨科工作20余年，专业于关节外科，专长肩、膝关节疾病的诊断和微创治疗，擅长脊柱疾病的诊断和治疗，四肢关节创伤、骨盆骨折、关节置换手术，治疗颈椎及腰椎间盘突出疾病等。', 26, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('19', '张万福', '男', 34, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院骨科三病房副主任，主任医师。白求恩医科大学医疗系。出诊时间：每周四上午出诊。', '手显微外科、颈椎间盘突出症、四肢、脊柱骨折、感染、肿瘤、血管神经损伤等疾病的诊疗。', 59, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('2', '刘爽', '男', 43, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院耳鼻喉科副主任、主任医师。出诊时间:每周二、四、周日上午出诊。', '率先开展CO2激光喉微创手术，擅长耳、鼻、喉科 、头颈外科疾病的诊断及治疗，擅长鼻内镜鼻窦手术，、鼻中隔手术、鼻息肉手术、过敏性鼻炎的规范治疗。擅长腺样体及扁桃体常规及微创手术、声带息肉、结节、肿瘤CO2激光及等离子微创手术、喉癌切除喉功能重建手术、颈淋巴清扫手术、中耳炎鼓室成型手术、耳内镜鼓膜修补手术、眩晕耳鸣、突聋、分泌型中耳炎等疾病的诊断治疗。', 24, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('20', '严永吉', '男', 37, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院骨科五病房主任、主任医师。出诊时间：每周五上午出诊。哈尔滨医科大学。学术任职：黑龙江省中西医结合学会骨关节病分会副会长、黑龙江省医学会骨科分会委员、黑龙江省医学会运动医疗分会委员、黑龙江省医学会小儿矫形外科学分会常务委员、黑龙江省康复医学会关节镜与关节修复康复专业委员会委员、黑龙江省康复医学会人工关节专业委员会委员、黑龙江省骨科专科联盟区域专家团成员、黑龙江省医学会骨科分会脊柱微创学组成员、双鸭山运动医学副主委、双鸭山骨科委员。', '擅长四肢及脊柱骨折的开放手术治疗及微创手术治疗，先天性髋脱位、骨关节炎、股骨头坏死等人工髋关节置换术，以及保守保髋治疗；骨性关节炎风湿性关节炎的膝关节人工关节置换术，以及保守保膝治疗；颈椎病、颈、腰椎间盘突出症间盘摘除关节融合术，小开窗间盘拆除术，颈椎间盘多阶段突出颈椎后路间盘摘除椎弓钉内固定术，以及颈椎病及腰椎间盘突出症的保守治疗。老年性腰椎压缩骨折单椎体成形术；膝关节损伤的半月板修整、缝合，前后交叉韧带损伤的修复和重建、以及在关节镜监视下行胫骨平台骨折，关节镜下治疗髌骨脱位的微创治疗。在肩关节镜下治疗肩袖损伤，修补等治疗。小儿四肢长骨骨折微创闭合复位弹性髓内针内固定术，小儿干骺端骨折的不切口闭合复位克氏针内固定术，以及小儿四肢骨折的保守治疗手法复位石膏外固定术，小儿多指（趾）畸形的矫形术等', 23, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('21', '李震', '男', 41, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院骨五科副主任、副主任医师。出诊时间：每周四上午出诊。', '从事骨科工作十余年， 擅长骨科常见病、多发病临床诊断和治疗。尤其是关节外科、专长肩、膝关节疾病的诊断及微创治疗，擅长脊柱疾病的诊断及治疗、四肢关节创伤、骨盆骨折、关节置换手术、微创伤射频消融技术治疗颈椎及腰椎间盘突出疾病的治疗等。', 2, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('22', '侍金梅', '女', 36, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院呼吸内科三病房主任、主任医师。 出诊时间：每周三、周四上午。 学术任职：黑龙江省尘肺专家组成员。', '擅长呼吸科常见病、多发病的临床诊断及治疗，如肺炎、肺脓肿、肺癌、肺结核、支气管扩张症、支气管哮喘、慢性支气管炎、慢性阻塞性肺气肿、肺源性心脏病等各类胸腔积液的诊断治疗以及肺部少见病，如肺血栓栓塞症、肺间质纤维化等疾病的诊断治疗。曾参与甲型流感的防治工作，对流感合并肺炎的诊治积累了比较丰富的临床经验。', 5, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('23', '陆鹏', '男', 38, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院呼吸与危重症医学科一病房主任、主任医师、医学硕士。 出诊时间：每周一、周五上午出诊。', '从事临床内科18年，对呼吸内科常见病、疑难病及危重症的诊断及治疗积累了丰富的经验。熟练掌握气管镜、呼吸机及肺功能操作及诊断技术。', 20, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('24', '杨素华', '男', 35, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院呼吸内三科副主任、主任医师。 出诊时间：每周二上午、周日上午轮。', '擅长慢性阻塞性肺疾病、支气管哮喘、支气管扩张、胸腔积液、肺结核等疾病的治疗。可开展中心静脉导管胸腔植入术，熟练掌握呼吸衰竭、肺间质纤维化、呼吸睡眠暂停综合症、肺结节等疑难病症的诊断治疗', 18, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('25', '蔡宝春', '男', 19, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '呼吸内科二病房副主任、副主任医师。出诊时间：每周四、周日上午（轮）。', '擅长呼吸系统常见疾病诊断及治疗，如矽肺、难治性呼吸道感染等。对气管纤维镜检查、支气管肺泡灌洗、无创机械通气等技术积累了较丰富的临床经验。', 31, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('26', '项飞', '男', 23, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', NULL, NULL, 37, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('27', '董存山 ', '男', 44, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', NULL, NULL, 26, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('28', '薛成', '男', 34, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院泌尿外科、副主任医师。出诊时间：每周一上午出诊。学术任职：中华医学会双鸭山泌尿男科分会委员。', '擅长泌尿外科及男科疾病诊治，熟练掌握泌尿外科微创及传统手术，尤其在经皮肾镜、输尿管镜治疗泌尿系结石、腹腔镜治疗肾囊肿及肾脏肿瘤、电切镜治疗前列腺增生及膀胱肿瘤经验丰富。', 19, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('29', '米春光', '男', 36, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院泌尿外科副主任。出诊时间:每周三上午出诊。学术任职：黑龙江省医学会前列腺组委员、中华医学会双鸭山泌尿男科分会委员。', '擅长泌尿外科及男科常见及疑难疾病的诊治。熟练掌握泌尿外科微创及传统手术，尤其在经皮肾镜、输尿管镜治疗泌尿系结石、腹腔镜治疗肾上腺疾病、肾囊肿及肾脏肿瘤、电切镜治疗前列腺增生及膀胱肿瘤方便积累了丰富经验，完成多项本地区高难度微创手术。', 18, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('3', '王雅超', '男', 47, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院耳鼻喉科主任医师。佳木斯医学院。出诊时间：每周一二五全天、周三四上午出诊。学术任职：黑龙江省医学会耳鼻喉分会耳内科学组成员、黑龙江省医学会眩晕专科联盟成员、黑龙江省老年医学研究会眩晕专业委员会委员、省中西医结合学会第二届眩晕分会委员、省人力资源社会保障厅劳动能力鉴定受聘专家。', '对耳鼻喉科和过敏（变态）反应科疾病的诊疗有较全面和独特的诊疗经验。如:眩晕的诊疗，特别是耳石症的诊断和手法复位治疗；耳聋耳鸣及急慢性中耳炎的诊疗；鼻炎、鼻窦炎和鼻息肉等的鼻内镜手术及治疗；呼吸睡眠低通气暂停综合症的规范化系统诊疗及手术；喉肿物内镜下切除术；过敏性鼻炎及哮喘的诊疗，于本院开展了抽血（体外）检测、皮肤点刺（体内）检测过敏原，和过敏性鼻炎和哮喘的特异性脱敏诊疗。', 35, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('30', '宋开忠', '男', 43, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院泌尿外科学术主任、主任医师。出诊时间：每周四上午出诊。 牡丹江医学院。学术任职：现任中华医学会泌尿外科学会黑龙江省专业委员，中华医学会黑龙江省分会男科学专业委员会委员，黑龙江省中医学会男科专业委员会委员。', '擅长并能独立完成利用等离子电切镜微创技术治疗前列腺增生症（TURP)、膀胱肿瘤（TURBt)；输尿管镜气压弹道碎石术（URL）及钬激光治疗输尿管中下端结石；经皮肾镜(PCNL)治疗肾结石及输尿管上段结石；腹腔镜治疗肾上腺肿瘤、肾囊肿、精索静脉曲张等。并熟练开展膀胱癌行膀胱全切、回肠膀胱术；肾上腺嗜铬细胞瘤切除术；肾盂成形术治疗特发性肾积水；尿道狭窄冷刀内切开；体外冲击波碎石技术；膀胱粘膜代尿道治疗尿道下裂等泌尿外科疑难病症的诊治工作。', 33, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('31', '董希寿', '男', 40, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院泌尿外科副主任医师。出诊时间:每周五上午出诊。牡丹江医学院。 泌尿外科及男科疾病的诊治，特别是老年男性前列腺疾病及女性尿失禁的诊治。', '熟练掌握泌尿外科传统开放手术及微创手术。', 47, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('32', '刘萍', '女', 45, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院内分泌、糖尿病科主任，主任医师。出诊时间：每周二、周五上午。学术任职：中俄医科大学联盟黑龙江省内分泌代谢病学委员会委员、白求恩精神研究会内分泌与糖尿病专委会常委、省医学会糖尿病学分会委员、内分泌分会委员、省医学会高尿酸与痛风分会委员、省糖尿病预防与控制分会委员、省康复医学会内分泌专业委员、黑龙江省东部地区内分泌协作中心副主任委员、双鸭山市医学会内分泌专业主任委员。', '擅长糖尿病、甲状腺疾病、痛风及其它内分泌代谢疾病治疗。', 6, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('33', '邢秀萍', '女', 36, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院营养科主任、内分泌主任医师，毕业于。 出诊时间：每周一、三、四上午出诊。学术任职：中国医师协会会员、黑龙江省营养医师协会副主任委员、黑龙江省营养学会临床营养专业委员会委员、黑龙江省医学会肿瘤营养代谢与治疗专委会委员，全国首批通过注册营养师认证。', '擅长糖尿病及其并发症、甲状腺疾病、包括甲亢、甲减、甲状腺炎、甲状腺肿瘤、血脂异常、营养不良、肠内和肠外营养等疾病的诊断和治疗。擅长糖尿病、痛风、肾脏疾病、心脑血管疾病、胃肠疾病、孕期营养及危重症的肠内营养支持。', 17, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('34', '张阳阳', '女', 37, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院内分泌、糖尿病科副主任、副主任医师。黑龙江省医促会内分泌分会青委会委员、黑龙江省东部地区内分泌协作中心委员、双鸭山市医学会内分泌专业委员会秘书。', '擅长糖尿病及相关并发症、甲状腺疾病、痛风及高尿酸血症，肾上腺疾病及垂体相关疾病。出诊时间：每周四、周六上午。', 4, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('35', '李志军 ', '男', 46, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '毕业于黑龙江中医药大学，中西医结合内分泌专业硕士，副主任医师，从事内分泌临床工作10余年。', '擅长内分泌常见疾病，如糖尿病、甲状腺疾病、痛风等疾病及其并发症的诊断与治疗。对肾上腺、垂体、性腺、甲状旁腺继发性高血压等内分泌相关疾病有较深刻认识，有较较丰富的诊治经验。', 30, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('36', '吕涛', '男', 39, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '皮肤性病科主任医师，出诊时间：每周一至周日全天出诊。中国整形美容协会会员、中国医师协会皮肤科医师分会第一届委员会委员、中国医师协会皮肤专科会员、中国性病艾滋病防治协会会员。', '对东方美学有较深入的研究，对男女面部年轻化有较深的造诣，擅长应用光电设备、激光治疗各种皮肤血管性性疾病、皮肤色素性疾病，如色斑、色沉、痘坑、痘印、激素脸、老年皮肤赘生物、脂溢性角化病等。可开展美白、美肤、美颈、除皱、嫩肤、除毛等美容项目。熟练开展治疗各种皮肤病、性病的治疗。', 10, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('37', '刘德才', '男', 38, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院普外科二病房主任、主任医师。出诊时间：每周一、周三、周四上午。', '对普外科各种疾病的诊断治疗有丰富的经验。擅长微创手术、甲状腺、乳腺、胃肠道、肝胆疾病的诊断及治疗。开展了以腹腔镜手术为代表的微创手术，微创胆囊切除术、微创胃癌、结直肠肿瘤切除术、微创疝修补术、甲状腺手术、脾切除术、肝脓肿病手术，得到广大病人的认可。开展了乳腺癌的保乳手术、甲状腺肿瘤的个体化治疗，使许多甲状腺肿瘤病人获得了良好的治疗和康复。', 24, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('38', '张春松', '男', 40, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院普外科二病房副主任医师。出诊时间：每周五上午出诊。', '从事普外临床工作15年余，积累了大量临床经验，对普外科疑难疾病有一定的独到见解。近年来主攻腹腔镜微创治疗技术，可熟练完成腹腔镜胃癌、结直肠癌根治术及腹腔镜下疝无张力修补、胆囊切除术，急腹症的微创治疗，并早期开展了双镜联合保胆取石术等。熟练掌握甲状腺癌、乳腺癌疾病的规范化治疗。发表相关学术论文数篇。XX', 23, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('39', '杨光 ', '男', 46, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院普外一科副主任、主任医师。出诊时间：每周二上午出诊。学术任职：黑龙江省医师协会胃肿瘤分会委员。', '对胃肠道肿瘤、乳腺、甲状腺肿瘤及腹部急症等创伤外科积累了丰富的治疗经验。能独立完成普通外科各种手术，尤其对腹腔镜胃结肠、胆囊及小儿及成人疝等部微创手术有一定的临床手术经验。', 46, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('4', '高光霞', '女', 51, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院儿科病房主任、主任医师。出诊时间：每周二、周三、周五全天出诊。学术任职：黑龙江省医学会儿科学会委员、黑龙江省儿童哮喘协作组委员、黑龙江省医师协会儿科分会常务委员。', '熟练掌握儿科常见病、疑难病的诊断和治疗。尤其擅长小儿呼吸系统疾病以及小儿生长发育问题的诊治。对小儿反复呼吸道感染、肺炎、毛细支气管炎、慢性咳嗽的诊治有丰富经验。在推广雾化吸入在儿科呼吸系统疾病中的应用、规范儿童哮喘的长期管理、加强儿童哮喘的家庭管理、改善哮喘儿童的生活质量等领域有较深造诣。对小儿矮小症、性早熟等儿童发育方面积累了诊治经验，在儿童神经系统疾病如脑炎和消化系统疾病如腹泻病、消化性溃疡、急慢性胃炎等治疗上有丰富的临床经验。', 41, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('40', '徐同庆 ', '男', 38, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院普外一病区副主任、副主任医师、医学硕士。白求恩医科大学临床专业。学术任职：黑龙江省医师协会胰腺分会青年委员、黑龙江省医学会乳腺分会青年委员、黑龙江省医师协会腔镜甲状腺外科专业委员会委员。出诊时间：每周五上午出诊。', '擅长普外科各种常见病及疑难病诊治，能开展甲状腺、乳腺、胃肠、肝胆、腹部外伤、疝与腹壁外科、腹膜后肿瘤等手术，具有丰富的临床经验，尤其擅长腹腔镜微创外科（行胆囊、阑尾、胃、结直肠、腹股沟疝等微创手术）手术。', 31, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('41', '鄂佳鑫', '男', 35, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院普外一科副主任医师。出诊时间：每周四上午出诊。 佳木斯大学临床医学专业。曾在中日医院肝胆外科、胃肠外科进修学习。黑龙江省医师协会胰腺分会青年委员、黑龙江省医学会乳腺分会青年委员。', '擅长普通外科常见病、胃肠道肿瘤、乳腺、甲状腺肿瘤及腹部急症创伤外科有丰富的治疗经验，能独立完成普通外科各种手术。尤其对腹腔镜胆囊切除、腹股沟疝修复术、等腹部微创手术有一定的临床手术经验。', 16, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('42', '李莎莎', '女', 55, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', 'null', 'null', 50, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('43', '柳鹏', '男', 34, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院神经内科二病房主任、主任医师、医学硕士。出诊时间：每周一全天。学术任职：黑龙江省神经病学委员会委员、黑龙江省康复医学会常务委员、黑龙江省重症医学学会委员、黑龙江医学会脑血管病青年委员。', '擅长脑血管病、神经康复、痴呆、神经重症及其他神经科常见疾病的诊断及治疗。在重症患者的诊断、治疗方面积累了丰富的经验，尤其在各种危重急病的抢救治疗及病房的管理方面具有较深的理论基础和丰富的实践经验。', 11, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('44', '杨世凤', '女', 41, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院神经内科主任兼神经内三科病房主任、主任医师双矿医院卒中中心医疗总监。出诊时间：每周一、周四上午出诊。学术任职：黑龙江省脑血管病学会重症脑血管病专业委员会副主任委员、黑龙江省医学会卒中专业委员会委员、黑龙江省神经系统变性病学会理事、黑龙江省医师协会眩晕专业委员会委员、黑龙江省医学会神经内科专业委员会委员、黑龙江省中西医结合学会第一届脑卒中分会脑血管疾病介入专业组委员、黑龙江省医师协会卒中分会委员、双鸭山市医学会第一届神经内和卒中专业委员会主任委员。', '擅长急性脑梗死静脉溶栓桥接动脉取栓治疗、糖尿病并发脑梗死的治疗、脑梗死的筛查和规范一、二级防治、神经脱髓鞘疾病诊治、位置性眩晕手法复位治疗、颅内感染性疾病诊治。对周围神经病和脊髓病变的诊治积累了丰富临床经验。', 33, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('45', '吕金华', '女', 32, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院神经内二科副主任，副主任医师、医学硕士。出诊时间：每周三上午出诊。学术任职：黑龙江省脑血管病学会重症脑血管病专业委员会会员。', '擅长脑血管病、头晕头痛、癫痫、颅内感染、多发性硬化等脱髓鞘疾病及其他神经科常见疾病的诊断及治疗。', 1, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('46a', '车仁柯', '男', 48, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院神经内科一病房副主任、副主任医师。佳木斯大学。出诊时间：每周五上午出诊。', '熟练掌握神经内科常见病、多发病的诊断、鉴别诊断及治疗，临床经验丰富，多次参加国家级、省级神经病学术培训学习。', 34, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('47', '谢艳萍', '女', 36, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院神经内科三病房副主任、主任医师、医学硕士。出诊时间：每周五上午出诊。学术任职：黑龙江省脑血管病重症分会委员、老年医学委员会眩晕委员会委员。', '擅长神经内科疾病的诊断和治疗，如急性脑梗死溶栓及介入治疗，神经脱髓鞘疾病，颅内感染性疾病，癫痫及周围神经病等疾病的诊治。', 39, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('48', '耿全海', '男', 48, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院神经外科主任、主任医师、医学硕士。出诊时间：每周一、周四上午出诊。', '擅长神经外科的常见病和多发病的治疗和手术。对颅内肿瘤、颅脑外伤的硬膜外血肿、硬膜下血肿、脑内血肿及脑挫裂伤及高血压脑出血的微创手术治疗，尤其对脑血管疾病，如：颅内动脉瘤和脑血管畸形的开颅及介入治疗等有丰富临床经验。', 28, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('49', '解鸿君', '男', 45, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院神经外科一病房副主任、副主任医师、医学硕士。出诊时间：每周三上午出诊。', '擅长脑外伤、脑出血等常见病的诊疗及专供脑血管疾病，如：脑动脉瘤、脑血管畸形、脑出血狭窄疾病的介入治疗。', 25, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('5', '李世红', '女', 42, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院儿科副主任、副主任医师。出诊时间：每周四全天。', '从事儿科临床诊疗工作十余年，擅长儿童呼吸系统及消化系统疾病的诊断与防治，如毛细支气管炎、肺炎、哮喘、急慢性腹泻病、胃炎等。对过敏性紫癜、颅内感染、惊厥、肾炎、肾病综合征、中毒等疾病的诊治亦有丰富经验。', 34, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('50', '梁昕', '男', 46, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院神经外科二病房主任、主任医师。 出诊时间：每周二、周五、周六、周日上午出诊。学术任职：中国研究型医院学会专业委员、黑龙江省医师协会卒中协会会员。', '擅长神经外科常见病和危重症病人的诊断及治疗，尤其对重症颅脑损失的抢救治疗。近几年逐步开展了显微微创手术治疗，在收治颅脑外伤、高血压性脑出血、脑膜瘤、胶质瘤等表浅肿瘤的基础上，成功开展治疗了颅底深部肿瘤、桥小脑角区肿瘤、椎管内肿瘤、后颅窝扁桃体下疝畸形以及脑血管疾病的手术治疗，尤其开展的脑血管造影及脑动脉瘤手术添补了专业学科的本地区空白。', 38, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('51', '孙建国 ', '男', 40, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院消化内科主任、主任医师。出诊时间：每周一、周三上午出诊；胃镜（每周二上午）；肠镜（每周一下午）。学术任职：黑龙江省消化专业委员会委员、黑龙江省中西医结合消化专业委员会委员、黑龙江省中西医结合肝病专业委员会委员。双鸭山市消化专业委员会主任委质。', '擅长食道胃肠肝胆胰等疾病的诊断和中西医结合治疗，精于胃镜、肠镜、十二指肠镜的诊断及内镜下治疗，并辅助以腹部超声引导下介入治疗。', 50, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('52', '孙健', '女', 47, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院消化内科主任医师、消化科副主任。出诊时间：每周一、周二、周四、周六上午出诊。', '擅长胃肠肝胆胰常见病、疑难病等各种消化疾病诊断及治疗。擅长胃镜、结肠镜的诊断及内镜下治疗。', 8, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('53', '曹劲松', '男', 45, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '消化内科副主任、主任医师。 出诊时间：每周二、周五上午出诊。', '擅长胃肠肝胆胰等消化系统疾病的诊断和治疗，擅长胃镜、结肠镜、超声内镜，熟练掌握内镜下止血、息肉切除、异物取出、胃石症碎石、EMR等疾病治疗。', 18, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('54', '王凤琴', '女', 41, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院消化内科副主任、副主任医师。出诊时间：每周三、周五上午出诊。', '擅长胃肠肝胆胰腺等各种消化系统疾病诊断及治疗、胃镜结肠镜的诊断及内镜下治疗，以及胶囊内镜的诊断。', 64, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('55', '魏春梅', '女', 34, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院消化科副主任医师、医学硕士。 出诊时间：每周四上午。', '擅长消化系统常见病的诊断及治疗，消化道早癌的诊断，疑难病的诊疗思维。', 10, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('56', '田立华 ', '女', 46, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院心血管内科主任医师。出诊时间：周一至周五全天。', '擅长心血管内科疾病的诊治，对冠心病、心绞痛、急性心肌梗死、心力衰竭、高血压、高血脂、各种心律失常、心肌病、心肌炎、风心病、肺心病、肺栓塞等疾病的诊治有较深的造诣。', 49, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('57', '高丹', '女', 38, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院心内科二病房副主任、副主任医师。 出诊时间：每周三上午出诊。', '擅长心血管内科常见病、多发病及疑难病的诊治，尤其对高血压、冠心病、心肌病、心律失常、顽固性心力衰竭有独到见解，可开展冠脉造影术及冠心病介入治疗。', 22, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('58', '王恒东', '男', 38, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院心血管内科一病房副主任、主任医师、医学硕士。出诊时间：每周三上午出诊。学术任职：黑龙江省中西医协会心脏重症委员会委员、黑龙江省心脏康复学会副主任委员。', '熟练掌握心内科多发病、常见病及疑难病的诊断治疗。在冠脉造影及冠脉介入治疗、急性心肌梗死、心绞痛、心肌病、心力衰竭、高血压、风湿性心脏病肺栓塞等疾病诊治方面积累了丰富临床经验。XX', 27, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('59', '王印 ', '男', 40, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院心内科二病房主任、主任医师。华北煤炭医学院。出诊时间：每周四上午出诊。', '擅长心血管内科常见病多发病的诊治，尤其擅长急性左心衰、急性心肌梗死、急性肺栓塞、主动脉夹层、动脉瘤及心源性休克等急危重症的诊断及治疗，对心电图有独到见解，同时开展冠状动脉造影及冠脉介入治疗。', 7, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('6', '刘顺英', '女', 38, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院肿瘤血液内科主任、主任医师。出诊时间：每周二、周五上午。学术任职：民族医药学会血液学分会理事、省中西医结合血液病分会委员、省医学会化疗专科委员会委员、乳腺癌专业委员会委员、结直肠肿瘤专业委员会委员、胸部肿瘤专业委员会委员、肝癌学组委员。', '肿瘤内科放化疗、血液内科常见病及多发病的诊断及治疗、风湿免疫病的诊断及治疗。', 46, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('60', '韩希龙', '男', 34, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', 'null', 'null', 15, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('61', '吕书久', '男', 48, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院眼科副主任医师。黑龙江省中医药大学。出诊时间：每周三、周日上午出诊。', '擅长眼科门诊的常见疾病如：眼睑、泪器和眼眶疾病、结膜、角膜和巩膜疾病、晶体与玻璃体疾病、青光眼、白内障、葡萄膜、视网膜疾病、眼屈光学、斜视、弱视、眼与全身性疾病等综合征的诊断治疗。', 53, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('62', '娄宏伟 ', '男', 34, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', 'null', 'null', 27, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('63', '刘顺英', '女', 38, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院肿瘤血液内科主任、主任医师,毕业于华北煤炭医学院。出诊时间：每周二、周五上午出诊。学术任职：民族医药学会血液学分会理事、省中西医结合血液病分会委员、省医学会化疗专科委员会委员、乳腺癌专业委员会委员、结直肠肿瘤专业委员会委员、胸部肿瘤专业委员会委员、肝癌学组委员。', '肿瘤内科放化疗、血液内科常见病及多发病的诊断及治疗、风湿免疫病的诊断及治疗。', 40, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('64', '阎素红', '女', 41, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院肿瘤血液内科医生、副主任医师、医学硕。出诊时间：每周五上午出诊。哈尔滨医科大学肿瘤学专业。', '擅长肿瘤内科常见恶性肿瘤的诊疗、化疗、靶向治疗等。', 55, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('65', '王静', '女', 43, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院肿瘤血液内科医生、副主任医师。牡丹江医学院临床医学专业。出诊时间：每周二上午。', '擅长肿瘤内科常见病，多发病的放化疗治疗。XX', 25, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('66', '隋德海 ', '男', 49, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '毕业于黑龙江中医药大学中医临床专业。黑龙江省中医学会内科分会、肾病分会委员。曾在中国中医科学院广安门医院进修学习，师从于张培彤、侯炜、李道睿教授。', '从事中医临床工作20余年，擅长应用中西医结合诊疗方法治疗中医常见病及疑难病，如：脑梗死、冠心病、失眠、胃炎、便秘、肝硬化、风湿、类风湿、高血压、痛风、糖尿病及其并发症、慢性肾衰竭、颈椎病、腰椎间盘突出症、泌尿系结石、胆石症、肺结节、乳腺结节、甲状腺结节、肺癌、乳腺癌、胃癌、肠癌、胰腺癌、肝癌、阳痿、早泄、不育、月经失调、痛经、更年期综合症、盆腔炎、荨麻疹、痤疮等疾病。运用中医药防止癌前病变、减轻肿瘤术后及放化疗不良反应、预防肿瘤复发转移。', 23, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('67', '张传慧', '女', 29, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院门诊中医科主任、主任医师、医学硕士。出诊时间：周一至周五全天出诊。学术任职：黑龙江中西医学会肾病分会委员、黑龙江中医学会肾病分会委员。', '擅长中医药治疗肾炎、肾功能不全、头晕头痛、失眠、心悸、冠心病、贫血、胃炎、肠炎、肺炎、咽炎、紫癜、月经不调、痛经、小儿便秘、小儿厌食、流涎、遗尿等疾病。并擅长中医体质调理治疗亚健康状态。', 40, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('68', '王丹鸣', '女', 52, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双矿医院中医科主任、主任医师。黑龙江省中医药大学中医学专业。学术任职：黑龙江省风湿病学术委员会委员、黑龙江省中西医结合风湿病委员会委员、黑龙江省医师协会风湿免疫科专业委员会委员。出诊时间：每周一至周五全天出诊', '擅长中医内科常见病及疑难病的诊断及治疗，并积累了丰富的临床经验。如：痹症、水肿、胸痹心痛、中风、眩晕、失眠、郁病、虚劳、淋症、血症、消渴等疾病。对部分皮肤科及妇科疾病的中医治疗效果显著。如：湿疹、黄褐斑、痤疮、带状疱疹、过敏性紫癜、荨麻疹、功能性子宫出血、月经不调、痛经、闭经、子宫肌瘤、乳腺小叶增生等。在中西医结合诊治风湿病中成果显著，如：类风湿性关节炎等疾病、强直性脊柱炎、痛风、骨性关节、干燥综合症、风湿寒性关节痛等。在抢救治疗脑梗塞、脑出血、外科急症、纠正慢性心功能不全、急慢性肾衰、异位妊娠、产后高热等疾病中发挥了祖国医学治疗的特长和作用。', 2, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('69', '吴宪', '男', 37, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '骨科五病房副主任、副主任医师、医学硕士。出诊时间：每周三上午出中西医结合骨科门诊。学术任职：中国医师学会骨科分会会员、中华中医药学会脊柱微创专家委员会委员、黑龙江省中医药学会专业青年委员会副主任委员、知名专家。', '从事骨科专业工作10余年，对颈椎病、腰椎间盘突出、骨质增生、股骨头缺血性坏死。膝关节半月板损伤、风湿及类风湿等疑难病症有独到见解及处理方法。对脊柱外科、创伤手术积累了一定的经验。', 20, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('7', '徐颖', '女', 33, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '双鸭山双矿医院妇产科副主任、副主任医师。出诊时间：每周三、四上午出诊。学术任职：黑龙江省妇幼保健与优生优育协会促进自然分娩专业委员会委员。', '常年在妇产科临床工作中处理妇产科常见病、多发病及急诊急救重症患者中积累了丰富的临床经验。擅长处理产科各种难产、在促进自然阴道分娩、无痛分娩中积累了丰富的临床经验，擅长妇科常见病、多发病诊疗、治疗、在阴道镜、宫、腹腔镜的检查、治疗中，摸索了一定的经验。', 64, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('8', '孟莉', '女', 42, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '骨科五病房副主任、副主任医师、医学硕士。出诊时间：每周三上午出中西医结合骨科门诊。学术任职：中国医师学会骨科分会会员、中华中医药学会脊柱微创专家委员会委员、黑龙江省中医药学会专业青年委员会副主任委员、知名专家。', '妇产科常见病、多发病的诊疗及门诊各种手术。在阴道镜、宫腔镜、腹腔镜等领域积累了丰富的经。', 55, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('9', '田玉杰', '女', 28, 'http://liuhuiying.natapp1.cc/doctor/1.jpg', '妇产科主任医师，毕业于哈尔滨医科大学。', '从事妇产科临床工作20余年，在临床工作中积累较多经验，擅长妇产科常见病，多发病的诊治。每周三出诊', 18, 0, 1, NULL, NULL);
INSERT INTO `doctor` VALUES ('dfz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '834EC9B612611CB0434B4A02DE8D65B5', '3429126090@qq.com');
INSERT INTO `doctor` VALUES ('doctor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '84A196C5983214C3183A29A2669F1F22', '2774668116@qq.com');
INSERT INTO `doctor` VALUES ('lhy2', 'lhy2', '女', 34, NULL, 'ddvdfvd', 'sdjjkfhdygfg', NULL, 0, NULL, '834EC9B612611CB0434B4A02DE8D65B5', '');
INSERT INTO `doctor` VALUES ('lixianhe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '83439608E3AEFAF99629327A92CA56FD', '24342');
INSERT INTO `doctor` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', '꧁유혜영꧂', NULL, NULL, 'https://thirdwx.qlogo.cn/mmopen/vi_32/5oQNw43pkMOjibbmoqfPGKTQlfsJAYOGvIr2PIhup7UsDeibE8J3bh7Lhze4ibp80dQjySZbR9Kj7MaMoTshgvWuw/132', NULL, NULL, NULL, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for drug
-- ----------------------------
DROP TABLE IF EXISTS `drug`;
CREATE TABLE `drug`  (
  `d_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `d_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `d_price` double NULL DEFAULT NULL,
  `d_classify` tinyint NULL DEFAULT NULL,
  `d_is_delete` tinyint NOT NULL DEFAULT 0,
  `d_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `d_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `d_old_price` double NULL DEFAULT NULL,
  `d_count` int NULL DEFAULT 0,
  PRIMARY KEY (`d_id`) USING BTREE,
  UNIQUE INDEX `index_name`(`d_name`) USING BTREE,
  INDEX `class_drug`(`d_classify`) USING BTREE,
  CONSTRAINT `foreign_classify_drug` FOREIGN KEY (`d_classify`) REFERENCES `classify` (`c_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 263 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of drug
-- ----------------------------
INSERT INTO `drug` VALUES (1, '感冒灵', 24, 8, 0, '解热镇痛。用于感冒引起的头痛，发热，鼻塞，流涕，咽痛。', 'http://liuhuiying.natapp1.cc/drug/1.jpg', 27, 32);
INSERT INTO `drug` VALUES (2, '维C银翘片', 19, 5, 0, '疏风解表，清热解毒。用于外感风热所致的流行性感冒，症见发热、头痛、咳嗽、口干、咽喉疼痛。', 'http://liuhuiying.natapp1.cc/drug/2.jpg', 35, 59);
INSERT INTO `drug` VALUES (3, '复方氨酚烷胺胶囊', 18, 2, 0, '适用于缓解普通感冒及流行性感冒引起的发热，头痛，四肢酸痛，打喷嚏。流鼻涕、鼻塞、咽痛等症状，也可用于流行性感冒的预防和治疗。', 'http://liuhuiying.natapp1.cc/drug/3.jpg', 34, 100);
INSERT INTO `drug` VALUES (4, '双黄连口服液', 26, 9, 0, '疏风解表，清热解毒。用于外感风热所致的感冒，症见发热、咳嗽、咽痛。', 'http://liuhuiying.natapp1.cc/drug/4.jpg', 29, 20);
INSERT INTO `drug` VALUES (5, '连花清瘟胶囊', 17, 6, 0, '清瘟解毒，宣肺泄热。用于治疗流行性感冒属热毒袭肺证，症见:发热或高热，恶寒，肌肉酸痛，鼻塞流涕，咳嗽，头痛，咽干咽痛，舌偏红，苔黄或黄腻等。', 'http://liuhuiying.natapp1.cc/drug/5.jpg', 37, 100);
INSERT INTO `drug` VALUES (6, '强力枇杷露', 27, 1, 0, '养阴敛肺，止咳祛痰。用于支气管炎咳嗽', 'http://liuhuiying.natapp1.cc/drug/6.jpg', 37, 40);
INSERT INTO `drug` VALUES (7, '咳特灵片', 22, 4, 0, '镇咳，祛痰，平喘，消炎。用于咳喘及慢性支气管炎咳嗽。', 'http://liuhuiying.natapp1.cc/drug/7.jpg', 39, 99);
INSERT INTO `drug` VALUES (8, '气管炎丸 ', 45, 17, 0, '散寒镇咳，祛痰定喘。用于外感风寒引起的咳嗽，气促哮喘，喉中发痒，痰涎壅盛，胸膈满闷，老年痰喘。', 'http://liuhuiying.natapp1.cc/drug/8.jpg', 71, 75);
INSERT INTO `drug` VALUES (9, '止咳宝片', 53, 6, 0, '宣肺祛痰，止咳平喘。用于外感风寒所致的咳嗽、痰多清稀、咳甚而喘；慢性支气管炎、上呼吸道感染见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/9.jpg', 80, 79);
INSERT INTO `drug` VALUES (10, '氢溴酸右美沙芬', 27, 9, 0, '用于支气管炎，咳嗽，干咳嗽，上呼吸道感染，感冒，头痛，咽喉炎，咽炎', 'http://liuhuiying.natapp1.cc/drug/10.jpg', 53, 69);
INSERT INTO `drug` VALUES (11, '人丹', 32, 13, 0, '驱风健胃。用于消化不良，恶心呕吐，晕船，轻度中暑，酒醉饱滞。', 'http://liuhuiying.natapp1.cc/drug/11.jpg', 57, 8);
INSERT INTO `drug` VALUES (12, '午时茶颗粒', 35, 17, 0, '祛风解表，化湿和中。用于外感风寒、内伤食积证，症见恶寒发热、头痛身楚、胸脘满闷、恶心呕吐、腹痛腹泻。', 'http://liuhuiying.natapp1.cc/drug/12.jpg', 53, 31);
INSERT INTO `drug` VALUES (13, '仁丹', 34, 14, 0, '清暑开窍。用于伤暑引起的恶心胸闷，头昏，晕车晕船', 'http://liuhuiying.natapp1.cc/drug/13.jpg', 50, 32);
INSERT INTO `drug` VALUES (14, '二天油', 26, 1, 0, '驱风兴奋药。用于伤风感冒，舟车晕眩，中暑。', 'http://liuhuiying.natapp1.cc/drug/14.jpg', 53, 64);
INSERT INTO `drug` VALUES (15, '复方氨酚那敏颗', 28, 14, 0, '适用于缓解普通感冒及流行性感冒引起的发热、头痛、四肢酸痛、打喷嚏、流鼻涕、鼻塞、咽痛等症状。', 'http://liuhuiying.natapp1.cc/drug/15.jpg', 30, 25);
INSERT INTO `drug` VALUES (16, '阿咖酚胶囊', 17, 13, 0, '用于普通感冒或流行性感冒引起的发热，也用于缓解轻至中度疼痛如头痛、关节痛、偏头痛、牙痛、肌肉痛、神经痛、痛经。', 'http://liuhuiying.natapp1.cc/drug/16.jpg', 28, 33);
INSERT INTO `drug` VALUES (17, '奥利司他胶囊', 59, 9, 0, '用于肥胖或体重超重患者(体重指数≥24)的治疗。', 'http://liuhuiying.natapp1.cc/drug/17.jpg', 82, 90);
INSERT INTO `drug` VALUES (18, '卡优平奥利司他', 53, 4, 0, '本品适用于肥胖症患者和伴发危险因素(高血压、糖尿病和高脂血症)的超重患者。本品通过减轻体重和维持体重，并结合低热量饮食控制肥胖；还可用于减少在体重降低后的反弹。\n中国成人超重和肥胖症的体重指数(BMI)的界定需参考现行相关预防控制指南。在美国，本品适用于BMI≥30kg/m2的肥胖症患者和BMI≥27kg/m2伴发危险因素(高血压、糖尿病和高脂血症)的超重患者。', 'http://liuhuiying.natapp1.cc/drug/18.jpg', 78, 49);
INSERT INTO `drug` VALUES (19, '轻身消胖丸', 34, 9, 0, '益气、利湿，降脂、消胖。用于单纯性肥胖症。', 'http://liuhuiying.natapp1.cc/drug/19.jpg', 38, 77);
INSERT INTO `drug` VALUES (20, '艾爱司奥美拉片', 39, 14, 0, '1.胃食管反流性疾病（GERD）：\n(1)反流性食管炎的治疗。\n(2)已经治愈的食管炎患者预防复发的长期治疗。\n(3)（GERD）的症状控制。\n2.与适当的抗菌疗法联合用药根除幽门螺杆菌，并且：\n(1) 使与幽门螺杆菌感染相关的十二指肠溃疡愈合。\n(2) 防止与幽门螺杆菌相关的消化性溃疡复发。\n3.需要持续NSAID治疗的患者：\n(1)与使用（非甾体抗炎药）NSAID治疗相关的胃溃疡治疗。', 'http://liuhuiying.natapp1.cc/drug/20.jpg', 64, 37);
INSERT INTO `drug` VALUES (21, '三九胃泰颗粒', 36, 12, 0, '清热燥湿，行气活血，柔肝止痛。用于湿热内蕴、气滞血瘀所致的胃痛，症见脘腹隐痛、饱胀反酸、恶心呕吐、嘈杂纳减；浅表性胃炎见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/21.jpg', 40, 52);
INSERT INTO `drug` VALUES (22, '唑肠溶胶囊', 14, 15, 0, '适用于胃溃疡、十二指肠溃疡、应激性溃疡、反流性食管炎和卓-艾综合征(胃泌素瘤)', 'http://liuhuiying.natapp1.cc/drug/22.jpg', 40, 50);
INSERT INTO `drug` VALUES (23, '枸橼酸铋钾片/替硝唑片/克拉霉素片', 37, 7, 0, '适用于对克拉霉素敏感的微生物所引起的感染: 1.下呼吸道感染:如支气管炎、肺炎等;2.上呼吸道感染:如咽炎、窦炎等;3.皮肤及软组织的轻中度感染:如毛囊炎、蜂窝组织炎、丹毒;4鸟分枝杆菌或胞内分枝杆菌感染的预防与治疗; 5与其他药物联合用于幽门螺杆菌感染的治疗。', 'http://liuhuiying.natapp1.cc/drug/23.jpg', 45, 91);
INSERT INTO `drug` VALUES (24, '甲硝唑凝胶', 13, 5, 0, '用于炎症性丘疹、脓疱疮、酒渣鼻红斑的局部治疗。', 'http://liuhuiying.natapp1.cc/drug/24.jpg', 27, 6);
INSERT INTO `drug` VALUES (25, '足光散', 21, 3, 0, '清热燥湿，杀虫敛汗。用于湿热下注所致的角化型手足癣及臭汗症。', 'http://liuhuiying.natapp1.cc/drug/25.jpg', 42, 55);
INSERT INTO `drug` VALUES (26, '景天祛斑胶囊', 24, 17, 0, '活血行气，祛斑消痤，用于气滞血瘀所致的黄褐斑、痤疮。', 'http://liuhuiying.natapp1.cc/drug/26.jpg', 32, 56);
INSERT INTO `drug` VALUES (27, '湿毒清片', 22, 9, 0, '养血润燥，化湿解毒，祛风止痒。用于皮肤瘙痒症属血虚湿蕴皮肤证者。', 'http://liuhuiying.natapp1.cc/drug/27.jpg', 25, 14);
INSERT INTO `drug` VALUES (28, '盐酸特比萘芬乳膏', 11, 11, 0, '用于治疗手癣、足癣、体癣、股癣、花斑癣及皮肤念珠菌病等。', 'http://liuhuiying.natapp1.cc/drug/28.jpg', 29, 2);
INSERT INTO `drug` VALUES (29, '盐酸特比萘芬喷雾剂', 31, 11, 0, '用于治疗手癣、足癣、体癣、股癣及花斑癣等。', 'http://liuhuiying.natapp1.cc/drug/29.jpg', 32, 67);
INSERT INTO `drug` VALUES (30, '复方聚维酮碘搽剂', 35, 4, 0, '1.用于足癣、体癣、头癣、花斑癣、手癣、甲癣；并发细菌感染也可使用。\n2.用于疖、蚊虫叮咬、手足多汗症。', 'http://liuhuiying.natapp1.cc/drug/30.jpg', 40, 29);
INSERT INTO `drug` VALUES (31, '氧氟沙星滴眼液', 23, 1, 0, '本品适用于治疗敏感细菌引起的细菌性结膜炎、细菌性角膜炎。', 'http://liuhuiying.natapp1.cc/drug/31.jpg', 41, 43);
INSERT INTO `drug` VALUES (32, '双氯芬酸钠滴眼液', 27, 13, 0, '1.葡萄膜炎、角膜炎、巩膜炎；2.过敏性眼病；3.眼科术后或眼部损伤消炎止痛。', 'http://liuhuiying.natapp1.cc/drug/32.jpg', 51, 29);
INSERT INTO `drug` VALUES (33, '普拉洛芬滴眼液', 19, 7, 0, '外眼及眼前节炎症的对症治疗。', 'http://liuhuiying.natapp1.cc/drug/33.jpg', 27, 13);
INSERT INTO `drug` VALUES (34, '牛磺酸滴眼液', 29, 15, 0, '用于牛磺酸代谢失调引起的白内障。也可用于急性结膜炎、疱疹性结膜炎、病毒性结膜炎的辅助治疗。', 'http://liuhuiying.natapp1.cc/drug/34.jpg', 53, 80);
INSERT INTO `drug` VALUES (35, '利巴韦林滴眼液', 31, 3, 0, '适用于单纯疱疹病毒性角膜炎。', 'http://liuhuiying.natapp1.cc/drug/35.jpg', 43, 60);
INSERT INTO `drug` VALUES (36, '玻璃酸钠滴眼液', 28, 2, 0, '用于干眼症，缓解干眼症状', 'http://liuhuiying.natapp1.cc/drug/36.jpg', 46, 60);
INSERT INTO `drug` VALUES (37, '氧氟沙星滴耳液', 47, 2, 0, '敏感菌引起的感染，如中耳炎、外耳道炎、鼓膜炎。', 'http://liuhuiying.natapp1.cc/drug/37.jpg', 72, 20);
INSERT INTO `drug` VALUES (38, '过氧化氢溶液', 15, 4, 0, '化脓性外耳道炎和中耳炎、文森口腔炎、齿龈脓漏、扁桃体炎及清洁伤口。', 'http://liuhuiying.natapp1.cc/drug/38.jpg', 30, 21);
INSERT INTO `drug` VALUES (39, '滴耳油', 39, 13, 0, '清热解毒，消肿止痛。用于肝经湿热上攻，耳鸣耳聋，耳内生疮，肿痛刺痒，破流脓水，久不收敛。', 'http://liuhuiying.natapp1.cc/drug/39.jpg', 66, 42);
INSERT INTO `drug` VALUES (40, '头孢克洛干混悬剂', 24, 17, 0, '希刻劳适用于治疗下列敏感菌株引起的感染:\n中耳炎:由肺炎双球菌、流感嗜血杆菌、葡萄球菌、化脓性链球菌(A组β溶血性链球菌)和卡他莫拉氏菌引起。\n下呼吸道感染(包括肺炎):由肺炎双球菌、流感嗜血杆菌、化脓性链球菌(A组β溶血性链球菌)和卡他莫拉氏菌引起。\n上呼吸道感染(包括咽炎和扁桃体炎):由化脓性链球菌(A组β溶血性链球菌)和卡他莫拉氏菌引起。', 'http://liuhuiying.natapp1.cc/drug/40.jpg', 32, 47);
INSERT INTO `drug` VALUES (41, '硼酸冰片滴耳液', 27, 14, 0, '耳内消炎止痛药。用于耳底、耳塞、耳内流黄水等症。', 'http://liuhuiying.natapp1.cc/drug/41.jpg', 41, 11);
INSERT INTO `drug` VALUES (42, '叶酸片', 75, 16, 0, '1.各种原因引起的叶酸缺乏及叶酸缺乏所致的巨幼红细胞贫血；\n2.妊娠期、哺乳期妇女预防给药；\n3.慢性溶血性贫血所致的叶酸缺乏。', 'http://liuhuiying.natapp1.cc/drug/42.jpg', 95, 10);
INSERT INTO `drug` VALUES (43, '复方硫酸亚铁叶酸片', 34, 6, 0, '缺铁性贫血。', 'http://liuhuiying.natapp1.cc/drug/43.jpg', 61, 18);
INSERT INTO `drug` VALUES (44, '多维复合维生素', 128, 16, 0, '用于预防和治疗因维生素与矿物质缺乏所引起的各种疾病。', 'http://liuhuiying.natapp1.cc/drug/44.jpg', 150, 59);
INSERT INTO `drug` VALUES (45, '蛋白琥珀酸铁口服溶液', 92.5, 8, 0, '绝对和相对缺铁性贫血的治疗，由于铁摄入量不足或吸收障碍、急性或慢性失血以及各种年龄患者的感染所引起的隐性或显性缺铁性贫血的治疗，妊娠与哺乳期贫血的治疗。', 'http://liuhuiying.natapp1.cc/drug/45.jpg', 93.5, 40);
INSERT INTO `drug` VALUES (46, '琥珀酸亚铁片', 15.5, 11, 0, '用于缺铁性贫血的预防和治疗。', 'http://liuhuiying.natapp1.cc/drug/46.jpg', 33.5, 22);
INSERT INTO `drug` VALUES (47, '复方锌铁钙颗粒', 11, 11, 0, '用于锌、铁、钙缺乏引起的各种疾病。', 'http://liuhuiying.natapp1.cc/drug/47.jpg', 18, 90);
INSERT INTO `drug` VALUES (48, '小儿复方四维亚铁散', 37.8, 6, 0, '用于促进婴幼儿骨骼发育、改善贫血以及婴幼儿缺钙的辅助治疗。', 'http://liuhuiying.natapp1.cc/drug/48.jpg', 43.8, 82);
INSERT INTO `drug` VALUES (49, '康维他R蜂胶片', 569.4, 12, 0, '增强免疫力', 'http://liuhuiying.natapp1.cc/drug/49.jpg', 580.4, 42);
INSERT INTO `drug` VALUES (50, '逍遥丸', 27, 6, 0, '疏肝健脾，养血调经。用于肝气不舒所致月经不调，胸胁胀痛，头晕目眩，食欲减退。', 'http://liuhuiying.natapp1.cc/drug/50.jpg', 34, 61);
INSERT INTO `drug` VALUES (51, '葡萄糖酸锌口服液', 29, 13, 0, '用于治疗缺锌引起的营养不良、厌食症、异食癖、口腔溃疡、痤疮、儿童生长发育迟缓等', 'http://liuhuiying.natapp1.cc/drug/51.jpg', 31, 81);
INSERT INTO `drug` VALUES (52, '安神补脑液', 21, 13, 0, '生精补髓，益气养血，强脑安神。用于肾精不足、气血两亏所致的头晕、乏力、健忘、失眠；神经衰弱症见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/52.jpg', 39, 21);
INSERT INTO `drug` VALUES (53, '牛黄解毒片', 29.8, 9, 0, '清热解毒，散风止痛。用于肺胃蕴热引起:头目眩晕，口鼻生疮，风火牙痛，暴发火眼，咽喉疼痛，耳鸣肿痛，大便秘结，皮肤刺痒。', 'http://liuhuiying.natapp1.cc/drug/53.jpg', 30.8, 63);
INSERT INTO `drug` VALUES (54, '弥可保 甲钴胺片 ', 145, 7, 0, '周围神经病。', 'http://liuhuiying.natapp1.cc/drug/54.jpg', 148, 50);
INSERT INTO `drug` VALUES (55, '驴胶补血颗粒', 39.9, 4, 0, '补血，益气，调经。用于久病气血两虚所数的体虚乏力、面黄肌瘦、头晕目眩。月经过少。', 'http://liuhuiying.natapp1.cc/drug/55.jpg', 50.9, 63);
INSERT INTO `drug` VALUES (56, '维生素C片', 11, 2, 0, '用于预防坏血病，也可用于各种急慢性传染疾病及紫癜等的辅助治疗。', 'http://liuhuiying.natapp1.cc/drug/56.jpg', 30, 61);
INSERT INTO `drug` VALUES (57, '小儿氨酚黄那敏颗粒', 25, 11, 0, '适用于缓解儿童普通感冒及流行性感冒引起的发热、头痛、四肢酸痛、打喷嚏流鼻涕、鼻塞、咽痛等症状。', 'http://liuhuiying.natapp1.cc/drug/57.jpg', 32, 16);
INSERT INTO `drug` VALUES (58, '维生素E软胶囊', 2.9, 17, 0, '用于心、脑血管疾病及习惯性流产、不孕症的辅助治疗。', 'http://liuhuiying.natapp1.cc/drug/58.jpg', 8.9, 97);
INSERT INTO `drug` VALUES (59, '生血宁片', 34, 15, 0, '益气补血。用于缺铁性贫血属气血两虚证者，证见:面部、肌肤萎黄或苍白，神疲乏力，眩晕耳鸣，心悸气短，舌淡或胖，脉弱等。', 'http://liuhuiying.natapp1.cc/drug/59.jpg', 40, 36);
INSERT INTO `drug` VALUES (60, '屈螺酮炔雌醇片', 148, 9, 0, '女性口服避孕。\n中度寻常痤疮，适用于≥14岁、没有口服避孕药已知禁忌症的已初潮女性。只有在患者希望使用口服避孕药作为避孕措施时才能使用本品治疗痤疮。', 'http://liuhuiying.natapp1.cc/drug/60.jpg', 162, 88);
INSERT INTO `drug` VALUES (61, '复方酮康唑发用洗剂', 45.5, 15, 0, '用于治疗和预防多种真菌引起的感染，如头皮糠疹(头皮屑)、脂溢性皮炎和花斑癣。能迅速缓解由脂溢性皮炎和头皮糠疹引起的脱屑和瘙痒。', 'http://liuhuiying.natapp1.cc/drug/61.jpg', 70.5, 32);
INSERT INTO `drug` VALUES (62, '维C泡腾片', 62.9, 15, 0, '1.增强机体抵抗力，用于预防和治疗各种急、慢性传染性疾病或其他疾病。 2.用于病后恢复期，创伤愈合期及过敏性疾病的辅助治疗。 3.用于预防和治疗坏血病。', 'http://liuhuiying.natapp1.cc/drug/62.jpg', 88.9, 96);
INSERT INTO `drug` VALUES (63, '铝碳酸镁咀嚼片', 35, 13, 0, '1.慢性胃炎\n2.与胃酸有关的胃部不适症状，如胃痛、胃灼热感(烧心)、酸性嗳气、饱胀等。', 'http://liuhuiying.natapp1.cc/drug/63.jpg', 37, 81);
INSERT INTO `drug` VALUES (64, '阿司匹林肠溶片', 48, 3, 0, '阿司匹林对血小板聚集有抑制作用，因此阿司匹林肠溶片适应症如下：\n降低急性心肌梗死疑似患者的发病风险\n预防心肌梗死复发\n中风的二级预防\n降低短暂性脑缺血发作（TIA）及继发脑卒中的风险\n降低稳定性和不稳定性心绞痛患者的发病风险\n动脉外科手术或介入手术后，如经皮冠脉腔内成形术（PTCA），冠状动脉旁路术（CABG），颈动脉内膜剥离术，动静脉分流术\n预防大手术后深静脉血栓和肺栓塞\n降低心血管危险因素者（冠心病家族史、糖尿病、血脂异常、高血压、肥胖、抽烟史、年龄大于50岁者）心肌梗死发作的风险。', 'http://liuhuiying.natapp1.cc/drug/64.jpg', 60, 19);
INSERT INTO `drug` VALUES (65, '糠酸莫米松', 26.8, 10, 0, '用于湿疹、神经性皮炎、异位性皮炎及皮肤瘙痒症。', 'http://liuhuiying.natapp1.cc/drug/65.jpg', 51.8, 50);
INSERT INTO `drug` VALUES (66, '脑心通胶囊', 390, 6, 0, '益气活血，化瘀通络。用于气虚血滞，脉络瘀阻所致中风中经络，半身不遂、肢体麻木、口眼歪斜、舌强语謇及胸痹心痛、胸闷、心悸、气短；脑梗塞、冠心病心绞痛属上述证候者。', 'http://liuhuiying.natapp1.cc/drug/66.jpg', 399, 95);
INSERT INTO `drug` VALUES (67, '阿托伐他汀钙片', 966, 16, 0, '高胆固醇血症。冠心病。', 'http://liuhuiying.natapp1.cc/drug/67.jpg', 988, 23);
INSERT INTO `drug` VALUES (68, '银杏叶滴丸', 175, 12, 0, '活血化瘀通络。用于瘀血阻络引起的胸痹心痛、中风、半身不遂、舌强语謇；冠心病稳定型心绞痛、脑梗死见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/68.jpg', 177, 29);
INSERT INTO `drug` VALUES (69, '达比加群酯胶囊', 2478, 12, 0, '预防存在以下一个或多个危险因素的成人非瓣性房颤患者的卒中和全身性栓塞（SEE）', 'http://liuhuiying.natapp1.cc/drug/69.jpg', 2502, 75);
INSERT INTO `drug` VALUES (70, '卡维地洛片', 69, 7, 0, '原发性高血压:卡维地洛适用于原发性高血压的治疗，可单独使用或与其他抗高血压药特别是噻嗪类利尿剂联合使用。\n治疗有症状的充血性心力衰竭:卡维地洛用于治疗有症状的充血性心力衰竭可降低死亡率和心血管事件的住院率，改善病人的一般情况并减慢疾病进展。卡维地洛可作为标准治疗的附加治疗，也可用于不耐受ACEI或没有使用洋地黄、肼苯哒嗪、硝酸盐类药物治疗的病人', 'http://liuhuiying.natapp1.cc/drug/70.jpg', 73, 90);
INSERT INTO `drug` VALUES (71, '替格瑞洛片', 1065, 15, 0, '本品用于急性冠脉综合征（不稳定性心绞痛、非ST段抬高心肌梗死或ST段抬高心肌梗死）患者，包括接受药物治疗和经皮冠状动脉介入（PCI）治疗的患者，降低血栓性心血管事件的发生率。与氯吡格雷相比，本品可以降低心血管死亡、心肌梗死或卒中复合终点的发生率，两治疗组之间的差异来源于心血管死亡和心肌梗死，而在卒中方面无差异。', 'http://liuhuiying.natapp1.cc/drug/71.jpg', 1069, 22);
INSERT INTO `drug` VALUES (72, '盐酸索他洛尔片', 742, 3, 0, '1、转复，预防室上性心动过速，特别是房室结折返性心动过速，也可用于予激综合征伴室上性心动过速。\n2、心房扑动，心房颤动。\n3、各种室性心率失常，包括室性早搏，持续性及非持续性室性心动过速。\n4、急性心肌梗死并发严重心率失常。', 'http://liuhuiying.natapp1.cc/drug/72.jpg', 748, 38);
INSERT INTO `drug` VALUES (73, '血脂康胶囊', 480, 3, 0, '化浊降脂，活血化瘀，健脾消食。用于痰阻血瘀所致的高脂血症，症见气短、乏力、头晕、头痛、胸闷、腹胀、食少纳呆；也可用于高脂血症及动脉粥样硬化所致的其他的心脑血管疾病的辅助治疗。', 'http://liuhuiying.natapp1.cc/drug/73.jpg', 498, 26);
INSERT INTO `drug` VALUES (74, '清眩治瘫丸', 289, 7, 0, '活血降压，化痰熄风。用于肝阳上亢，肝火内炽引起:头目眩晕、项强脑胀，胸中闷热，惊恐虚烦，半身不遂，口眼歪斜，痰涎壅盛，言语不清，血压升高。', 'http://liuhuiying.natapp1.cc/drug/74.jpg', 306, 17);
INSERT INTO `drug` VALUES (75, '消栓再造丸', 398, 7, 0, '活血化瘀，息风通络，补气养血，消血栓。用于气虚血滞，风痰阻络引起的中风后遗症，肢体偏瘫，半身不遂，口眼歪斜，言语障碍，胸中郁闷等症。', 'http://liuhuiying.natapp1.cc/drug/75.jpg', 401, 4);
INSERT INTO `drug` VALUES (76, '硫酸氢氯吡格雷片', 277, 12, 0, '氯吡格雷用于以下患者的预防动脉粥样硬化血栓形成事件 ：\n1.近期心肌梗死患者（从几天到小于35天），近期缺血性卒中患者（从7天到小于6个月）或确诊外周动脉性疾病的患者。\n2.急性冠脉综合征的患者：\n(1)非ST段抬高性急性冠脉综合征（包括不稳定性心绞痛或非Q波心肌梗死），包括经皮冠状动脉介入术后置入支架的患者，与阿司匹林合用。\n(2)用于ST段抬高性急性冠脉综合征患者，与阿司匹林联合，可合并在溶栓治疗中使用。。', 'http://liuhuiying.natapp1.cc/drug/76.jpg', 294, 69);
INSERT INTO `drug` VALUES (77, '瑞舒伐他汀钙片', 447, 6, 0, '本品适用于经饮食控制和其它非药物治疗（如：运动治疗、减轻体重）仍不能适当控制血脂异常的原发性高胆固醇血症（IIa型，包括杂合子家族性高胆固醇血症）或混合型血脂异常症（IIb型）。\n本品也适用于纯合子家族性高胆固醇血症的患者，作为饮食控制和其它降脂措施（如LDL去除疗法）的辅助治疗，或在这些方法不适用时使用。。', 'http://liuhuiying.natapp1.cc/drug/77.jpg', 469, 34);
INSERT INTO `drug` VALUES (78, '牛黄清心丸', 810, 9, 0, '益气养血，镇静安神，化痰熄风。用于气血不足，痰热上扰引起:胸中郁热，惊悸虚烦，头目眩晕，中风不语，口眼歪斜，半身不遂，言语不清，神志昏迷，痰涎壅盛。', 'http://liuhuiying.natapp1.cc/drug/78.jpg', 814, 62);
INSERT INTO `drug` VALUES (79, '硝苯地平控释片', 197, 12, 0, '高血压，冠心病，慢性稳定型心绞痛。', 'http://liuhuiying.natapp1.cc/drug/79.jpg', 207, 6);
INSERT INTO `drug` VALUES (80, '谷胱甘肽片', 1680, 16, 0, '本品适用于慢性乙肝的保肝治疗。', 'http://liuhuiying.natapp1.cc/drug/80.jpg', 1688, 44);
INSERT INTO `drug` VALUES (81, '富马酸丙酚替诺福韦片', 3024, 7, 0, '富马酸丙酚替诺福韦片适于治疗成人和青少年（年龄 12 岁及以上，体重至少为 35 kg）慢性乙型肝炎（参见【药理毒理】）。', 'http://liuhuiying.natapp1.cc/drug/81.jpg', 3035, 4);
INSERT INTO `drug` VALUES (82, '优思弗熊去氧胆酸胶囊', 2526, 4, 0, '1.胆囊胆固醇结石-必须是X射线能穿透的结石，同时胆囊收缩功能须正常；\n2.胆汁淤积性肝病(如:原发性胆汁性肝硬化)；\n3.胆汁反流性胃炎。', 'http://liuhuiying.natapp1.cc/drug/82.jpg', 2529, 85);
INSERT INTO `drug` VALUES (83, '水飞蓟宾胶囊', 486, 14, 0, '用于急慢性肝炎、脂肪肝的肝功能异常的恢复。', 'http://liuhuiying.natapp1.cc/drug/83.jpg', 495, 12);
INSERT INTO `drug` VALUES (84, '丁二磺酸腺苷蛋氨酸肠溶片', 1199, 8, 0, '适用于肝硬化前和肝硬化所致肝内胆汁淤积。\n适用于妊娠期肝内胆汁淤积。', 'http://liuhuiying.natapp1.cc/drug/84.jpg', 1209, 5);
INSERT INTO `drug` VALUES (85, '平肝舒络丸', 165, 14, 0, '平肝疏络，活血祛风。用于肝气郁结，经络不疏引起的胸肋胀痛，肩背串痛，手足麻木，筋脉拘挛。', 'http://liuhuiying.natapp1.cc/drug/85.jpg', 187, 89);
INSERT INTO `drug` VALUES (86, '拉米夫定片', 870, 13, 0, '拉米夫定片适用于伴有丙氨酸氨基转移酶[ALT]升高和病毒活动复制的、肝功能代偿的成年慢性乙型肝炎病人的治疗。', 'http://liuhuiying.natapp1.cc/drug/86.jpg', 893, 27);
INSERT INTO `drug` VALUES (87, '化滞柔肝颗粒', 543, 6, 0, '清热利湿，化浊解毒，祛瘀柔肝。用于非酒精性单纯性脂肪肝湿热中阻证，症见肝区不适或隐痛，乏力，食欲减退，舌苔黄腻。', 'http://liuhuiying.natapp1.cc/drug/87.jpg', 566, 71);
INSERT INTO `drug` VALUES (88, '达美康格列齐特缓释片', 244, 8, 0, '当单用饮食疗法、运动治疗和减轻体重不足以控制血糖水平的成人2型糖尿病。', 'http://liuhuiying.natapp1.cc/drug/88.jpg', 262, 72);
INSERT INTO `drug` VALUES (89, '拜唐苹阿卡波糖片', 64.5, 5, 0, '配合饮食控制，用于：\n（1）2型糖尿病。\n（2）降低糖耐量低减者的餐后血糖。', 'http://liuhuiying.natapp1.cc/drug/89.jpg', 83.5, 46);
INSERT INTO `drug` VALUES (90, '消渴丸', 149, 15, 0, '滋肾养阴，益气生津。用于气阴两虚所致的消渴病，症见多饮、多尿、多食、消瘦、体倦乏力、眠差、腰痛；2型糖尿病见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/90.jpg', 163, 12);
INSERT INTO `drug` VALUES (91, '西格列汀二甲双胍片II', 720, 11, 0, '本品配合饮食和运动治疗，用于经二甲双胍单药治疗血糖仍控制不佳或正在接受二者联合治疗的2型糖尿病患者。', 'http://liuhuiying.natapp1.cc/drug/91.jpg', 732, 23);
INSERT INTO `drug` VALUES (92, '参芪降糖胶囊', 232, 9, 0, '益气养阴，滋脾补肾。主治消渴症，用于Ⅱ型糖尿病。', 'http://liuhuiying.natapp1.cc/drug/92.jpg', 249, 79);
INSERT INTO `drug` VALUES (93, '消乳散结胶囊', 385, 12, 0, '疏肝解郁，化痰散结，活血止痛。用于肝郁气滞，痰瘀凝聚所致的乳腺增生，乳房胀痛。', 'http://liuhuiying.natapp1.cc/drug/93.jpg', 407, 23);
INSERT INTO `drug` VALUES (94, ' 左乙拉西坦片', 1122, 15, 0, '用于成人及4岁以上儿童癫痫患者部分性发作的加用治疗。', 'http://liuhuiying.natapp1.cc/drug/94.jpg', 1127, 79);
INSERT INTO `drug` VALUES (95, '双环醇片', 25, 6, 0, '本品可用于治疗慢性肝炎所致的氨基转移酶升高。', 'http://liuhuiying.natapp1.cc/drug/95.jpg', 39, 26);
INSERT INTO `drug` VALUES (96, '复方鳖甲软肝片', 32, 17, 0, '软坚散结，化瘀解毒，益气养血。用于慢性乙型肝炎肝纤维化，以及早期肝硬化属瘀血阻络、气血亏虛兼热毒未尽证。症见：胁肋隐痛或肋下痞块，面色晦黯，脘腹胀满，纳差便溏，神疲乏力，口干口苦，赤缕红丝等。', 'http://liuhuiying.natapp1.cc/drug/96.jpg', 58, 93);
INSERT INTO `drug` VALUES (97, '七十味珍珠丸感冒灵', 17, 16, 0, '安神，镇静，通经活络，调和气血，醒脑开窍。用于黑白脉病、龙血不调；中风，瘫痪、半身不遂、癫痫、脑溢血、脑震荡、心脏病、高血压及神经性障碍。', 'http://liuhuiying.natapp1.cc/drug/97.jpg', 25, 84);
INSERT INTO `drug` VALUES (98, '伊木萨克片', 35, 10, 0, '补肾壮阳，益精固涩。用于阳痿，早泄，滑精，遗尿及神经衰弱。', 'http://liuhuiying.natapp1.cc/drug/98.jpg', 48, 42);
INSERT INTO `drug` VALUES (99, '脉血康胶囊', 24, 2, 0, '破血逐瘀，通脉止痛。用于中风，半身不遂，癥瘕痞块，血瘀经闭，跌扑损伤。', 'http://liuhuiying.natapp1.cc/drug/99.jpg', 38, 56);
INSERT INTO `drug` VALUES (100, '白脉软膏', 19, 15, 0, '舒筋活络。用于白脉病，瘫痪，偏瘫，筋腱强直，外伤引起的经络及筋腱断伤、手足挛急、跛行等。', 'http://liuhuiying.natapp1.cc/drug/100.jpg', 23, 56);
INSERT INTO `drug` VALUES (101, '仙灵骨葆胶囊', 35, 15, 0, '滋补肝肾，接骨续筋，强身健骨。用于骨质疏松和骨质疏松症，骨折，骨关节炎，骨无菌性坏死等。', 'http://liuhuiying.natapp1.cc/drug/101.jpg', 40, 11);
INSERT INTO `drug` VALUES (102, '十味蒂达胶囊', 27, 15, 0, '疏肝理气，清热解毒，利胆溶石。用于肝胆湿热所致胁痛，症见右上腹钝痛或绞痛，口苦，恶心，嗳气，泛酸，腹胀；慢性胆囊炎或胆石症见上述证侯者；热源性赤巴(即藏医称谓热症性肝胆疾病)。', 'http://liuhuiying.natapp1.cc/drug/102.jpg', 41, 87);
INSERT INTO `drug` VALUES (103, '复方木尼孜其颗粒', 23, 11, 0, '调节体液及气质，为四种异常体液成熟剂。', 'http://liuhuiying.natapp1.cc/drug/103.jpg', 50, 3);
INSERT INTO `drug` VALUES (104, '二十五味珊瑚胶囊', 34, 10, 0, '开窍、通络、止痛。用于白脉病，神志不清，身体麻木，头昏目眩，脑部疼痛，血压不调，头痛，癫痫及各种神经性疼痛。', 'http://liuhuiying.natapp1.cc/drug/104.jpg', 43, 52);
INSERT INTO `drug` VALUES (105, '银丹心脑通软胶囊', 53, 16, 0, '中医:活血化瘀、行气止痛、消食化滞。用于气滞血瘀引起的胸痹，胸闷，气短，心悸等；冠心病心绞痛，高脂血症、脑动脉硬化，中风、中风后遗症见上述症状者。', 'http://liuhuiying.natapp1.cc/drug/105.jpg', 73, 49);
INSERT INTO `drug` VALUES (106, '驱白巴布期片', 128, 15, 0, '通脉，理血。用于白热斯(白癜风)。', 'http://liuhuiying.natapp1.cc/drug/106.jpg', 145, 88);
INSERT INTO `drug` VALUES (107, '汉方芪胶升白胶囊', 36, 9, 0, '补血益气。用于气血亏损证所引起的头昏眼花、久短乏力、自汗盗汗，以及白细胞减少症见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/107.jpg', 62, 94);
INSERT INTO `drug` VALUES (108, '制霉素片', 41, 17, 0, '口服用于治疗消化道念珠菌病。', 'http://liuhuiying.natapp1.cc/drug/108.jpg', 67, 7);
INSERT INTO `drug` VALUES (109, '修正消糜栓', 45, 9, 0, '清热解毒，燥湿杀虫。用于湿热下注所致的带下病，症见带下量多、色黄、质稠、腥臭、阴部瘙痒；滴虫性阴道炎、霉菌性阴道炎见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/109.jpg', 67, 50);
INSERT INTO `drug` VALUES (110, '安康信依托考昔片', 37, 7, 0, '本品适用于\n1、治疗骨关节炎急性期和慢性期的症状和体征；\n2、治疗急性痛风性关节炎；\n3、治疗原发性痛经；\n处方选择性环氧化酶-2抑制剂应基于对个体患者风险的全面评估（参见【注意事项】）。', 'http://liuhuiying.natapp1.cc/drug/110.jpg', 40, 28);
INSERT INTO `drug` VALUES (111, '低分子量 肝素钠凝胶', 65, 10, 0, '适用闭合性浅表软组织挫伤(恢复期)伤口愈合皮肤美化痘瘢痕斑痕祛痘剖腹产去祛疤痕', 'http://liuhuiying.natapp1.cc/drug/111.jpg', 71, 89);
INSERT INTO `drug` VALUES (112, '保法止非那雄胺片', 99, 9, 0, '本品适用于治疗男性秃发（雄激素性秃发），能促进头发生长并防止继续脱发。不适用于妇女和儿童。（详见说明书）', 'http://liuhuiying.natapp1.cc/drug/112.jpg', 116, 62);
INSERT INTO `drug` VALUES (113, '蔓迪米诺地尔酊', 81, 14, 0, '本品用于治疗男性型脱发和斑秃', 'http://liuhuiying.natapp1.cc/drug/113.jpg', 96, 42);
INSERT INTO `drug` VALUES (114, '达霏欣米诺地尔酊搽剂', 61, 10, 0, '达霏欣用于治疗男性型脱发及斑秃。', 'http://liuhuiying.natapp1.cc/drug/114.jpg', 85, 25);
INSERT INTO `drug` VALUES (115, '养血生发胶囊', 84, 7, 0, '养血祛风，益肾填精。用于血虚风盛、肾精不足所致的脱发，症见毛发松动或呈稀疏状脱落、毛发干燥或油腻、头皮瘙痒；斑秃、全秃、脂溢性脱发与病后、产后脱发见上述证侯者。', 'http://liuhuiying.natapp1.cc/drug/115.jpg', 102, 97);
INSERT INTO `drug` VALUES (116, '复方甘草酸苷胶囊', 52, 3, 0, '治疗慢性肝病，改善肝功能异常。可用于治疗湿疹﹑皮肤炎﹑斑秃。', 'http://liuhuiying.natapp1.cc/drug/116.jpg', 70, 12);
INSERT INTO `drug` VALUES (117, '首乌丸', 118, 12, 0, '补肝肾，强筋骨，乌须发。用于肝肾两虚，头晕目花，耳鸣，腰瘦肢麻，须发早白。', 'http://liuhuiying.natapp1.cc/drug/117.jpg', 128, 66);
INSERT INTO `drug` VALUES (118, '参归生发酊', 136, 15, 0, '养血活血，固表祛风。用于脂溢性皮炎脱发、斑秃、普秃。', 'http://liuhuiying.natapp1.cc/drug/118.jpg', 157, 92);
INSERT INTO `drug` VALUES (119, '汾河胱氨酸片', 45, 7, 0, '用于产后和病后继发性脱发症、慢性肝炎的辅助治疗。', 'http://liuhuiying.natapp1.cc/drug/119.jpg', 64, 65);
INSERT INTO `drug` VALUES (120, '能全素整蛋白型肠内营养剂', 1080, 7, 0, '适用于有胃肠道功能或部分胃肠道功能，而不能或不愿进食足够数量的常规食物以满足机体营养需求的应进行肠内营养治疗的病人，主要用于:\n1.厌食和其相关的疾病\n2.机械性胃肠道功能紊乱；\n3.危重疾病；\n4.营养不良病人的手术前喂养；\n', 'http://liuhuiying.natapp1.cc/drug/120.jpg', 1086, 47);
INSERT INTO `drug` VALUES (121, '能全力肠内营养混悬液', 1188, 12, 0, '本品适用于有胃肠道功能或部分胃肠道功能，而不能或不愿进食足够数量的常规食物，以满足机体营养需求的应进行肠内营养治疗的病人。\n主要用于:\n1.厌食和其相关的疾病\n2.机械性胃肠道功能紊乱\n3.危重疾病\n4.营养不良病人的手术前喂养\n5.本品能用于糖尿病病人', 'http://liuhuiying.natapp1.cc/drug/121.jpg', 1215, 41);
INSERT INTO `drug` VALUES (122, '亚泰参一胶囊', 116, 3, 0, '培元固本，补益气血。与化疗配合用药,有助于提高原发性肺癌、肝癌的疗效,可改善肿瘤患者的气虚症状，提高机体免疫功能。', 'http://liuhuiying.natapp1.cc/drug/122.jpg', 124, 62);
INSERT INTO `drug` VALUES (123, '修正贞芪扶正颗粒', 65, 15, 0, '有提高人体免疫功能，保护骨髓和肾上腺皮质功能；用于各种疾病引起的虚损；配合手术、放射线、化学治疗，促进正常功能的恢复。', 'http://liuhuiying.natapp1.cc/drug/123.jpg', 76, 88);
INSERT INTO `drug` VALUES (124, '恒扬艾瑞昔布片', 46, 14, 0, '本品用于缓解骨关节炎的疼痛症状。', 'http://liuhuiying.natapp1.cc/drug/124.jpg', 51, 51);
INSERT INTO `drug` VALUES (125, '硫酸羟氯喹片', 69, 9, 0, '类风湿关节炎，青少年慢性关节炎，盘状和系统性红斑狼疮，以及由阳光引发或加剧的皮肤疾病。', 'http://liuhuiying.natapp1.cc/drug/125.jpg', 89, 92);
INSERT INTO `drug` VALUES (126, '葡立盐酸氨基葡萄糖胶囊', 58, 3, 0, '用于治疗和预防全身所有部位的骨关节炎，包括膝关节、肩关节、髋关节、手腕关节、颈及脊椎关节和踝关节等。可缓解和消除骨关节炎的疼痛、肿胀等症状，改善关节活动功能。', 'http://liuhuiying.natapp1.cc/drug/126.jpg', 61, 6);
INSERT INTO `drug` VALUES (127, '塞来昔布胶囊', 24, 3, 0, '塞来昔布适用于:\n1)用于缓解骨关节炎(OA)的症状和体征。\n2)用于缓解成人类风湿关节炎(RA)的症状和体征。\n3)用于治疗成人急性疼痛(AP)。(见【临床试验】)\n4)用于缓解强直性脊柱炎的症状和体征。', 'http://liuhuiying.natapp1.cc/drug/127.jpg', 31, 53);
INSERT INTO `drug` VALUES (128, '沙库巴曲缬沙坦钠片', 111, 4, 0, '用于射血分数降低的慢性心力衰竭成人患者，降低心血管死亡和心力衰竭住院的风险。\n沙库巴曲缬沙坦钠片可代替血管紧张素转化酶抑制剂（ACEI）或血管紧张素II受体拮抗剂（ARB），与其他心力衰竭治疗药物（例如：β受体阻断剂、利尿剂和盐皮质激素拮抗剂）合用。', 'http://liuhuiying.natapp1.cc/drug/128.jpg', 137, 46);
INSERT INTO `drug` VALUES (129, '葛洪牌桂龙药膏', 1580, 11, 0, '祛风除湿，舒筋活络，温肾补血。用于风湿骨痛，慢性腰腿痛，肾阳不足及气血亏虚引起的贫血，失眠多梦，气短，心悸，多汗，厌食，腹胀，尿频。', 'http://liuhuiying.natapp1.cc/drug/129.jpg', 1582, 73);
INSERT INTO `drug` VALUES (130, '九典洛索洛芬钠凝胶贴膏', 42, 9, 0, '用于以下疾病及症状的消炎、镇痛:\n骨关节炎、肌肉痛、外伤后的肿胀疼痛。', 'http://liuhuiying.natapp1.cc/drug/130.jpg', 54, 26);
INSERT INTO `drug` VALUES (131, '万特力吲哚美辛凝胶', 55, 10, 0, '肌肉痛、肩部僵硬酸痛、腰痛、关节痛、腱鞘炎(手和腕部疼痛)、肘部疼痛(网球肘等)及跌打损伤、扭伤引起的疼痛', 'http://liuhuiying.natapp1.cc/drug/131.jpg', 81, 9);
INSERT INTO `drug` VALUES (132, '双氯芬酸钠气雾剂', 49, 6, 0, '用于缓解肌肉、软组织和关节的轻至中度疼痛。如缓解肌肉、软组织的扭伤、拉伤、挫伤、劳损、腰背部损伤引起的疼痛以及关节疼痛等。也可用于骨关节炎的对症治疗。', 'http://liuhuiying.natapp1.cc/drug/132.jpg', 63, 66);
INSERT INTO `drug` VALUES (133, '双醋瑞因胶囊', 24, 17, 0, '用于髋、膝关节的骨关节炎治疗。', 'http://liuhuiying.natapp1.cc/drug/133.jpg', 43, 4);
INSERT INTO `drug` VALUES (134, '羚锐通络祛痛膏药骨质增生膏', 39, 15, 0, '活血通络，散寒除湿，消肿止痛。用于腰部、膝部骨性关节炎属瘀血停滞、寒湿阻络证，症见关节刺痛或钝痛，关节僵硬，屈伸不利，畏寒肢冷。', 'http://liuhuiying.natapp1.cc/drug/134.jpg', 65, 22);
INSERT INTO `drug` VALUES (135, '史国公药酒国公酒', 86, 6, 0, '祛风除湿，活血通络。用于风寒湿痹，骨节疼痛，四肢麻木。', 'http://liuhuiying.natapp1.cc/drug/135.jpg', 102, 97);
INSERT INTO `drug` VALUES (136, '雷公一枝蒿伤湿祛痛膏', 66, 3, 0, '祛风除湿，活血止痛。用于风湿关节终痛、肌肉痛，扭伤。', 'http://liuhuiying.natapp1.cc/drug/136.jpg', 67, 17);
INSERT INTO `drug` VALUES (137, '云南白药粉', 36.5, 12, 0, '化瘀止血，活血止痛，解毒消肿。用于跌打损伤，瘀血肿痛，吐血，咳血，便血，痔血，崩漏下血，疮疡肿毒及软组织挫伤，闭合性骨折，支气管扩张及肺结核咳血，溃疡病出血，以及皮扶感染性疾病', 'http://liuhuiying.natapp1.cc/drug/137.jpg', 46.5, 94);
INSERT INTO `drug` VALUES (138, '利多卡因氯己定气雾剂', 41.2, 16, 0, '用于轻度割伤、擦伤、软组织损伤，灼伤、晒伤、以及蚊虫叮咬、瘙痒、痱子等。', 'http://liuhuiying.natapp1.cc/drug/138.jpg', 61.2, 18);
INSERT INTO `drug` VALUES (139, '根痛平胶囊', 72, 11, 0, '活血，通络，止痛。用于风寒阻络所致颈、腰椎病，症见肩颈疼痛、活动受限、上肢麻木。', 'http://liuhuiying.natapp1.cc/drug/139.jpg', 89, 9);
INSERT INTO `drug` VALUES (140, '跌打损伤丸', 52, 6, 0, '行气活血，舒筋止痛。用于跌打损伤，筋骨疼痛。', 'http://liuhuiying.natapp1.cc/drug/140.jpg', 74, 89);
INSERT INTO `drug` VALUES (141, '羚锐壮骨止痛贴', 86, 15, 0, '祛风湿，活血止痛。用于风湿关节、肌肉痛、扭伤。', 'http://liuhuiying.natapp1.cc/drug/141.jpg', 93, 16);
INSERT INTO `drug` VALUES (142, '神芦跳骨片', 77, 3, 0, '消肿定痛，活血舒筋，促进骨痂生长。用于骨折，脱目，新久伤痛.', 'http://liuhuiying.natapp1.cc/drug/142.jpg', 98, 15);
INSERT INTO `drug` VALUES (143, '云南白药酊', 50, 4, 0, '活血散瘀，消肿止痛。用于跌打损伤，风湿麻木，筋骨及关节疼痛，肌肉酸痛及冻伤。', 'http://liuhuiying.natapp1.cc/drug/143.jpg', 55, 24);
INSERT INTO `drug` VALUES (144, '法能阿法骨化醇软胶囊', 54, 12, 0, '\n1.骨质疏松症。\n2.佝偻病和软骨病。\n3.肾性骨病。\n4.甲状旁腺功能减退症。', 'http://liuhuiying.natapp1.cc/drug/144.jpg', 68, 77);
INSERT INTO `drug` VALUES (145, '星鲨维生素D3', 63, 14, 0, '用于预防和治疗维生素D缺乏症。如佝偻病等。', 'http://liuhuiying.natapp1.cc/drug/145.jpg', 89, 11);
INSERT INTO `drug` VALUES (146, '多元康肾骨片钙片', 57, 16, 0, '儿童、成人或老年人缺钙引起的骨质疏松、骨质增生等。', 'http://liuhuiying.natapp1.cc/drug/146.jpg', 65, 24);
INSERT INTO `drug` VALUES (147, '四烯甲萘醌软胶囊', 41, 2, 0, '提高骨质疏松症患者的骨量。', 'http://liuhuiying.natapp1.cc/drug/147.jpg', 57, 85);
INSERT INTO `drug` VALUES (148, '氟比洛芬凝胶贴膏', 74, 12, 0, '下列疾病及症状的镇痛、消炎：\n骨关节炎、肩周炎、肌腱及腱鞘炎、腱鞘周围炎、肱骨外上髁炎（网球肘）、肌肉痛、外伤所致肿胀、疼痛。', 'http://liuhuiying.natapp1.cc/drug/148.jpg', 77, 55);
INSERT INTO `drug` VALUES (149, '爱若华来氟米特片', 53, 4, 0, '(1)适用于成人类风湿关节炎，有改善病情作用。(2)狼疮性肾炎。', 'http://liuhuiying.natapp1.cc/drug/149.jpg', 72, 17);
INSERT INTO `drug` VALUES (150, '火把花根片', 44, 1, 0, '祛风除湿，舒筋活络，清热解毒。具有抗炎和免疫抑制作用。用于类风湿性关节炎，红斑狼疮。', 'http://liuhuiying.natapp1.cc/drug/150.jpg', 47, 21);
INSERT INTO `drug` VALUES (151, '艾拉莫德片', 33, 10, 0, '类风湿关节炎免疫调节保护腰腿酸痛关节疼痛口服抗炎痛风风湿关节病止疼活血风湿病', 'http://liuhuiying.natapp1.cc/drug/151.jpg', 44, 52);
INSERT INTO `drug` VALUES (152, '拜瑞妥利伐沙班片', 133, 13, 0, '用于具有一种或多种危险因素(例如:充血性心力衰竭、高血压、年龄≥75岁、糖尿病、卒中或短暂性脑缺血发作病史)的非瓣膜性房颤成年患者，以降低卒中和全身性栓塞的风险。\n在使用华法林治疗控制良好的条件下，与华法林相比，利伐沙班在降低卒中及全身性栓塞风险方面的相对有效性的数据有限。', 'http://liuhuiying.natapp1.cc/drug/152.jpg', 154, 98);
INSERT INTO `drug` VALUES (153, '白芍总苷胶囊', 24.7, 16, 0, '类风湿性关节炎。', 'http://liuhuiying.natapp1.cc/drug/153.jpg', 40.7, 34);
INSERT INTO `drug` VALUES (154, '冯了性大活络丸', 118, 8, 0, '祛风止痛，除湿豁痰，舒筋活络。用于中风痰厥引起的瘫痪，足萎痹痛，筋脉拘急，腰腿疼痛及跌打损伤，行走不便，胸痹等症。', 'http://liuhuiying.natapp1.cc/drug/154.jpg', 136, 75);
INSERT INTO `drug` VALUES (155, '痹祺胶囊', 95, 7, 0, '益气养血，袪风除湿，活血止痛。用于气血不足，风湿瘀阻，肌肉关节酸痛，关节肿大，僵硬变形或肌肉萎缩，气短乏力；风湿、类风湿性关节炎，腰肌劳损，软组织损伤属上述证候者。', 'http://liuhuiying.natapp1.cc/drug/155.jpg', 110, 72);
INSERT INTO `drug` VALUES (156, '陈李济昆仙胶囊', 56, 11, 0, '\n补肾通络，祛风除湿。主治类风湿关节炎属风湿痹阻兼肾虚证。症见关节肿胀疼痛，屈伸不利，晨僵，关节压痛，关节喜暖畏寒，腰膝酸软，舌质淡，苔白，脉沉细。', 'http://liuhuiying.natapp1.cc/drug/156.jpg', 76, 34);
INSERT INTO `drug` VALUES (157, '爱丽0.3玻璃酸钠滴眼液', 29, 17, 0, '伴随下述疾患的角结膜上皮损伤： 1.干燥综合征（Sjogren\'s syndrome）、斯·约二氏综合征（Stevens-Johnson syndrome）、干眼综合征（dry eye syndrome）等内因性疾患； 2.手术后、药物性、外伤、配戴隐形眼镜等外因性疾患。', 'http://liuhuiying.natapp1.cc/drug/157.jpg', 56, 53);
INSERT INTO `drug` VALUES (158, '丽爱思地夸磷索钠滴眼液', 21, 16, 0, '适用于经诊断为伴随泪液异常的角结膜上皮损伤的干眼患者。', 'http://liuhuiying.natapp1.cc/drug/158.jpg', 43, 64);
INSERT INTO `drug` VALUES (159, '妥布霉素地塞米松滴眼液', 39, 14, 0, '\n1.眼部细菌感染；2..感染性结膜炎；3.眼表感染部位。', 'http://liuhuiying.natapp1.cc/drug/159.jpg', 65, 58);
INSERT INTO `drug` VALUES (160, '富马酸依美斯汀滴眼液', 32, 1, 0, '用于暂时缓解过敏性结膜炎的体征和症状。', 'http://liuhuiying.natapp1.cc/drug/160.jpg', 42, 99);
INSERT INTO `drug` VALUES (161, '布林佐胺滴眼液', 51, 17, 0, '适用于下列情况降低升高的眼压:\n高眼压症\n开角型青光眼\n可以作为对β阻滞剂无效，或者有使用禁忌症的患者单独的治疗药物，或者作为β阻滞剂的协同治疗药物。', 'http://liuhuiying.natapp1.cc/drug/161.jpg', 75, 20);
INSERT INTO `drug` VALUES (162, '酒石酸溴莫尼定滴眼液', 23, 13, 0, '本品适用于降低开角型青光眼及高眼压患者的眼内压。部分患者长期使用本品时，其降低眼内压的作用逐渐减弱。作用减弱出现的时间因人而异，因此应予以密切监视。', 'http://liuhuiying.natapp1.cc/drug/162.jpg', 34, 5);
INSERT INTO `drug` VALUES (163, '莎普爱思苄达赖氨酸滴眼液', 36, 13, 0, '早期老年性白内障。', 'http://liuhuiying.natapp1.cc/drug/163.jpg', 46, 62);
INSERT INTO `drug` VALUES (164, '吡诺克辛滴眼液', 23.5, 6, 0, '初期老年性白内障', 'http://liuhuiying.natapp1.cc/drug/164.jpg', 38.5, 96);
INSERT INTO `drug` VALUES (165, '白内停吡诺克辛钠滴眼液感冒灵', 38, 11, 0, '主要治疗初期老年性白内障、轻度糖尿病性白内障或并发性白内障等。', 'http://liuhuiying.natapp1.cc/drug/165.jpg', 55, 94);
INSERT INTO `drug` VALUES (166, '白内停冰珍去翳滴眼液', 36, 3, 0, '去翳明目。用于老年性白内障初发期。', 'http://liuhuiying.natapp1.cc/drug/166.jpg', 47, 80);
INSERT INTO `drug` VALUES (167, '酒石酸西尼必利片', 49, 13, 0, '改善轻度至中度的功能性消化不良的早饱、餐后饱胀不适、腹胀症状。', 'http://liuhuiying.natapp1.cc/drug/167.jpg', 56, 16);
INSERT INTO `drug` VALUES (168, '波利特雷贝拉唑钠肠溶片', 52, 3, 0, '胃溃疡、十二指肠溃疡、吻合口溃疡、反流性食管炎、卓-艾氏(Zollinger-Ellison)综合征。辅助用于胃溃疡或十二指肠溃疡患者根除幽门螺旋杆菌。', 'http://liuhuiying.natapp1.cc/drug/168.jpg', 78, 42);
INSERT INTO `drug` VALUES (169, '施维舒替普瑞酮胶囊红', 49, 13, 0, '1.急性胃炎、慢性胃炎急性加重期的胃粘膜病变(糜烂、出血、潮红、浮肿)的改善。\n2.胃溃疡。', 'http://liuhuiying.natapp1.cc/drug/169.jpg', 76, 61);
INSERT INTO `drug` VALUES (170, '帕米帕利胶囊 ', 136, 4, 0, '本品适用于既往经过二线及以上化疗的伴有胚系BRCA(gBRCA)突变的复发性晚期卵巣癌、输卵管癌或原发性腹膜癌患者的治疗。\n该适应症是基于一项包括113例既往经过二线及以上化疗的伴有gBRCA突变的复发性晚期卵巣癌、输卵管癌或原发性腹膜癌患者中开展的开放性、多中心、单臂、II期临床实验结果给予的附条件批准(参见【临床实验】)。该适应症的完全批准将取决于正在进行的确证性试验证实本品在该人群的临床获益。', 'http://liuhuiying.natapp1.cc/drug/170.jpg', 139, 78);
INSERT INTO `drug` VALUES (171, '法乐通', 179, 15, 0, '适用于治疗绝经后妇女雌激素受体阳性/或不详的转移性乳腺癌。', 'http://liuhuiying.natapp1.cc/drug/171.jpg', 193, 8);
INSERT INTO `drug` VALUES (172, '优普洛罗替高汀贴片', 124, 13, 0, '本品适用于早期特发性帕金森病症状及体征的单药治疗（不\n与左旋多巴联用），或与左旅多巴联合用于病程中的各个阶段，直至疾病晚期左\n旋多巴的疗效减退、不稳定或出现波动时（剂末现象或开关现象）。', 'http://liuhuiying.natapp1.cc/drug/172.jpg', 131, 3);
INSERT INTO `drug` VALUES (173, '开浦兰 左乙拉西坦片', 114, 1, 0, '用于成人及4岁以上儿童癫痫患者部分性发作的加用治疗。', 'http://liuhuiying.natapp1.cc/drug/173.jpg', 135, 90);
INSERT INTO `drug` VALUES (174, '开浦兰左乙拉西坦口服溶液', 96, 15, 0, '用于成人、儿童及一岁以上婴幼儿癫痫患者部分性发作的加用治疗。', 'http://liuhuiying.natapp1.cc/drug/174.jpg', 97, 40);
INSERT INTO `drug` VALUES (175, '维派特拉考沙胺片', 115, 3, 0, '本品适用于4岁及以上癫痫患者部分性发作的联合治疗。', 'http://liuhuiying.natapp1.cc/drug/175.jpg', 140, 29);
INSERT INTO `drug` VALUES (176, '维派特 拉考沙胺口服溶液', 200, 4, 0, '适用于4岁及以上癫痫患者部分性发作的联合治疗', 'http://liuhuiying.natapp1.cc/drug/176.jpg', 211, 25);
INSERT INTO `drug` VALUES (177, '德巴金丙戊酸钠缓释片', 128, 13, 0, '癫痫\n既可作为单药治疗，也可作为添加治疗,\n用于治疗全面性癫痫:包括失神发作、肌阵挛发作、强直阵挛发作、失张力发作及混合型发作，West，Lennox-Gastaut综合征等。\n用于治疗部分性癫痫:局部癫痫发作，伴有或不伴有全面性发作,\n用于治疗与双相情感障碍相关的躁狂发作。', 'http://liuhuiying.natapp1.cc/drug/177.jpg', 135, 36);
INSERT INTO `drug` VALUES (178, '生物谷灯盏生脉胶囊', 46, 16, 0, '益气养阴，活血健脑。用于气阴两虚，瘀阻脑络引起的胸痹心痛，中风后遗症，症见痴呆、健忘、手足麻木症；冠心病心绞痛，缺血性心脑血管疾病，高脂血症见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/178.jpg', 49, 6);
INSERT INTO `drug` VALUES (179, '九芝堂安宫牛黄丸', 51, 7, 0, '清热解毒，镇惊开窍。用于热病，邪入心包，高热惊厥，神昏谵语；中风昏迷及脑炎、脑膜炎、中毒性脑病、脑出血、败血症见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/179.jpg', 72, 21);
INSERT INTO `drug` VALUES (180, '尔同舒苯溴马隆片', 16, 4, 0, '原发性高尿酸血症，以及痛风性关节炎间歇期。', 'http://liuhuiying.natapp1.cc/drug/180.jpg', 28, 87);
INSERT INTO `drug` VALUES (181, '同仁堂国公酒', 73, 16, 0, '散风祛湿，舒筋活络。用于风寒湿邪闭阻所致的痹病，症见关节疼痛、沉重、屈伸不利、手足麻木、腰腿疼痛。', 'http://liuhuiying.natapp1.cc/drug/181.jpg', 99, 72);
INSERT INTO `drug` VALUES (182, '白云山陈李济咳喘顺丸', 23, 14, 0, '宣肺化痰，止咳平喘。用于痰浊壅肺、肺气失宣所致的咳嗽、气喘、痰多、胸闷；慢性支气管炎见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/182.jpg', 34, 98);
INSERT INTO `drug` VALUES (183, '卢医山大风丸', 16, 7, 0, '舒筋活血，补虚祛风。用于腰腿疼痛，四肢麻木，筋骨酸重。', 'http://liuhuiying.natapp1.cc/drug/183.jpg', 20, 73);
INSERT INTO `drug` VALUES (184, '白云山蛇胆陈皮散', 13, 7, 0, '顺气化痰，祛风健胃。用于痰浊阻肺，胃失和降，咳嗽。呕逆。', 'http://liuhuiying.natapp1.cc/drug/184.jpg', 25, 71);
INSERT INTO `drug` VALUES (185, '云南白药云丰玄麦甘桔颗粒', 43, 16, 0, '清热滋阴，祛痰利咽。用于阴虚火旺，虚火上浮，口鼻干燥，咽喉肿痛。', 'http://liuhuiying.natapp1.cc/drug/185.jpg', 65, 33);
INSERT INTO `drug` VALUES (186, '舒利迭沙美特罗替卡松粉吸入剂', 48, 6, 0, '本品以联合用药形式（支气管扩张剂和吸入皮质激素），用于可逆性阻塞性气道疾病的规则治疗，包括成人和儿童哮喘。', 'http://liuhuiying.natapp1.cc/drug/186.jpg', 65, 55);
INSERT INTO `drug` VALUES (187, '云南白药云丰伤风停胶囊', 62, 17, 0, '发散风寒。用于外感风寒，恶寒发热，头痛，鼻塞，鼻流清涕，肢体酸重，喉痒咳嗽，咳嗽痰清稀，及上呼吸道感染，感冒鼻炎等见上述症候者。', 'http://liuhuiying.natapp1.cc/drug/187.jpg', 83, 75);
INSERT INTO `drug` VALUES (188, '同仁堂桑菊感冒片', 29, 16, 0, '疏风清热，宣肺止咳。用于风热感冒初起，头痛，咳嗽，口干，咽痛。', 'http://liuhuiying.natapp1.cc/drug/188.jpg', 54, 8);
INSERT INTO `drug` VALUES (189, '太极穿龙骨刺片', 56, 12, 0, '补肾，健骨，活血，止痛。用于骨质增生，骨刺疼痛。', 'http://liuhuiying.natapp1.cc/drug/189.jpg', 65, 16);
INSERT INTO `drug` VALUES (190, '富露施乙酰半胱氨酸颗粒', 12, 13, 0, '适用于慢性支气管炎等咳嗽有粘痰而不易咳出的患者。', 'http://liuhuiying.natapp1.cc/drug/190.jpg', 36, 55);
INSERT INTO `drug` VALUES (191, '明仁颈痛片', 19, 8, 0, '活血化瘀，行气止痛。用于神经根型颈椎病属血瘀气滞、脉络闭阻证。症见:颈、肩及上肢疼痛，发僵或窜麻、窜痛。', 'http://liuhuiying.natapp1.cc/drug/191.jpg', 29, 27);
INSERT INTO `drug` VALUES (192, '茂祥烧烫伤膏', 41, 3, 0, '清热解毒，消肿止痛。用于轻度水、火烫伤。', 'http://liuhuiying.natapp1.cc/drug/192.jpg', 47, 68);
INSERT INTO `drug` VALUES (193, '羚锐伤湿止痛膏', 23, 8, 0, '祛风湿，活血止痛。用于风湿性关节炎、肌肉疼痛，关节肿痛。', 'http://liuhuiying.natapp1.cc/drug/193.jpg', 24, 59);
INSERT INTO `drug` VALUES (194, '余良卿号活血止痛膏', 14, 13, 0, '活血止痛，舒筋通络。用于筋骨疼痛，肌肉麻痹，痰核流注，关节酸痛。', 'http://liuhuiying.natapp1.cc/drug/194.jpg', 25, 92);
INSERT INTO `drug` VALUES (195, '晶珠六味壮骨颗粒', 28, 4, 0, '养肝补肾，强筋壮骨。用于骨质疏松证属肝肾不足者。', 'http://liuhuiying.natapp1.cc/drug/195.jpg', 54, 82);
INSERT INTO `drug` VALUES (196, '贝莱盐酸氨溴索口服溶液', 74, 1, 0, '适用于痰液粘稠不易咳出者。', 'http://liuhuiying.natapp1.cc/drug/196.jpg', 87, 34);
INSERT INTO `drug` VALUES (197, '海宝珍珠层粉', 50, 6, 0, '安神，清热，解毒。用于神经衰弱，咽炎，外治口舌肿痛。', 'http://liuhuiying.natapp1.cc/drug/197.jpg', 65, 21);
INSERT INTO `drug` VALUES (198, '佛慈二陈丸', 36, 12, 0, '燥湿化痰，理气和胃。用于咳嗽痰多，胸脘胀闷，恶心呕吐。', 'http://liuhuiying.natapp1.cc/drug/198.jpg', 44, 4);
INSERT INTO `drug` VALUES (199, '好娃娃小儿咳喘灵口服液', 32, 8, 0, '宣肺、清热、止咳、祛痰。用于上呼吸道感染引起的咳嗽。', 'http://liuhuiying.natapp1.cc/drug/199.jpg', 52, 56);
INSERT INTO `drug` VALUES (200, '昆中梅苏颗粒', 48, 3, 0, '清热解暑，生津止渴。于感冒暑热引起的口渴，咽干，胸中满闷，头目眩晕。', 'http://liuhuiying.natapp1.cc/drug/200.jpg', 73, 68);
INSERT INTO `drug` VALUES (201, '鼎炉六一散', 25, 9, 0, '清暑利湿。用于感受暑湿所致的发热、身倦、口渴、泄泻、小便黄少；外用治痱子。', 'http://liuhuiying.natapp1.cc/drug/201.jpg', 33, 71);
INSERT INTO `drug` VALUES (202, '莲花峰茶', 51, 17, 0, '疏风散寒，清热解暑，祛痰利湿，健脾开胃，理气和中。用于四时感冒，伤暑挟湿，脘腹胀满，呕吐泄泻。', 'http://liuhuiying.natapp1.cc/drug/202.jpg', 69, 51);
INSERT INTO `drug` VALUES (203, '五环牌红花油', 14, 9, 0, '驱风药。用于风湿骨痛，跌打扭伤，外感头痛，皮肤瘙痒。', 'http://liuhuiying.natapp1.cc/drug/203.jpg', 26, 41);
INSERT INTO `drug` VALUES (204, '太极川芎茶调丸', 49, 8, 0, '疏风止痛。用于风邪头痛,或有恶寒,发热,鼻塞。', 'http://liuhuiying.natapp1.cc/drug/204.jpg', 57, 51);
INSERT INTO `drug` VALUES (205, '赛立克清开灵滴丸', 36, 13, 0, '清热解毒，镇静安神。用于外感风热所致发热，烦躁不安，咽喉肿痛；及上呼吸道感染、病毒性感冒、急性咽炎见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/205.jpg', 63, 34);
INSERT INTO `drug` VALUES (206, '葛仙翁罗汉果玉竹颗粒', 11, 5, 0, '养阴润肺，止咳生津。用于肺燥咳嗽，咽喉干痛。', 'http://liuhuiying.natapp1.cc/drug/206.jpg', 14, 15);
INSERT INTO `drug` VALUES (207, '振君草复方鱼腥草软胶囊', 18, 3, 0, '清热解毒。用于外感风热引起的咽喉疼痛；急性咽炎、扁桃体炎。', 'http://liuhuiying.natapp1.cc/drug/207.jpg', 33, 73);
INSERT INTO `drug` VALUES (208, '万通感通片', 35, 16, 0, '清热解毒，疏风解表。用于感冒风热证，症见为身热较著，微恶风，头痛鼻塞，咳嗽痰黄，口干咽痛，咽喉红肿。', 'http://liuhuiying.natapp1.cc/drug/208.jpg', 46, 18);
INSERT INTO `drug` VALUES (209, '一正康复方银翘氨敏胶囊', 46, 3, 0, '适用于缓解普通感冒及流行性感冒引起的发热、头痛、四肢酸痛、打喷嚏、流鼻涕、鼻塞、咽痛、咳嗽口干等症状。', 'http://liuhuiying.natapp1.cc/drug/209.jpg', 58, 73);
INSERT INTO `drug` VALUES (210, '仁和活血止痛片', 31, 2, 0, '活血散瘀、消肿止痛。用于跌打损伤，瘀血肿痛。', 'http://liuhuiying.natapp1.cc/drug/210.jpg', 56, 10);
INSERT INTO `drug` VALUES (211, '叶开泰清火片', 32, 15, 0, '清热泻火，通便。用于咽酸肿痛，牙痛，头目眩是，口鼻生疮，风火目赤，大便不通。', 'http://liuhuiying.natapp1.cc/drug/211.jpg', 39, 29);
INSERT INTO `drug` VALUES (212, '广仁午时茶颗粒', 52, 1, 0, '祛风解表，化湿和中。用于外感风寒、内伤食积证，症见恶寒发热、头痛身楚、胸脘满闷、恶心呕吐、腹痛腹泻。', 'http://liuhuiying.natapp1.cc/drug/212.jpg', 67, 15);
INSERT INTO `drug` VALUES (213, '正茂痰咳净片', 24, 12, 0, '通窍顺气，止咳，化痰。用于支气管炎、咽炎等引起的咳嗽多痰，气促，气喘。', 'http://liuhuiying.natapp1.cc/drug/213.jpg', 51, 88);
INSERT INTO `drug` VALUES (214, '立效橘红颗粒', 38, 6, 0, '清肺，化痰，止咳。用于痰热咳嗽，痰多，色黄黏稠，胸闷口干。', 'http://liuhuiying.natapp1.cc/drug/214.jpg', 45, 93);
INSERT INTO `drug` VALUES (215, '思壮独活止痛搽剂', 16, 11, 0, '止痛，消肿，散瘀。用于小关节挫伤，韧带、肌肉拉伤及风湿痛。', 'http://liuhuiying.natapp1.cc/drug/215.jpg', 22, 1);
INSERT INTO `drug` VALUES (216, '太福红药片', 69, 17, 0, '活血止痛，去瘀生新。用于跌打损伤，瘀血肿痛，风湿麻木。', 'http://liuhuiying.natapp1.cc/drug/216.jpg', 81, 25);
INSERT INTO `drug` VALUES (217, '百年乐芒果止咳片', 59, 1, 0, '宣肺化痰，止咳平喘。用于咳嗽，气喘，多痰。', 'http://liuhuiying.natapp1.cc/drug/217.jpg', 71, 23);
INSERT INTO `drug` VALUES (218, '再康复方锌布颗粒', 43, 4, 0, '用于缓解普通感冒或流行性感冒引起的发热、头痛、四肢酸痛、鼻塞、流涕、打喷嚏等症状。', 'http://liuhuiying.natapp1.cc/drug/218.jpg', 67, 38);
INSERT INTO `drug` VALUES (219, '吉浩布洛芬混悬液', 34, 15, 0, '用于儿童普通感冒或流行性感冒引起的发热。也用于缓解儿童轻至中度疼痛，如头痛、关节痛、偏头痛、牙痛、肌肉痛、神经痛、痛经', 'http://liuhuiying.natapp1.cc/drug/219.jpg', 38, 20);
INSERT INTO `drug` VALUES (220, '久金感冒疏风胶囊', 52, 14, 0, '辛温解表，宣肺和中。用于风寒感冒，发热咳嗽，头痛怕冷，鼻流清涕，骨节酸痛，四肢疲倦。', 'http://liuhuiying.natapp1.cc/drug/220.jpg', 54, 86);
INSERT INTO `drug` VALUES (221, '史达功右美沙芬愈创甘油醚糖浆', 12, 8, 0, '用于上呼吸道感染(如普通感冒和流行性感冒)、支气管炎等引起的咳嗽、咳痰。', 'http://liuhuiying.natapp1.cc/drug/221.jpg', 37, 68);
INSERT INTO `drug` VALUES (222, '前列康普乐安', 31, 14, 0, '补肾固本。用于肾气不固所致腰膝痠软，排尿不畅、尿后余沥或失禁;慢性前列腺炎及前列腺增生症见上述症候者。', 'http://liuhuiying.natapp1.cc/drug/222.jpg', 41, 81);
INSERT INTO `drug` VALUES (223, '太极鼻窦炎口服液', 16, 12, 0, '疏散风热，清热利湿，宜通鼻窦。用于风热犯肺、湿热内蕴所致的鼻塞不通、流黄稠涕；急慢性鼻炎、鼻窦炎见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/223.jpg', 17, 1);
INSERT INTO `drug` VALUES (224, '拜耳开瑞坦氯雷他定片', 39, 15, 0, '用于缓解过敏性鼻炎有关的症状，如喷嚏、流涕、鼻痒、鼻塞以及眼部痒及烧灼感。口服药物后，鼻和眼部症状及体征得以迅速缓解。亦适用于缓解慢性荨麻疹、瘙痒性皮肤病及其他过敏性皮肤病的症状及体征。', 'http://liuhuiying.natapp1.cc/drug/224.jpg', 66, 61);
INSERT INTO `drug` VALUES (225, '曼秀雷敦伤风通', 10, 7, 0, '用于感冒引起的鼻塞。', 'http://liuhuiying.natapp1.cc/drug/225.jpg', 33, 3);
INSERT INTO `drug` VALUES (226, '雷诺考特布地奈德鼻炎喷雾剂', 46, 5, 0, '治疗季节性和常年性过敏性鼻炎，常年性非过敏性鼻炎 ；预防鼻息肉切除后鼻息肉的再生，对症治疗鼻息肉', 'http://liuhuiying.natapp1.cc/drug/226.jpg', 52, 31);
INSERT INTO `drug` VALUES (227, '白马通窍鼻炎片', 15, 3, 0, '散风固表、宣肺通窍。用于内热蕴肺、表虚不固所致的鼻塞时轻时重、鼻流清涕或浊涕、前额头痛；慢性鼻炎、过敏性鼻炎、鼻窦炎见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/227.jpg', 31, 47);
INSERT INTO `drug` VALUES (228, '必喷复方熊胆通鼻喷雾剂', 41, 17, 0, '疏风通窍。适用于急性鼻炎之鼻塞，流涕', 'http://liuhuiying.natapp1.cc/drug/228.jpg', 50, 42);
INSERT INTO `drug` VALUES (229, '伯克纳丙酸倍氯米松鼻喷雾剂', 32, 8, 0, '预防和治疗常年性及季节性的过敏性鼻炎，也可用于血管舒缩性鼻炎。', 'http://liuhuiying.natapp1.cc/drug/229.jpg', 54, 70);
INSERT INTO `drug` VALUES (230, '博科富马酸酮替芬滴鼻液', 30, 4, 0, '用于过敏性鼻炎。', 'http://liuhuiying.natapp1.cc/drug/230.jpg', 57, 20);
INSERT INTO `drug` VALUES (231, '奥可安甲硝唑口颊片', 39, 12, 0, '用于牙龈炎、牙周炎、冠周炎及口腔溃疡。', 'http://liuhuiying.natapp1.cc/drug/231.jpg', 56, 89);
INSERT INTO `drug` VALUES (232, '白云山口洁喷雾剂', 19, 13, 0, '清热解毒。用于口舌生疮，牙龈、咽喉肿痛。', 'http://liuhuiying.natapp1.cc/drug/232.jpg', 23, 85);
INSERT INTO `drug` VALUES (233, '信龙复方硼砂含漱液', 29, 14, 0, '健齿固龈，清血止痛。用于牙周疾病引起的牙齿酸软，咀嚼无力，松动移位，牙龈出血以及口舌生疮，咽喉肿痛，口臭烟臭。', 'http://liuhuiying.natapp1.cc/drug/233.jpg', 52, 57);
INSERT INTO `drug` VALUES (234, '奇康西帕依固龈液口服液', 36, 14, 0, '关节炎颈椎病骨质增生阵痛舒筋活洛', 'http://liuhuiying.natapp1.cc/drug/234.jpg', 57, 29);
INSERT INTO `drug` VALUES (235, '水仙丁硼乳膏牙膏', 53, 8, 0, '有消炎止痛作用。用于牙龈炎、牙周炎、牙龈红肿、口腔炎等。', 'http://liuhuiying.natapp1.cc/drug/235.jpg', 61, 76);
INSERT INTO `drug` VALUES (236, '百灵鸟复方一枝黄花喷雾剂', 60, 15, 0, '清热解毒，宣散风热，清利咽喉。用于上呼吸道感染，急、慢性咽炎、口舌生疮、牙龈肿痛、口臭。', 'http://liuhuiying.natapp1.cc/drug/236.jpg', 63, 92);
INSERT INTO `drug` VALUES (237, '汇仁肾宝片', 20, 15, 0, '调和阴阳，温阳补肾，扶正固本。用于腰腿酸痛，精神不振，夜尿频多，畏寒怕冷；妇女白带清稀。', 'http://liuhuiying.natapp1.cc/drug/237.jpg', 36, 30);
INSERT INTO `drug` VALUES (238, '同溢堂香港益安宁丸', 31, 14, 0, '补气活血，益肝健肾，养心安神。治疗气血虚弱，肝肾不足所致的胸闷气短，畏寒肢冷，手足麻木，对失眠健忘、神疲乏力、腰膝酸软也有一定疗效。', 'http://liuhuiying.natapp1.cc/drug/238.jpg', 48, 75);
INSERT INTO `drug` VALUES (239, '同仁堂桂附地黄丸水蜜丸', 22, 8, 0, '温补肾阳。用于肾阳不足，腰膝痠冷，小便不利或反多，痰饮喘咳。', 'http://liuhuiying.natapp1.cc/drug/239.jpg', 32, 82);
INSERT INTO `drug` VALUES (240, '花城龟鹿补肾丸', 36, 16, 0, '补肾壮阳，益气血，壮筋骨。用于肾阳虚所致的身体虚弱、精神疲乏、腰腿痠软，头晕目眩、精冷、性欲减退、小便夜多、健忘、失眠。', 'http://liuhuiying.natapp1.cc/drug/240.jpg', 61, 87);
INSERT INTO `drug` VALUES (241, '济民可信金水宝胶囊', 19, 3, 0, '补益肺肾，秘精益气。用于肺肾两虚，精气不足，久咳虚喘，神疲乏力，不寐健忘，腰膝痠软，月经不调，阳痿早泄', 'http://liuhuiying.natapp1.cc/drug/241.jpg', 33, 87);
INSERT INTO `drug` VALUES (242, '九芝堂杞菊地黄丸浓缩丸', 45, 16, 0, '滋肾养肝。用于肝肾阴亏，眩晕耳鸣，羞明畏光，迎风流泪，视物昏花。', 'http://liuhuiying.natapp1.cc/drug/242.jpg', 66, 74);
INSERT INTO `drug` VALUES (243, '参苓白术丸', 79, 4, 0, '健脾、益气。用于体倦乏力，食少便溏。', 'http://liuhuiying.natapp1.cc/drug/243.jpg', 90, 8);
INSERT INTO `drug` VALUES (244, '京果海狗丸', 49, 7, 0, '温肾助阳。用于肾阳虚引起的腰膝酸软，神疲乏力，肢体困倦，怕冷，夜尿频多，气短作喘。', 'http://liuhuiying.natapp1.cc/drug/244.jpg', 64, 19);
INSERT INTO `drug` VALUES (245, '美迪生还少胶囊', 34, 3, 0, '温肾补脾，养血益精。用于脾肾虚损，腰膝酸痛，阳萎遗精，耳鸣目眩，精血亏耗，肌体瘦弱，食欲减退，牙根酸痛。', 'http://liuhuiying.natapp1.cc/drug/245.jpg', 51, 70);
INSERT INTO `drug` VALUES (246, '中亚牌包邮至宝三鞭丸', 24, 13, 0, '补血生精，健脑补肾。用于肾虑所致体质虚弱，腰背酸痛，头晕，心悸键忘。', 'http://liuhuiying.natapp1.cc/drug/246.jpg', 38, 93);
INSERT INTO `drug` VALUES (247, '白云山陈李济壮腰健肾丸', 23, 2, 0, '壮腰健肾，养血。祛风湿。用于肾亏腰痛，膝软无力，小便频数，风湿骨痛，神经衰弱。', 'http://liuhuiying.natapp1.cc/drug/247.jpg', 38, 54);
INSERT INTO `drug` VALUES (248, '太极五子衍宗丸', 43, 7, 0, '补肾益精。用于肾虚精亏所致的阳痿不育、遗精早泄、腰痛、尿后余沥。', 'http://liuhuiying.natapp1.cc/drug/248.jpg', 51, 89);
INSERT INTO `drug` VALUES (249, '前列康普乐安胶囊', 32, 10, 0, '补肾固本。用于肾气不固所致腰膝痠软，排尿不畅、尿后余沥或失禁；慢性前列腺炎及前列腺增生症见上述症候者', 'http://liuhuiying.natapp1.cc/drug/249.jpg', 54, 82);
INSERT INTO `drug` VALUES (250, '百年丹固本延龄丸', 35, 11, 0, '固本培元，滋阴，补髓填精，强壮筋骨。用于虚劳损伤，腰痛体倦，心悸失眠，肌肤憔悴，须发早白，经血不调，食欲不振。', 'http://liuhuiying.natapp1.cc/drug/250.jpg', 37, 43);
INSERT INTO `drug` VALUES (251, '力补金秋胶囊', 39, 8, 0, '益气固本，滋阴壮阳，用于肾阳不足、气血亏损所引起的腰膝酸软，畏寒胶冷，神疲乏力，失眠健忘，头晕耳鸣，以及阳痿，遗精，早泄。', 'http://liuhuiying.natapp1.cc/drug/251.jpg', 64, 68);
INSERT INTO `drug` VALUES (252, '黄氏益寿强身膏', 47, 6, 0, '补气养血，滋补肝肾，养心安神，强筋健骨，健脾开胃。用于体虚气弱，食欲不振，腰膝酸软，神疲乏力，头晕目眩，失眠健忘，年老体弱。', 'http://liuhuiying.natapp1.cc/drug/252.jpg', 57, 10);
INSERT INTO `drug` VALUES (253, '仁和补肾填精丸', 23, 5, 0, '补气补血，温肾壮阳。用于气血亏损，肾气不足，腰膝无力，阳萎精冷。', 'http://liuhuiying.natapp1.cc/drug/253.jpg', 25, 46);
INSERT INTO `drug` VALUES (254, '百世康白云山绞股蓝总甙片', 10, 9, 0, '关节炎颈椎病骨质增生阵痛舒筋活洛', 'http://liuhuiying.natapp1.cc/drug/254.jpg', 17, 99);
INSERT INTO `drug` VALUES (255, '圣手镇痛活络酊', 43, 11, 0, '养心健脾，益气和血，除痰化瘀，降血脂。用于高血脂症，见有心悸气短、胸闷肢麻、眩晕头痛、健忘耳鸣、自汗乏力或脘腹胀满等心脾气虚，痰阻血瘀者。', 'http://liuhuiying.natapp1.cc/drug/255.jpg', 44, 56);
INSERT INTO `drug` VALUES (256, '侨光肌苷片', 33, 10, 0, '用于急、慢性肝炎的辅助治疗', 'http://liuhuiying.natapp1.cc/drug/256.jpg', 45, 85);
INSERT INTO `drug` VALUES (257, '白云山维生素E软胶囊', 19, 16, 0, '用于心、脑血管疾病及习惯性流产、不孕症的辅助治疗。', 'http://liuhuiying.natapp1.cc/drug/257.jpg', 21, 54);
INSERT INTO `drug` VALUES (258, '云南白药云丰生脉饮', 41, 17, 0, '益气，养阴生津。用于气阴两亏，心悸气短，自汗。', 'http://liuhuiying.natapp1.cc/drug/258.jpg', 67, 16);
INSERT INTO `drug` VALUES (259, '泛谷心脑欣丸', 46, 3, 0, '益气养阴，活血化瘀。用于气阴不足，瘀血阻滞所引起头晕，头痛，心悸，气喘，乏力。', 'http://liuhuiying.natapp1.cc/drug/259.jpg', 61, 18);
INSERT INTO `drug` VALUES (260, '汉方黄芪颗粒', 52, 14, 0, '补气固表。用于气短心悸、自汗。', 'http://liuhuiying.natapp1.cc/drug/260.jpg', 74, 39);
INSERT INTO `drug` VALUES (261, '上龙归脾丸', 15, 8, 0, '益气健脾，养血安神。用于心脾两虚，气短心悸，失眠多梦，头昏头晕，肢倦乏力，食欲不振。', 'http://liuhuiying.natapp1.cc/drug/261.jpg', 29, 43);
INSERT INTO `drug` VALUES (262, '西藏药业诺迪康胶囊', 48, 15, 0, '益气活血，通脉止痛。用于气虚血瘀所致胸痹，症见胸闷，刺痛或隐痛，心悸气短，神疲乏力，少气懒言，头晕目眩；冠心病、心绞痛见上述证候者。', 'http://liuhuiying.natapp1.cc/drug/262.jpg', 51, 97);
INSERT INTO `drug` VALUES (263, '悦康唐泰平悦达宁盐酸二甲双胍缓释片', 34, 15, 0, '适用于单用饮食和运动治疗不能获良好控制的2型糖尿病患者。本品可单独用药，也可与磺脲类或胰岛素合用。', 'http://liuhuiying.natapp1.cc/drug/263.jpg', 61, 56);

-- ----------------------------
-- Table structure for duty
-- ----------------------------
DROP TABLE IF EXISTS `duty`;
CREATE TABLE `duty`  (
  `d_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `d_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`d_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of duty
-- ----------------------------
INSERT INTO `duty` VALUES (1, '主任医师');
INSERT INTO `duty` VALUES (2, '副主任医师');
INSERT INTO `duty` VALUES (3, '护士');
INSERT INTO `duty` VALUES (4, '实习护士');
INSERT INTO `duty` VALUES (5, '实习医生');

-- ----------------------------
-- Table structure for index
-- ----------------------------
DROP TABLE IF EXISTS `index`;
CREATE TABLE `index`  (
  `i_image` varchar(52) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `i_announcement` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of index
-- ----------------------------
INSERT INTO `index` VALUES ('http://liuhuiying.natapp1.cc/index/3.jpg', '医院核酸检测最新事宜');
INSERT INTO `index` VALUES ('http://liuhuiying.natapp1.cc/index/2.jpg', '核酸检测报告于下午两点查询');
INSERT INTO `index` VALUES ('http://liuhuiying.natapp1.cc/index/1.jpg', '医院内排队保持距离');

-- ----------------------------
-- Table structure for label
-- ----------------------------
DROP TABLE IF EXISTS `label`;
CREATE TABLE `label`  (
  `l_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `l_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `l_pid` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `l_is_delete` tinyint NOT NULL DEFAULT 0,
  PRIMARY KEY (`l_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of label
-- ----------------------------
INSERT INTO `label` VALUES (1, '家', NULL, 0);
INSERT INTO `label` VALUES (2, '公司', NULL, 0);
INSERT INTO `label` VALUES (3, '学校', NULL, 0);
INSERT INTO `label` VALUES (4, '宾馆', '1', 1);
INSERT INTO `label` VALUES (5, 'zhu', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1);
INSERT INTO `label` VALUES (6, 'ouoy', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1);
INSERT INTO `label` VALUES (7, '123', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1);
INSERT INTO `label` VALUES (8, '老家', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1);
INSERT INTO `label` VALUES (9, '大学', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1);
INSERT INTO `label` VALUES (10, 'dv', 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', 1);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `o_id` char(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `o_sid` int UNSIGNED NOT NULL,
  `o_is_h` tinyint NOT NULL DEFAULT 0,
  `o_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`o_id`) USING BTREE,
  INDEX `fore_p_id`(`o_sid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('006245686870359753', 4, 1, '1655811787000');
INSERT INTO `order` VALUES ('068557285188202781', 2, 0, '1652841008000');
INSERT INTO `order` VALUES ('092755985890944155', 3, 0, '1655774375000');
INSERT INTO `order` VALUES ('191813556417681823', 3, 1, '1655776155000');
INSERT INTO `order` VALUES ('263404613105699768', 4, 1, '1655772981000');
INSERT INTO `order` VALUES ('270883207998863799', 4, 0, '1655811433000');
INSERT INTO `order` VALUES ('321608426963651733', 4, 0, '1655773540000');
INSERT INTO `order` VALUES ('338803948873467670', 4, 1, '1655776871000');
INSERT INTO `order` VALUES ('371619517302339627', 8, 0, '1652257561000');
INSERT INTO `order` VALUES ('395429265456731527', 2, 1, '1655634466000');
INSERT INTO `order` VALUES ('405517615859240900', 4, 0, '1655774710000');
INSERT INTO `order` VALUES ('514461521498090592', 11, 1, '1652329393000');
INSERT INTO `order` VALUES ('524479069143241830', 2, 1, '1655603096000');
INSERT INTO `order` VALUES ('538414218219943316', 3, 1, '1655811073000');
INSERT INTO `order` VALUES ('542936316754796102', 3, 1, '1655776155000');
INSERT INTO `order` VALUES ('546363575994251394', 9, 1, '1652261542000');
INSERT INTO `order` VALUES ('582049869419198183', 8, 0, '1652257071000');
INSERT INTO `order` VALUES ('744588514779125173', 8, 1, '1652258029000');
INSERT INTO `order` VALUES ('792693559322088487', 2, 1, '1655771797000');
INSERT INTO `order` VALUES ('886345061955766071', 1, 1, '1652602631000');
INSERT INTO `order` VALUES ('910246051705780381', 3, 1, '1655774895000');
INSERT INTO `order` VALUES ('999110784855954588', 10, 0, '1652265105000');

-- ----------------------------
-- Table structure for patient
-- ----------------------------
DROP TABLE IF EXISTS `patient`;
CREATE TABLE `patient`  (
  `p_openid` char(28) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `p_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_image` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_email` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_age` tinyint NULL DEFAULT NULL,
  `p_province` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_city` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_area` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_ethnic` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `p_is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`p_openid`) USING BTREE,
  UNIQUE INDEX `unique_name`(`p_name`) USING BTREE,
  UNIQUE INDEX `emailIndex`(`p_email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of patient
-- ----------------------------
INSERT INTO `patient` VALUES ('dfz', NULL, '834EC9B612611CB0434B4A02DE8D65B5', NULL, NULL, '3429126090qq.cm', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-08 15:15:06');
INSERT INTO `patient` VALUES ('lhy', NULL, '085C8DDCA6A9C8A1E95E69B4F61EE7F9', NULL, NULL, '3056412108@q.om', NULL, NULL, NULL, NULL, NULL, '0', '2022-04-27 13:43:41');
INSERT INTO `patient` VALUES ('lhy3', NULL, '834EC9B612611CB0434B4A02DE8D65B5', NULL, NULL, '2774668116@qcom', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-09 10:49:36');
INSERT INTO `patient` VALUES ('lkl', NULL, '834EC9B612611CB0434B4A02DE8D65B5', NULL, NULL, '2649663545@qqcm', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-04 09:27:07');
INSERT INTO `patient` VALUES ('lxh', NULL, 'D2180EDC73F9267857A0E20CEA0D5F0B', NULL, 'http://10.3.21.140:8086/hospital/static/0492E0E5F71A47D0B0DB26648ED0C9701652262374028.png', 'm', NULL, NULL, NULL, NULL, NULL, '0', '2022-03-15 13:43:44');
INSERT INTO `patient` VALUES ('ojVvv5a1d7A7pfqix0WaIFbJnaQU', '꧁유혜영꧂', NULL, NULL, 'https://thirdwx.qlogo.cn/mmopen/vi_32/5oQNw43pkMOjibbmoqfPGKTQlfsJAYOGvIr2PIhup7UsDeibE8J3bh7Lhze4ibp80dQjySZbR9Kj7MaMoTshgvWuw/132', NULL, NULL, NULL, NULL, NULL, NULL, '0', '2021-11-05 16:49:56');
INSERT INTO `patient` VALUES ('patient', NULL, '84A196C5983214C3183A29A2669F1F22', NULL, NULL, '3056412108@qq.com', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-30 10:53:21');
INSERT INTO `patient` VALUES ('刘慧颖', NULL, 'C6F79EE421D42A57BD9C01AFD3DADC68', NULL, NULL, '3056412108.co', 18, NULL, 'sda', NULL, NULL, '0', '2022-04-28 15:31:49');
INSERT INTO `patient` VALUES ('张三', NULL, '834EC9B612611CB0434B4A02DE8D65B5', NULL, NULL, '3056412108@qq.co', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-18 16:45:32');
INSERT INTO `patient` VALUES ('李显赫', NULL, '591A8E1538A6309CB58FEC9B15FD85FF', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '0', '2022-04-27 13:43:52');
INSERT INTO `patient` VALUES ('李显赫1号', NULL, '06936C0B54EFE66CE3B5EB33FD1B1503', NULL, NULL, '4545', NULL, NULL, 'ss', NULL, NULL, '0', '2022-04-26 15:31:53');
INSERT INTO `patient` VALUES ('李显赫2号', NULL, '185C6BC688F426CF125E2479D4F80085', NULL, NULL, '9', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-04 09:18:20');
INSERT INTO `patient` VALUES ('李显赫3号', NULL, '185C6BC688F426CF125E2479D4F80085', NULL, NULL, '9hgfghh', NULL, NULL, NULL, NULL, NULL, '0', '2022-05-06 10:55:31');

-- ----------------------------
-- Table structure for result
-- ----------------------------
DROP TABLE IF EXISTS `result`;
CREATE TABLE `result`  (
  `r_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `r_oid` char(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `r_res` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`r_id`) USING BTREE,
  INDEX `foreign_pid`(`r_oid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of result
-- ----------------------------
INSERT INTO `result` VALUES (2, '582049869419198183', '阴');
INSERT INTO `result` VALUES (3, '371619517302339627', '阴');
INSERT INTO `result` VALUES (4, '744588514779125173', '阴');
INSERT INTO `result` VALUES (5, '546363575994251394', '阴');
INSERT INTO `result` VALUES (6, '999110784855954588', '阴');
INSERT INTO `result` VALUES (7, '514461521498090592', '阴');
INSERT INTO `result` VALUES (8, '068557285188202781', '阴');
INSERT INTO `result` VALUES (9, '524479069143241830', '阴');
INSERT INTO `result` VALUES (10, '395429265456731527', '阴');
INSERT INTO `result` VALUES (11, '792693559322088487', '阴');
INSERT INTO `result` VALUES (12, '263404613105699768', '阴');
INSERT INTO `result` VALUES (13, '321608426963651733', '阴');
INSERT INTO `result` VALUES (14, '092755985890944155', '阴');
INSERT INTO `result` VALUES (15, '405517615859240900', '阴');
INSERT INTO `result` VALUES (16, '910246051705780381', '阴');
INSERT INTO `result` VALUES (17, '542936316754796102', '阴');
INSERT INTO `result` VALUES (18, '191813556417681823', '阴');
INSERT INTO `result` VALUES (19, '338803948873467670', '阴');
INSERT INTO `result` VALUES (20, '538414218219943316', '阴');
INSERT INTO `result` VALUES (21, '270883207998863799', '阴');
INSERT INTO `result` VALUES (22, '006245686870359753', '阴');

-- ----------------------------
-- Table structure for sick
-- ----------------------------
DROP TABLE IF EXISTS `sick`;
CREATE TABLE `sick`  (
  `s_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `s_pid` char(28) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `s_licence` char(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `s_gender` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `s_phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `s_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `s_default` tinyint NOT NULL DEFAULT 0,
  `s_is_delete` tinyint NOT NULL DEFAULT 0,
  PRIMARY KEY (`s_id`) USING BTREE,
  UNIQUE INDEX `pid_license`(`s_pid`, `s_licence`) USING BTREE,
  CONSTRAINT `fore_pid` FOREIGN KEY (`s_pid`) REFERENCES `patient` (`p_openid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sick
-- ----------------------------
INSERT INTO `sick` VALUES (1, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200101012113', '男', '18241121112', '张三', 0, 1);
INSERT INTO `sick` VALUES (2, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '23050520020531001X', '男', '13147865895', '33', 0, 1);
INSERT INTO `sick` VALUES (3, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '123243200104012121', '女', '18241123111', '张三', 0, 0);
INSERT INTO `sick` VALUES (4, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200201012111', '男', '18241132113', '李四', 0, 0);
INSERT INTO `sick` VALUES (5, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '142232197802023112', '男', '13241123112', '王五', 0, 1);
INSERT INTO `sick` VALUES (6, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '151313200103032112', '男', '18241123858', '王五', 0, 1);
INSERT INTO `sick` VALUES (7, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '151313200103012113', '男', '18241123131', '王五', 0, 1);
INSERT INTO `sick` VALUES (8, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200009092111', '男', '18231143222', '王五', 0, 1);
INSERT INTO `sick` VALUES (9, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '150321200801013112', '男', '18241132113', '小红', 0, 1);
INSERT INTO `sick` VALUES (10, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324198809092111', '男', '15241123112', '王五', 0, 1);
INSERT INTO `sick` VALUES (11, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200901012222', '女', '18241123112', '王五', 0, 1);
INSERT INTO `sick` VALUES (12, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200902023112', '男', '18241123112', '小李', 0, 1);
INSERT INTO `sick` VALUES (13, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200301022113', '男', '18231124112', '王五', 0, 1);
INSERT INTO `sick` VALUES (14, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200901012113', '男', '18241123111', '王五', 0, 1);
INSERT INTO `sick` VALUES (15, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324200908082117', '男', '18241123112', '王二', 0, 1);
INSERT INTO `sick` VALUES (16, 'ojVvv5a1d7A7pfqix0WaIFbJnaQU', '152324199901012127', '女', '13241123232', '张三', 1, 0);

-- ----------------------------
-- Table structure for standard
-- ----------------------------
DROP TABLE IF EXISTS `standard`;
CREATE TABLE `standard`  (
  `r_id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT,
  `r_desc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `r_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `r_min` tinyint NOT NULL,
  `r_max` tinyint NOT NULL,
  PRIMARY KEY (`r_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of standard
-- ----------------------------
INSERT INTO `standard` VALUES (1, '没有抑郁症', '你很健康，很开朗，周围的朋友都很喜欢你这样的个性，是他们获取正能量的来源之一，继续保持这样的心态下去，你会活得更快乐。', 0, 20);
INSERT INTO `standard` VALUES (2, '有抑郁情绪(建议自我调节或心理咨询)', '不要紧张，你只是有这样的情绪而已，并非是真正的抑郁症，这只是正常的心理情绪。但你要注意了，不能继续在加深这样的情绪了，不然真的会变成抑郁症的，尽可能的想一些好的事情，和开心的事情，找一些自己感兴趣的事情来做。', 21, 40);
INSERT INTO `standard` VALUES (3, '有轻度抑郁症(建议心理咨询)', '偶尔自卑的你，对事有点不太自信，总在不断地贬低自己，却没有看到自己的优点，这样的心理是不对的。趁现在你的抑郁症还未加深，赶紧改变这样的想法，在一定的程度 上多给自己一点自信，相信自己，从多方面看看自己的优点。', 41, 60);
INSERT INTO `standard` VALUES (4, '有中度抑郁症(建议立即寻求帮助)', '对事不自信，总在关键的时候掉链子，害怕自己做不成事的你，总是觉得自己差人一等，觉得自己就是做不成功任何事情的想法是不对的，你应该改变这样的想法，很多时候只要你自信一点就可以成功，只要迈出你勇气的一步，你就会发现，其实自己也不是那么差的。', 61, 80);
INSERT INTO `standard` VALUES (5, '有严重抑郁症(建议立即寻求帮助)', '有严重抑郁症的你，害怕与外界接触，一旦接触你就会觉得自己很自卑，觉得他人看不起自己。你应该多接触外界，在一定的程度上多于他人接触，你会发现其实外界也不是那么可怕的，你自己也不是那么的一无是处，其实你的存在也是很重要的。以上只是抑郁测评里面的一部分测试题，只能测试比较表面的情况，如果想深入了解自己是否有抑郁症，建议找专业精神心理科医生为你分析解答。', 81, 100);

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `test_id` tinyint UNSIGNED NOT NULL AUTO_INCREMENT,
  `test_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `test_score` tinyint NOT NULL,
  PRIMARY KEY (`test_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES (1, '你是否一直感到伤心或悲哀?', 3);
INSERT INTO `test` VALUES (2, '是不是总在怀念着以往的美好感伤现在的悲伤?', 3);
INSERT INTO `test` VALUES (3, '你是否感到前景渺茫?', 3);
INSERT INTO `test` VALUES (4, '是否总觉得困难太多，怕自己克服不了?', 3);
INSERT INTO `test` VALUES (5, '你是否觉得自己没有价值或自以为是一个失败者?', 3);
INSERT INTO `test` VALUES (6, '这段时间总在贬低自己抬高他人?', 3);
INSERT INTO `test` VALUES (7, '你是否觉得力不从心或自叹不如别人?', 3);
INSERT INTO `test` VALUES (8, '是否觉得自己总在关键的时候差人一等?', 3);
INSERT INTO `test` VALUES (9, '你是否对任何事都自责?', 3);
INSERT INTO `test` VALUES (10, '这段时间是否觉得自己做了很多对不起朋友的事?', 3);
INSERT INTO `test` VALUES (11, '这段时间你是否一直处在愤怒和不满状态?', 3);
INSERT INTO `test` VALUES (12, '你是否常常有患得患失的感觉?', 3);
INSERT INTO `test` VALUES (13, '你对事业、家庭、爱好或朋友是否丧失了兴趣?', 3);
INSERT INTO `test` VALUES (14, '你常常觉得很无聊?', 3);
INSERT INTO `test` VALUES (15, '你是否感到一蹶不振，做事情毫无动力?', 3);
INSERT INTO `test` VALUES (16, '你觉得任何的事物都不能引起你想动的欲望?', 3);
INSERT INTO `test` VALUES (17, '你是否以为自己已衰老或失去魅力?', 3);
INSERT INTO `test` VALUES (18, '你觉得自己总是比不过他人，没有独特的特点?', 3);
INSERT INTO `test` VALUES (19, '你是否感到食欲不振或情不自禁地暴饮暴食?', 3);
INSERT INTO `test` VALUES (20, '你是否感到食欲不振或情不自禁地暴饮暴食?', 3);
INSERT INTO `test` VALUES (21, '你是否觉得自己有一些心理疾病?', 4);
INSERT INTO `test` VALUES (22, '你是否患有失眠症或整天感到体力不支、昏昏欲睡?', 4);
INSERT INTO `test` VALUES (23, '你是否常常觉得精神恍惚?', 4);
INSERT INTO `test` VALUES (24, '你是否丧失了对性的兴趣?', 4);
INSERT INTO `test` VALUES (25, '你觉得异性对你来说就像同性一般没有差别?', 4);
INSERT INTO `test` VALUES (26, '你是否经常担心自己的健康?', 4);
INSERT INTO `test` VALUES (27, '你是否常常自认自己是哪一个角色?', 4);
INSERT INTO `test` VALUES (28, '你是否认为你的身边有外星人?', 4);
INSERT INTO `test` VALUES (29, '你是否认为生存没有价值，或生不如死?', 4);
INSERT INTO `test` VALUES (30, '你常常觉得生无可恋?', 4);

-- ----------------------------
-- Table structure for work
-- ----------------------------
DROP TABLE IF EXISTS `work`;
CREATE TABLE `work`  (
  `w_id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `w_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`w_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of work
-- ----------------------------
INSERT INTO `work` VALUES (1, '牙科门诊');
INSERT INTO `work` VALUES (2, '急性牙科门诊');
INSERT INTO `work` VALUES (3, '便秘门诊');
INSERT INTO `work` VALUES (4, '便民门诊');
INSERT INTO `work` VALUES (5, '产科诊室');
INSERT INTO `work` VALUES (6, '耳鼻喉科诊室');
INSERT INTO `work` VALUES (7, '耳鼻喉专家诊室');
INSERT INTO `work` VALUES (8, '儿科普通诊室');
INSERT INTO `work` VALUES (9, '儿科专家诊室');
INSERT INTO `work` VALUES (10, '风湿血液科专家诊室');
INSERT INTO `work` VALUES (11, '妇产科专家诊室');
INSERT INTO `work` VALUES (12, '妇产科室');
INSERT INTO `work` VALUES (13, '发热门诊');
INSERT INTO `work` VALUES (14, '关节疾病微创外科');
INSERT INTO `work` VALUES (15, '肛肠科');
INSERT INTO `work` VALUES (16, '骨科门诊');
INSERT INTO `work` VALUES (17, '骨科专家诊室');
INSERT INTO `work` VALUES (18, '肝炎科');
INSERT INTO `work` VALUES (19, '呼吸内科门诊');
INSERT INTO `work` VALUES (20, '呼吸专家诊室');
INSERT INTO `work` VALUES (21, '急诊耳鼻喉科诊室');
INSERT INTO `work` VALUES (22, '急诊儿科诊室');
INSERT INTO `work` VALUES (23, '急诊妇产科诊室');
INSERT INTO `work` VALUES (24, '急诊口腔科诊室');
INSERT INTO `work` VALUES (25, '急诊科诊室');
INSERT INTO `work` VALUES (26, '急诊泌尿烧伤科诊室');
INSERT INTO `work` VALUES (27, '急诊眼科诊室');
INSERT INTO `work` VALUES (28, '口腔科专家诊室');
INSERT INTO `work` VALUES (29, '口腔科诊室');
INSERT INTO `work` VALUES (30, '理疗科');
INSERT INTO `work` VALUES (31, '慢性伤口造口护理门诊');
INSERT INTO `work` VALUES (32, '泌尿专家诊室');
INSERT INTO `work` VALUES (33, '慢性病诊室');
INSERT INTO `work` VALUES (34, '内分泌普通诊室');
INSERT INTO `work` VALUES (35, '内分泌专家诊室');
INSERT INTO `work` VALUES (36, '男科门诊');
INSERT INTO `work` VALUES (37, '普通皮肤门诊');
INSERT INTO `work` VALUES (38, '皮肤专家诊室');
INSERT INTO `work` VALUES (39, 'PICC护理门诊');
INSERT INTO `work` VALUES (40, '普外门诊');
INSERT INTO `work` VALUES (41, '普外专家诊室');
INSERT INTO `work` VALUES (42, '肾内科专家诊室');
INSERT INTO `work` VALUES (43, '神经内科门诊');
INSERT INTO `work` VALUES (44, '神经内科专家诊室');
INSERT INTO `work` VALUES (45, '神经外科脑血管病诊室');
INSERT INTO `work` VALUES (46, '神经外科专家');
INSERT INTO `work` VALUES (47, '疼痛门诊');
INSERT INTO `work` VALUES (48, '胸外诊室');
INSERT INTO `work` VALUES (49, '线上核酸');
INSERT INTO `work` VALUES (50, '消化内科门诊');
INSERT INTO `work` VALUES (51, '消化专家诊室');
INSERT INTO `work` VALUES (52, '心内科门诊');
INSERT INTO `work` VALUES (53, '心内专家诊室');
INSERT INTO `work` VALUES (54, '眼科专家诊室');
INSERT INTO `work` VALUES (55, '眼科诊室');
INSERT INTO `work` VALUES (56, '肿瘤科诊室');
INSERT INTO `work` VALUES (57, '肿瘤科专家诊室');
INSERT INTO `work` VALUES (58, '中医妇科诊室');
INSERT INTO `work` VALUES (59, '中医普通二诊室');
INSERT INTO `work` VALUES (60, '中医普通一诊室');
INSERT INTO `work` VALUES (61, '中医专家二诊室');
INSERT INTO `work` VALUES (62, '中医针灸科');
INSERT INTO `work` VALUES (63, '中医专家三诊室');
INSERT INTO `work` VALUES (64, '中医专家四诊室');

-- ----------------------------
-- View structure for doctor_work_duty
-- ----------------------------
DROP VIEW IF EXISTS `doctor_work_duty`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `doctor_work_duty` AS select `doc`.`doc_open_id` AS `doc_open_id`,`doc`.`doc_name` AS `doc_name`,`doc`.`doc_gender` AS `doc_gender`,`doc`.`doc_age` AS `doc_age`,`doc`.`doc_img` AS `doc_img`,`doc`.`doc_desc` AS `doc_desc`,`doc`.`doc_best` AS `doc_best`,`doc`.`doc_work_id` AS `doc_work_id`,`doc`.`doc_is_delete` AS `doc_is_delete`,`doc`.`doc_duty` AS `doc_duty`,`doc`.`doc_password` AS `doc_password`,`doc`.`doc_email` AS `doc_email`,`w`.`w_id` AS `w_id`,`w`.`w_name` AS `w_name`,`d`.`d_id` AS `d_id`,`d`.`d_name` AS `d_name` from ((`doctor` `doc` join `work` `w` on((`doc`.`doc_work_id` = `w`.`w_id`))) join `duty` `d` on((`d`.`d_id` = `doc`.`doc_duty`)));

SET FOREIGN_KEY_CHECKS = 1;
