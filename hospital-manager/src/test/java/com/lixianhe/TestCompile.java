package com.lixianhe;

import org.springframework.core.io.ClassPathResource;

import java.io.*;

//定制的类装入器
public class TestCompile extends ClassLoader {
    String _compiler;
    String _classpath;

    public static void main(String[] args) throws IOException {
        new TestCompile();
    }

    public TestCompile() throws IOException {
        super(ClassLoader.getSystemClassLoader());

        String java_home = System.getenv("JAVA_HOME");

        String javac = java_home + "\\javac";

        System.out.println(javac);

        //默认编译器
        if (_compiler == null)

            _compiler = "D://study//Java/JDK//JDK1.8///bin//javac";

        _classpath = ".";
        String extraclasspath =
                "c://Program Files//Java//j2re1.4.2//lib//rt.jar";
        // = System.getProperty("calc.classpath");
        if (extraclasspath != null) {
            _classpath =
                    _classpath
                            + System.getProperty("path.separator")
                            + extraclasspath;
        }
        compile();
    }

    public void compile() throws IOException {
        String filename = "";
        String classname = "";

        ClassPathResource resource = new ClassPathResource("static");

        String path = resource.getFile().getAbsolutePath();

        System.out.println(path);


        try {
            //创建临时文件
            File javafile =
                    File.createTempFile("compiled_", ".java", new File("."));
            System.out.println(javafile.getAbsolutePath());

            filename = javafile.getName();
            System.out.println(filename);
            classname = filename.substring(0, filename.lastIndexOf("."));
            System.out.println(classname);
            generateJavaFile(javafile, classname);
            //编译文件
            invokeCompiler(javafile);
            //创建java类
            byte[] buf = readBytes(classname + ".class");
            Class c = defineClass(buf, 0, buf.length);
            try {
                c.newInstance();
            } catch (IllegalAccessException | InstantiationException e) {
                throw new RuntimeException(e.getMessage());
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    //生成java文件  
    void generateJavaFile(File javafile, String classname) throws IOException {
        FileOutputStream out = new FileOutputStream(javafile);
        String text =
                "public class "
                        + classname
                        + " {"
                        + " public int getCreater() {return 1;}"
                        + "}";
        out.write(text.getBytes());
        out.close();
    }

    //编译java文件  
    void invokeCompiler(File javafile) throws IOException {
        String[] cmd =
                {_compiler, "-classpath", _classpath, javafile.getName()};
        //执行编译命令
        //A1：
        Process process = Runtime.getRuntime().exec(cmd);
        try { //等待编译器结束
            process.waitFor();
        } catch (InterruptedException e) {
        }
        int val = process.exitValue();
        if (val != 0) {
            throw new RuntimeException("编译错误:" + "错误代码" + val);
        }
    }

    //以byte数组形式读入类文件  
    byte[] readBytes(String filename) throws IOException {
        File classfile = new File(filename);
        byte[] buf = new byte[(int) classfile.length()];
        FileInputStream in = new FileInputStream(classfile);
        in.read(buf);
        in.close();
        return buf;
    }

}