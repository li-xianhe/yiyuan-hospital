package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.lixianhe.dao.DrugMapper;
import com.lixianhe.pojo.Search;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 李显赫
 * @Date 2022-04-27 22:38
 */
@Service
public class DrugServiceImpl implements DrugService{

    @Autowired
    private DrugMapper drugMapper;

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;


    @Override
    public String getDrugs() {
        return JSON.toJSONString(drugMapper.getDrug());
    }

    @Override
    public String addES() throws IOException {
        List<Search> list = drugMapper.getNameId();
        System.out.println(list);
        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> data = new HashMap<>();
            data.put("name",list.get(i).getD_name());
            data.put("id",list.get(i).getD_id());
            System.out.println(data);
            IndexRequest indexRequest = new IndexRequest("hospital").id(String.valueOf(i+1)).source(data);
            client.index(indexRequest, RequestOptions.DEFAULT);
        }
        return "添加成功";
    }

    @Override
    public String getNameId() {
        return JSON.toJSONString(drugMapper.getNameId());
    }


}
