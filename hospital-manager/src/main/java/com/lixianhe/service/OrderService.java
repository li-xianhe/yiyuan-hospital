package com.lixianhe.service;

/**
 * @author 李显赫
 * @Date 2022-04-27 18:12
 */
public interface OrderService {
    String getOrder();
    String getResult();
}
