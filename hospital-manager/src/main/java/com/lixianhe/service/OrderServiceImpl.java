package com.lixianhe.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.lixianhe.dao.OrderMapper;
import com.lixianhe.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 李显赫
 * @Date 2022-04-27 18:16
 */
@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public String getOrder() {
        List<Order> list = orderMapper.getOrder();
        return JSON.toJSONString(list);
    }

    @Override
    public String getResult() {
        return JSON.toJSONString(orderMapper.getResult());
    }
}
