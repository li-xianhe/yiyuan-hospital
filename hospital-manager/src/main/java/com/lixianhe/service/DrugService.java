package com.lixianhe.service;

import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-27 22:37
 */
public interface DrugService {
    String getDrugs();
    String addES() throws IOException;
    String getNameId();
}
