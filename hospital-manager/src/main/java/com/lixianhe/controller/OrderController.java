package com.lixianhe.controller;

import com.lixianhe.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 李显赫
 * @Date 2022-04-27 18:06
 */
@RestController
public class OrderController {
    @Autowired
    private OrderServiceImpl orderService;

    @GetMapping("/order")
    public String getOrder(HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin","*");
        return orderService.getOrder();
    }

    @GetMapping("/result")
    public String getResult(HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin","*");
        return orderService.getResult();
    }
}
