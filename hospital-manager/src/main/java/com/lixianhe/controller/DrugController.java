package com.lixianhe.controller;

import com.lixianhe.service.DrugServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李显赫
 * @Date 2022-04-27 22:35
 */
@RestController
public class DrugController {

    @Autowired
    private DrugServiceImpl drugService;

    @GetMapping("/drug")
    public String getDrugs(HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin","*");
        return drugService.getDrugs();
    }

    @GetMapping("/es")
    public String addES(HttpServletResponse response) throws IOException {
        response.addHeader("Access-Control-Allow-Origin","*");
        return drugService.addES();
    }
}
