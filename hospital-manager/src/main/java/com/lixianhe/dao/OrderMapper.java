package com.lixianhe.dao;

import com.lixianhe.pojo.Order;
import com.lixianhe.pojo.Result;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 李显赫
 * @Date 2022-04-27 19:04
 */
@Mapper
public interface OrderMapper {
    List<Order> getOrder();
    List<Result> getResult();
}
