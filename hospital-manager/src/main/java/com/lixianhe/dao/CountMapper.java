package com.lixianhe.dao;

import com.lixianhe.pojo.DayCount;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 李显赫
 * @Date 2022-04-28 14:07
 */
@Mapper
public interface CountMapper {
    List<DayCount> RegisterCountByDay();

    List<DayCount> LoginCountByDay();

    List<DayCount> RegisterCountByWeek();

    List<DayCount> LoginCountByWeek();

    List<DayCount> RegisterCountByMonth();

    List<DayCount> LoginCountByMonth();
}
