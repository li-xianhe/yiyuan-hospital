package com.lixianhe.dao;

import com.lixianhe.pojo.Drug;
import com.lixianhe.pojo.Search;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 李显赫
 * @Date 2022-04-27 22:37
 */
@Mapper
public interface DrugMapper {
    List<Drug> getDrug();
    /**
     * 向ES中添加数据
     *
     * @return name和id数据对象的list
     */
    @Select("select d_id ,d_name from drug")
    List<Search> getNameId();
}
