package com.lixianhe.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.*;

import java.util.Date;

/**
 * @author 李显赫
 * @Date 2022-04-27 18:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private String o_id;
    @JSONField(serialzeFeatures= SerializerFeature.DisableCircularReferenceDetect)
    private Sick sick;
    private Integer o_is_h;
    private String o_time;
}
