package com.lixianhe.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-27 22:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private Integer r_id;
    @JSONField(serialzeFeatures= SerializerFeature.DisableCircularReferenceDetect)
    private Order order;
    private String r_res;
}
