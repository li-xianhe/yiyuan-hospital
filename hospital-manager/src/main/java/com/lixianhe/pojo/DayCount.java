package com.lixianhe.pojo;

import lombok.*;

/**
 * @author 李显赫
 * @Date 2022-04-28 14:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DayCount {
    private short year;
    private Byte month;
    private Byte week;
    private String day;
    private Integer count;
    private Double row;
}
