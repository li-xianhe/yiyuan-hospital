package com.lixianhe.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 李显赫
 * @Date 2022-04-08 21:58
 */
@ConfigurationProperties(prefix = "elasticsearch")
@Configuration
public class ElasticSearchConfig {

    private String host;
    private int port;

    public String getHost() {return host;}

    public void setHost(String host) {this.host = host;}

    public int getPort() {return port;}

    public void setPort(int port) {this.port = port;}

    // ES客户端对象
    @Bean
    public RestHighLevelClient restHighLevelClient(){
        HttpHost httpHost = new HttpHost(getHost(),getPort(),"http");
        return new RestHighLevelClient(RestClient.builder(httpHost));
    }
}
