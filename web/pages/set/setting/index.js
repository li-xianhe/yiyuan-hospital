// pages/set/setting/index.js
var app = getApp();
Page({
    data: {
        BaseFontSize: '',
        disabled: false,
        alter_pwd: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        if (!wx.getStorageSync("user") && !wx.getStorageSync("userinfo")){
            that.setData({
                disabled: true
            })
        }
        if (wx.getStorageSync("user")) {
            that.setData({
                alter_pwd: false
            })
        } else {
            that.setData({
                alter_pwd: true
            })
        }
    },
    onShow() {
        this.setData({
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    // 未登录
    noLogin(e) {
        if (!wx.getStorageSync("user") && !wx.getStorageSync("userinfo")){
            wx.showModal({
                title: '重要通知',
                content: '请您先登录！',
                showCancel: false,
                //showCancel: false//加上此句话取消键没有
                success(res) {
                    wx.switchTab({
                        url: "/pages/user/index"
                    })
                }
            })
        } else {
            if (e.currentTarget.id == "修改密码") {
                wx.navigateTo({
                    url: "/pages/set/set_change_pwd/index"
                })
            } else if (e.currentTarget.id == "意见反馈") {
                wx.navigateTo({
                    url: "/pages/set/feedback/index"
                })
            }
        }
    },
    //清除缓存，退出登录
    clear: function () {
        wx.clearStorageSync(); //清除缓存
        wx.showToast({
            title: '退出登录成功',
            icon: 'none',
            duration: 2000,
            success: function () {
                setTimeout(function () {
                    //跳转到首页，强制重启
                    wx.reLaunch({
                        url: '/pages/user/index',
                    })
                }, 2000);
            }
        })
    },
})