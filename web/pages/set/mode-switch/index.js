var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },

    // 点击大字版
    handleBig() {
        app.globalData.BaseFontSize = 35;
        wx.navigateBack({
            delta: 1
        });
    },
    // 点击标准版
    handleStand() {
        app.globalData.BaseFontSize = 28;
        wx.navigateBack({
            delta: 1
        });
    }
})