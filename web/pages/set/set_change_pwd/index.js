// pages/set/set_change_pwd/index.js
var app = getApp();

var password_is_error = false;
var password_same = false;
var oldpassword_is_error = false;
Page({
    data: {
        password: '',
        confirmPassword: '',
        oldpassword: '',
        // 旧密码是否正确
        isoldPwdsame: true,
        // 密码含有非法字符
        isPwdlegal: true,
        // 密码小于6位
        isPwdsix: true,
        // 密码不是由大写字母、小写字母、数字组成!
        isPwdmake: true,
        // 密码是否相同 
        isPwdsame: true,
        // 是否重置
        isDisabled: true,
        BaseFontSize: '',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onShow() {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        this.setData({
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    // 旧密码获得焦点
    bindoldPassword() {
        this.setData({
            isoldPwdsame: true,
        })
    },
    // 旧密码失去焦点
    oldpwdaway(e) {
        var openid = wx.getStorageSync("openid");
        var id = wx.getStorageSync("identity_id");
        const pwd = e.detail.value;
        this.setData({
            oldpassword: e.detail.value
        })
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/password/register',
                method: 'POST',
                data: {
                    "status": id,
                    "username": openid,
                    "password": pwd,
                },
                success: res => {
                    if (res.data == 0) {
                        that.setData({
                            isoldPwdsame: false
                        })
                    } else {
                        oldpassword_is_error = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 新密码获得焦点
    bindPasswordInput: function (e) {
        this.setData({
            password: e.detail.value,
            // 密码含有非法字符
            isPwdlegal: true,
            // 密码小于6位
            isPwdsix: true,
            // 密码不是由大写字母、小写字母、数字组成!
            isPwdmake: true
        });
    },
    // 新密码输入失去焦点
    pwdaway(e) {
        const pwd = e.detail.value;
        this.setData({
            password: e.detail.value
        })
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/password/legal',
                method: 'POST',
                data: {
                    "password": pwd,
                },
                success: res => {
                    if (res.data == -1) {
                        that.setData({
                            isPwdlegal: false
                            // isDisabled:true
                        })
                    } else if (res.data == -2) {
                        that.setData({
                            isPwdsix: false
                        })
                    } else if (res.data == 0) {
                        that.setData({
                            isPwdmake: false
                        })
                    } else {
                        password_is_error = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 确认密码获得焦点
    bindConfirmPasswordInput: function (e) {
        this.setData({
            isPwdsame: true,
        });
    },
    // 确认密码失去焦点
    inputrepwd(e) {
        this.setData({
            confirmPassword: e.detail.value,
        });
        const repwd = e.detail.value;
        var pwd = this.data.password;
        var that = this;
        if (e.detail.value.length == pwd.length) {
            wx.request({
                url: app.getPort(8086) + '/verify/password/same',
                method: 'POST',
                data: {
                    "password1": pwd,
                    "password2": repwd,
                },
                success: res => {
                    if (res.data == 0) {
                        that.setData({
                            isPwdsame: false
                        })
                    } else {
                        password_same = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 是否能提交
    handlecommit() {
        if (oldpassword_is_error && password_is_error && password_same) {
            this.setData({
                isDisabled: false
            })
        }
    },
    // 密码重置
    startReset: function () {
        var that = this;
        var id = wx.getStorageSync("identity_id");
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        var oldpassword = that.data.oldpassword;
        var password = that.data.password;
        wx.request({
            url: app.getPort(8086) + '/reset',
            method: 'POST',
            data: {
                "old_password": oldpassword,
                "new_password": password,
                "status": id,
                "username": openid,
            },
            success: function (res) {
                if (res.data == 1) {
                    wx.showModal({
                        title: '重置密码成功！',
                        // content: '重置密码成功！',
                        showCancel: false,
                        //showCancel: false//加上此句话取消键没有
                        success(res) {
                            wx.switchTab({
                                url: "/pages/set/setting/index"
                            })
                        }
                    });

                }
            }
        });
    },


})