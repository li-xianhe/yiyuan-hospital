var app = getApp();
var weatherApi = require('../../utils/weather.js');
Page({
    /**
     * 页面的初始数据
     */
    data: {
        swiper: {},
        collectNums: 0,
        city: '',
        num: '',
        temp: "",
        weather: "",
        weatherIcon: "",
        navH: 0,
        BaseFontSize: '',
        isDisabled: true,
        HealthTips: [],
        modalHidden: true,
        img: app.globalData.Global_imgUrl + '/images/background.png',
        images: [
            app.globalData.Global_imgUrl + '/icons/notice.png',
            app.globalData.Global_imgUrl + '/images/1.png',
            app.globalData.Global_imgUrl + '/images/2.png',
            app.globalData.Global_imgUrl + '/images/3.png',
            app.globalData.Global_imgUrl + '/images/7.png',
            app.globalData.Global_imgUrl + '/images/6.png',
            app.globalData.Global_imgUrl + '/images/4.png',
            app.globalData.Global_imgUrl + '/images/5.png',
            app.globalData.Global_imgUrl + '/images/8.png',
            app.globalData.Global_imgUrl + '/images/health.png',
            app.globalData.Global_imgUrl + '/video/sp.mp4',
            app.globalData.Global_imgUrl + '/images/depression.jpg',
        ]
    },
    // 接口要的参数
    QueryParams: {
        query: "",
        cid: "",
        pagenum: 1,
        pagesize: 8
    },
    // 总页数  全局用
    totalPages: 1,
    /**
     * 生命周期函数--监听页面加载
     */
    // 接口返回数据
    Drug: [],
    onLoad: function (options) {
        this.getswiper();
        this.getHealthTips();
        if (app.globalData.index_showModal == 0) {
            wx.showModal({
                title: '重要通知',
                content: '尊敬的患者朋友们：\r\n根据目前疫情防控形势，现对门诊患者就诊要求调整如下：\r\n1.所有呼吸科患者，就诊时须持48小时内核酸检测阴性报告。\r\n2..2022年4月11日起，最近14天内无大连市外旅居史且未到访过重点关注地区的人员，进入医院无需提供核酸阴性报告。最近14天内有大连市外（除外重点关注地区）旅居史的人员，需提供返连前后2次核酸检测报告，若无返连前核酸，需提供返连后间隔24小时2次核酸，最后一次核酸要在48小时内。\r\n3.从重点关注地区来（返）连不足21天人员，请到发热门诊排查（重点关注地区以就诊当日更新的名单为准 ，请您理解）。\r\n4.从境外入境不足28天的，请到发热门诊排查。',
                showCancel: false,
                success(res) {
                    app.globalData.index_showModal = 1;

                }
            })
        }
        this.getLocation();
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    onShow() {
        this.setData({
            navH: app.globalData.navHeight,
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    // 轮播图、公告
    getswiper() {
        var that = this;
        wx.request({
            url: app.getPort(8082) + '/index',
            method: 'GET',
            //成功返回的数据
            success: function (res) {
                that.setData({
                    swiper: res.data
                })
            }
        });
    },
    // 获取健康小技巧
    getHealthTips() {
        var that = this;
        wx.request({
            url: app.getPort(8086) + '/title',
            method: 'GET',
            //成功返回的数据
            success: function (res) {

                that.setData({
                    HealthTips: res.data
                })
            }
        });
    },
    // 下拉刷新事件
    onPullDownRefresh() {
        // 1重置数组
        this.setData({
            drug: []
        })
        // 2重置页码
        this.QueryParams.pagenum = 1;
        // 3重新发送请求
        this.getdrug();
    },
    // 点击搜索
    handlesearch() {
        wx.navigateTo({
            url: '/pages/search/index'
        })
    },
    // 获取地址、天气
    getLocation: function () {
        var that = this;
        let weatherIconURL = "https://xai-1251091977.cos.ap-chengdu.myqcloud.com/weather/";
        wx.getLocation({
            type: 'wgs84',
            success: function (res) {
                var latitude = res.latitude
                var longitude = res.longitude

                weatherApi.weatherRequest(longitude, latitude, {
                    success(res) {

                        that.setData({
                            weather: res,
                            weatherIcon: weatherIconURL + res.now.cond_code + 'd.png'
                        })
                    }
                });
            },
        })
    },
    // 未登录
    noLogin(e) {
        if (!wx.getStorageSync("user") && !wx.getStorageSync("userinfo")) {
            wx.showModal({
                title: '重要通知',
                content: '请您先登录！',
                showCancel: false,
                //showCancel: false//加上此句话取消键没有
                success(res) {
                    wx.switchTab({
                        url: "/pages/user/index"
                    })
                }
            })
        } else {
            if (e.currentTarget.id == "药品中心") {
                wx.navigateTo({
                    url: "/pages/goods_list/index"
                })
            } else if (e.currentTarget.id == "切换就诊人") {
                wx.navigateTo({
                    url: "/pages/NAT/shift_NATuser/index"
                })
            } else if (e.currentTarget.id == "报告查询") {
                wx.navigateTo({
                    url: "/pages/reports/NAT_report/index"
                })
            } else if (e.currentTarget.id == "核酸检测预约") {
                wx.navigateTo({
                    url: "/pages/NAT/NAT_order/index"
                })
            }
        }
    },
    // 点击获取二维码
    handleBinCode(e) {
        var that = this;

        var id = Number(e.currentTarget.id)
        wx.request({
            url: app.getPort(8086) + '/binCode',
            method: 'POST',
            //发送的数据
            data: {
                "id": id
            },
            // 最为关键
            responseType: 'arraybuffer',
            //成功返回的数据
            success: function (res) {

                var imgSrc = "data:image/jpeg;base64," + wx.arrayBufferToBase64(res.data); //二进制流转为base64编码

                that.setData({
                    modalHidden: false,
                    img: imgSrc,
                    title: e.currentTarget.dataset.title
                })
            },
        });
    },
    // 点击图片预览
    handlePrevewImage() {
        wx.previewImage({
            current: '',
            urls: [this.data.img],
        })
    },
    //点击取消
    modalCandel: function () {
        // do something
        this.setData({
            modalHidden: true
        })
    },
    // 点击确认
    modalConfirm: function () {
        // do something
        this.setData({
            modalHidden: true
        })
    }
})