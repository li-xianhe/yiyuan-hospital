var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        drugdetails: [],
        // 商品是否被收藏
        isCollect: false
    },
    onLoad: function (options) {
        const {
            cid
        } = options;
        this.getGoodsDetail(cid);
        this.getisCollect(cid);
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    // 定义全局变量，函数内部可以调用
    DrudInfo: {},
    // 获取详情数据
    getGoodsDetail(cid) {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8081) + '/drugs/id',
            method: 'POST',
            //成功返回的数据
            data: {
                "drugId": cid
            },
            success: function (res) {
                that.DrudInfo = res.data;
                that.setData({
                    drugdetails: res.data
                })
            }
        });
    },
    // 点击图片预览
    handlePrevewImage() {
        wx.previewImage({
            current: '',
            urls: [this.DrudInfo.d_img],
        })
    },

    // 商品是否被收藏
    getisCollect(id) {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8086) + '/collects/is',
            method: 'POST',
            //成功返回的数据
            data: {
                "drugId": id,
                "openid": openid
            },
            success: function (res) {
                if (res.data) {
                    that.setData({
                        isCollect: !res.data.c_is_delete
                    })
                }
            }
        })
    },

    // 点击 商品收藏图标
    handleCollect() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        if (openid) {
            wx.request({
                url: app.getPort(8086) + '/collects/_all',
                method: 'POST',
                data: {
                    "openid": openid
                },
                //成功返回的数据
                success: function (res) {
                    let collect = res.data;
                    // 2 判断 商品对象是否已收藏
                    let index = collect.findIndex(v => v.drug.d_id === that.data.drugdetails.d_id);
                    if (index === -1) {
                        //3  不存在 
                        wx.request({
                            url: app.getPort(8086) + '/collects',
                            method: 'POST',
                            data: {
                                "openid": openid,
                                "drugId": that.data.drugdetails.d_id,
                            },
                            //成功返回的数据
                            success: function (res) {
                                if (res.data == 1 || res.data == 2) {
                                    that.setData({
                                        isCollect: true
                                    })
                                    wx.showToast({
                                        title: '收藏成功！',
                                        icon: 'success',
                                        // true 防止用户 手抖 疯狂点击按钮 
                                        mask: true
                                    });
                                }
                            }
                        })
                    } else {
                        wx.request({
                            url: app.getPort(8086) + '/collects',
                            method: 'DELETE',
                            data: {
                                "openid": openid,
                                "drugId": that.data.drugdetails.d_id,
                            },
                            //成功返回的数据
                            success: function (res) {
                                that.setData({
                                    isCollect: false
                                })
                            }
                        })


                    }
                }
            })
        } else {
            wx.showModal({
                title: '重要通知',
                content: '请您先登录！',
                showCancel: false,
                //showCancel: false//加上此句话取消键没有
                success(res) {
                    wx.switchTab({
                        url: "/pages/user/index"
                    })
                }
            })
        }
    },
    // 点击 加入购物车
    handleCartAdd() {
        // 获取购物车数组
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8087) + '/carts/_all',
            method: 'POST',
            data: {
                "openid": openid
            },
            //成功返回的数据
            success: function (res) {
                let cart = res.data;
                // 2 判断 商品对象是否存在于购物车数组中
                let index = cart.findIndex(v => v.drug.d_id === that.data.drugdetails.d_id);
                if (index === -1) {
                    //3  不存在 第一次添加
                    that.DrudInfo.num = 1;
                    wx.request({
                        url: app.getPort(8087) + '/carts',
                        method: 'POST',
                        data: {
                            "openid": openid,
                            "drugId": that.data.drugdetails.d_id,
                            "count": that.DrudInfo.num
                        },
                        //成功返回的数据
                        success: function (res) {
                            if (res.data == 1 || res.data == 2) {
                                wx.showToast({
                                    title: '加入购物车成功！',
                                    icon: 'success',
                                    // true 防止用户 手抖 疯狂点击按钮 
                                    mask: true
                                });
                            }
                        }
                    })
                } else {
                    wx.showToast({
                        title: '已存在！',
                        icon: 'success',
                        // true 防止用户 手抖 疯狂点击按钮 
                        mask: true
                    });
                }
            }
        });
    },
    // 点击立即购买
    handleBuy() {
        app.globalData.checked_List = [{
            c_count: 1,
            d_img: this.data.drugdetails.d_img,
            d_name: this.data.drugdetails.d_name,
            d_price: this.data.drugdetails.d_price,
        }]
        app.globalData.totalPrice = this.data.drugdetails.d_price;
        wx.navigateTo({
            url: '/pages/payment/pay/index'
        });
    }

})