// pages/user/index.js
var app = getApp();
Page({

    data: {
        userinfo: {},
        collectNums: 0,
        user: {},
        days: 0,
        BaseFontSize: '',
        imgpath: '',
        images:[
            app.globalData.Global_imgUrl+'/images/kongbai.png',
            app.globalData.Global_imgUrl+'/images/NOlogin.jpg',
            app.globalData.Global_imgUrl+'/icons/setting.png',
            app.globalData.Global_imgUrl+'/icons/collect.png',
            app.globalData.Global_imgUrl+'/icons/cart.png',
            app.globalData.Global_imgUrl+'/icons/my_order.png',
            app.globalData.Global_imgUrl+'/icons/address.png',
            app.globalData.Global_imgUrl+'/images/NOlogin.jpg',
        ]
    },
    appId: 'wxd34f960e8de43d52',
    appSecret: '2e866c2e1ddf3ad7026450d9dde9d828',
    onShow() {
        var that = this;
        const user = wx.getStorageSync("user");
        const userinfo = wx.getStorageSync("userinfo");
        const collect = wx.getStorageSync("collect") || [];
        this.setData({
            userinfo,
            collectNums: collect.length,
            user: user,
            BaseFontSize: app.globalData.BaseFontSize,
            identity_id: wx.getStorageSync("identity_id")
        });

        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        this.getDays(openid);
        if (wx.getStorageSync("user").p_image) {
            that.setData({
                imgpath: wx.getStorageSync("user").p_image
            })
        } else {
            that.setData({
                imgpath: app.globalData.Global_imgUrl + '/images/resident.png'
            })
        }
    },
    onLoad: function () {
        var that = this;
        app.globalData.shift_address = 0;
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    // 获取天数
    getDays(openid) {
        var that = this;
        wx.request({
            url: app.getPort(8086) + '/login/time',
            method: 'POST',
            data: {
                "openid": openid
            },
            //成功返回的数据
            success: function (res) {
                that.setData({
                    days: res.data
                })
            }
        })
    },
    // 选择要更换的头像
    uploadimg: function () {
        var that = this;
        // 从本地相册选择图片或使用相机拍照。
        wx.chooseImage({
            // 最多可以选择的图片张数
            count: 1,
            sizeType: ["compressed"],
            //   选择图片的来源
            sourceType: ["album", "camera"],
            success: function (res) {
                var temppath = res.tempFilePaths;

                that.setData({
                    imgpath: temppath[0]
                });
                uploadimg(that, temppath[0]);
            },
        })
        //更换的头像传给后端
        function uploadimg(page, path) {
            wx.showToast({
                    icon: 'loading',
                    title: '正在上传',
                }),
                wx.uploadFile({
                    // 开发者服务器地址
                    url: 'http://192.168.0.115:8086/hospital/load',
                    filePath: path,
                    name: 'file',
                    formData: {
                        "openid": wx.getStorageSync("user").p_openid,
                    },
                    // 超时时间，单位为毫秒
                    timeout: 600000000,
                    header: {
                        "content-type": "multipart/form-data"
                    },
                    success: function (res) {

                        //这里直接用that大概是因为这个自定义的函数在上面uploadimg有了回调，而uploadimg函数
                        //已经定义了that，所以下面就直接使用that
                        that.setData({
                            imgname: '/images/tximg/' + res.data
                        })
                    },

                })
        }
    },
})