// pages/search_list/index.js

var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    search_List: [],
    drugs: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 发送给朋友、分享朋友圈
    app.onShareAppMessage();
    this.getdrugs();
  },

  getdrugs() {
    const drugs = wx.getStorageSync("drugs");
    this.setData({
      drugs: drugs,
      search_List: app.globalData.search_List
    })
  }
})