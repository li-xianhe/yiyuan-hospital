// pages/sleep_cost/index.js
let app = getApp();
var phone_error = false;
var isdetail_address = false;
var isname = false;
Page({
    data: {
        region: ['辽宁省', '大连市', '金州区'],
        province: "",
        city: "",
        area: "",
        isSubmit: false,
        name: "",
        phone: "",
        detail_address: "",
        // 手机号是否合法
        isphoneLegal: true,
        isDefault: true,
        isDisabled: true,
        // 查看原来是否添加过收货地址
        num: '',
        // 0时添加收货地址，1时更改收货地址
        add_or_alter: '',
        alter_address: [],
        hidden: true,
        InputValue: "",
        isChoice: '1', //设置默认选中
        labelList: [],
        labelchoiceID: '1',
        Detele_Label: false,
        Detele_Cancel: true
    },
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        app.globalData.shift_address = 0;
        // 如果是修改收货地址，保存按钮始终可以按
        if (app.globalData.alter_address == 1) {
            this.setData({
                isDisabled: false
            })
        }
        // 如果是修改收货地址，即add_or_alter=1时，input的value会带上要修改的信息
        this.setData({
            add_or_alter: app.globalData.alter_address
        })
        this.getAddress_alter();
    },
    onReady() {
        this.getLabel();
    },
    //  提交
    formSubmit: function (e) {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        let {
            name,
            phone,
            province,
            city,
            area,
            detail_address,
            isDefault,
        } = e.detail.value;
        this.setData({
            name,
            phone,
            province,
            city,
            area,
            detail_address,
            isDefault,
        })
        if (this.data.num == 0) {
            isDefault = true
        } else {
            isDefault = e.detail.value.isDefault
        }
        // 0时添加收货地址，1时更改收货地址
        if (this.data.add_or_alter == 1) {
            wx.request({
                url: app.getPort(8085) + '/address',
                method: 'PUT',
                data: {
                    "name": e.detail.value.name,
                    "phone": e.detail.value.phone,
                    "province": e.detail.value.province,
                    "city": e.detail.value.city,
                    "area": e.detail.value.area,
                    "detail": e.detail.value.detail_address,
                    "isDefault": isDefault,
                    "openid": openid,
                    "id": app.globalData.address_id,
                    "labelID": that.data.labelchoiceID
                },
                success: function (res) {
                    wx.redirectTo({
                        url: '/pages/shop_address/address_list/index'
                    })
                }
            })
        } else {
            wx.request({
                url: app.getPort(8085) + '/address',
                method: 'POST',
                data: {
                    "name": e.detail.value.name,
                    "phone": e.detail.value.phone,
                    "province": e.detail.value.province,
                    "city": e.detail.value.city,
                    "area": e.detail.value.area,
                    "detail": e.detail.value.detail_address,
                    "isDefault": isDefault,
                    "openid": openid,
                    "labelID": that.data.labelchoiceID
                },
                success: function (res) {
                    wx.navigateBack({
                        delta: 1,
                    })
                }
            })
        }
    },
    // 所在地是否改变
    bindRegionChange: function (e) {
        this.setData({
            region: e.detail.value,
        })
    },
    // 名字失去焦点
    awayName(e) {
        if (e.detail.value) {
            isname = true;
            this.handlecommit();
        }
    },
    // 手机号获得焦点
    handlephone() {
        this.setData({
            isphoneLegal: true,
            isDisabled: true
        })
    },
    // 手机号失去焦点
    awayphone(e) {
        var that = this;
        if (e.detail.value) {
            wx.request({
                url: app.getPort(8086) + '/verify/phone',
                method: 'POST',
                //发送的数据
                data: {
                    "phone": e.detail.value
                },
                //成功返回的数据
                success: function (res) {
                    if (res.data == 0) {
                        that.setData({
                            isphoneLegal: false,
                        })
                    } else {
                        phone_error = true;
                        that.handlecommit();
                    }
                }
            });
        }
    },
    // 详细地址失去焦点
    awaydetail(e) {
        if (e.detail.value) {
            isdetail_address = true;
            this.handlecommit();
        }
    },
    // 是否能提交
    handlecommit() {
        if (isname && phone_error && isdetail_address) {
            this.setData({
                isDisabled: false
            })
        }
    },
    // 获取需要修改的地址
    getAddress_alter() {
        var id = app.globalData.address_id;
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/address/_all',
            method: 'POST',
            //发送的数据
            data: {
                "openid": openid
            },
            //成功返回的数据
            success: function (res) {
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].a_id == id) {
                        that.setData({
                            alter_address: res.data[i],
                            isChoice: res.data[i].label.l_id,
                            labelchoiceID: res.data[i].label.l_id,
                            num: res.data.length
                        })
                    }
                }
            }
        });
    },
    // 标签---------------------------------------------------------------------------------
    // 获取标签
    getLabel() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/labels/_all',
            method: 'POST',
            //发送的数据
            data: {
                "openid": openid,
            },
            //成功返回的数据
            success: function (res) {
                that.setData({
                    labelList: res.data
                })
            }
        });
    },
    itemChoice(e) {
        let id = e.currentTarget.id;
        this.setData({
            isChoice: id,
            labelchoiceID: e.currentTarget.id
        })
    },
    //   点击添加
    addinput(e) {
        this.setData({
            hidden: false,
        });
    },
    // 输入
    bindValue(e) {
        this.setData({
            InputValue: e.detail.value
        })
    },
    // 点击确定
    onInputValue(e) {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/labels',
            method: 'POST',
            //发送的数据
            data: {
                "openid": openid,
                "labelName": that.data.InputValue
            },
            //成功返回的数据
            success: function (res) {
                that.setData({
                        hidden: true,
                    }),
                    wx.redirectTo({
                        url: '/pages/shop_address/add_address/index'
                    })
            }
        });
    },
    // 点击取消
    onCancel(e) {
        this.setData({
            hidden: true,
        })
    },
    //   点击-
    Sublabel(e) {
        this.setData({
            Detele_Label: true,
            Detele_Cancel: false
        })
    },
    //   点击取消
    handleCancel() {
        this.setData({
            Detele_Label: false,
            Detele_Cancel: true
        })
    },
    // 点击删除标签
    handleDetele(e) {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/labels',
            method: 'DELETE',
            //发送的数据
            data: {
                "openid": openid,
                "labelID": e.currentTarget.dataset.id
            },
            //成功返回的数据
            success: function (res) {
                if (res.data == 1) {
                    that.onReady();
                }
            }
        });
    }
})