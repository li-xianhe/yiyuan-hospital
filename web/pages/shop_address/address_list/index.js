// pages/shop_adress/adress_list/index.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        address_list: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    onShow() {
        this.getAddress();
    },
    // 点击添加收货地址
    handleadd() {
        app.globalData.alter_address = 0;
        wx.navigateTo({
            url: '/pages/shop_address/add_address/index'
        })
    },
    // 修改默认
    radioChange(e) {
        var that = this;
        var id = e.detail.value;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/address/default',
            method: 'PUT',
            //发送的数据
            data: {
                "openid": openid,
                "id": id
            },
            //成功返回的数据
            success: function (res) {
                if (app.globalData.shift_address == 1) {
                    wx.redirectTo({
                        url: '/pages/payment/pay/index'
                    })
                } else {
                    wx.redirectTo({
                        url: '/pages/shop_address/address_list/index'
                    })
                }
            }
        });
    },
    // 点击修改
    handlereset(e) {
        app.globalData.alter_address = 1;
        app.globalData.address_id = e.currentTarget.id;
        wx.navigateTo({
            url: '/pages/shop_address/add_address/index'
        })
    },
    // 获得地址列表
    getAddress() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/address/_all',
            method: 'POST',
            //发送的数据
            data: {
                "openid": openid
            },
            //成功返回的数据
            success: function (res) {

                that.setData({
                    address_list: res.data
                })
            }
        });
    },
    // 删除
    handleDelete(e) {
        var that = this;
        var id = e.currentTarget.id;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/address',
            method: 'DELETE',
            data: {
                "id": id,
                "openid": openid
            },
            success: function (res) {
                wx.redirectTo({
                    url: '/pages/shop_address/address_list/index'
                })
            }
        })

    },
})