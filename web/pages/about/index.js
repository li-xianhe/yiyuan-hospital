// 引入用来发送请求的方法

var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        details: {},
        BaseFontSize: '',
        Date:'',
        images:[
            app.globalData.Global_imgUrl+'/images/favicon.png',
            app.globalData.Global_imgUrl+'/images/yellow.jpg',
            app.globalData.Global_imgUrl+'/icons/notice.png',
            app.globalData.Global_imgUrl+'/video/Chinese_introduce.mp4',
            app.globalData.Global_imgUrl+'/video/English_introduce.mp4',
            app.globalData.Global_imgUrl+'/images/background.png',
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onShow() {
        this.getdetails();
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        this.setData({
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    // 获取详情信息
    getdetails() {
        var that = this;
        wx.request({
            url: app.getPort(8084) + '/briefs',
            method: 'GET',
            //成功返回的数据
            success: function (result) {
                var date = new Date(result.data.b_create_date_time);
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                m = m < 10 ? ('0' + m) : m;
                var d = date.getDate();
                d = d < 10 ? ('0' + d) : d;
                var h = date.getHours();
                h = h < 10 ? ('0' + h) : h;
                var minute = date.getMinutes();
                var second = date.getSeconds();
                minute = minute < 10 ? ('0' + minute) : minute;
                second = second < 10 ? ('0' + second) : second;
                var date = y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
                that.setData({
                    details: result.data,
                    Date: date
                })
            }
        });
    },
    //   打电话
    call() {
        let phone = this.data.details.b_phone;
        wx.makePhoneCall({
            phoneNumber: phone
        })
    },
    // 点击医院地址后面的图标
    address(){
        wx.navigateTo({
          url: '/pages/map/index',
        })
    }

})