//index.js
//获取应用实例
var app = getApp()
var tab = 0;
var Min_Price = 0;
var Max_Price = 0;
Page({
    data: {
        /**
         * 页面配置
         */
        winWidth: 0,
        winHeight: 0,
        // 下方页码
        maxPages: 0, //最大页码
        pagesNum: 1,
        tyindex: 1,
        frontPage: false,
        lastPage: false,
        // tab切换
        currentTab: 0,
        // 当前页数
        currentPage: 1,
        // 销量中的药品
        sales: [],
        // 综合中的药品
        synthesis: [],
        count: 0,
        // 价格中的药品
        price: [],
        // 下方页数是否存在
        isPages: false,
    },
    onLoad: function () {
        var that = this;
        /**
         * 获取系统信息
         */
        wx.getSystemInfo({
            success: function (res) {

                that.setData({
                    winWidth: res.windowWidth,
                    winHeight: 2300
                });
            }
        });
        that.getsynthesis(1);
        that.getCount();
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    /**
     * 滑动切换tab
     */
    bindChange: function (e) {
        var that = this;
        that.setData({
            currentTab: e.detail.current
        });
    },
    /**
     * 点击tab切换
     */
    swichNav: function (e) {
        var that = this;

        var that = this;
        tab = e.target.dataset.current;
        if (e.target.dataset.current == 0) {
            that.getsynthesis(1);
            that.setData({
                isPages: false,
                pagesNum: 1
            })
        } else if (e.target.dataset.current == 2) {
            that.getSales(1);
            that.setData({
                isPages: false,
                pagesNum: 1
            })
        } else {
            that.setData({
                isPages: true,
            })
        }
        if (this.data.currentTab === e.target.dataset.current) {
            return false;
        } else {
            that.setData({
                currentTab: e.target.dataset.current
            })
        }
    },
    //  销量
    getSales(page) {
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/sell/sort/' + page,
            method: 'GET',
            //成功返回的数据
            success: function (res) {
                that.setData({
                    sales: res.data
                })
                var num = Math.ceil(res.data.length / 2)
                var query = wx.createSelectorQuery(); //创建节点选择器
                query.select('#zumn').boundingClientRect(function (rect) {
                    that.setData({
                        zumn1: rect.height,
                        winHeight: rect.height * (num + 1)
                    });
                }).exec();
            }
        });
    },
    // 价格
    getPrice_num(min, max) {
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/drugs/price',
            method: 'POST',
            data: {
                "min": min,
                "max": max
            },
            //成功返回的数据
            success: function (res) {
                that.setData({
                    price: res.data
                })
                var num = Math.ceil(res.data.length / 2)
                var query = wx.createSelectorQuery(); //创建节点选择器
                query.select('#zumn').boundingClientRect(function (rect) {
                    that.setData({
                        zumn1: rect.height,
                        winHeight: rect.height * num + rect.height * 1.5
                    });
                }).exec();
            }
        });

    },
    // 输入最低价
    Min_input(e) {
        Min_Price = e.detail.value;
        var min = Number(Min_Price);
        if (min < 0) {
            Min_Price = '0';
        }
    },
    Max_input(e) {
        Max_Price = e.detail.value;
    },
    min_max() {
        var min = Number(Min_Price);
        var max = Number(Max_Price);
        if (min <= max && max > 0) {
            this.getPrice_num(Min_Price, Max_Price);
        }
    },
    // 综合
    getsynthesis(page) {
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/drugs/limit/' + page,
            method: 'GET',
            //成功返回的数据
            success: function (res) {
                that.setData({
                    synthesis: res.data
                })
            }
        });

    },
    // 获取药品数量
    getCount() {
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/drugs/count',
            method: 'GET',
            //成功返回的数据
            success: function (res) {

                var pages = Math.ceil(res.data / 20)

                that.setData({
                    maxPages: pages,
                    lastPage: pages > 6 ? true : false
                })
            }
        });
    },
    // 点击页码，跳转页面
    pagesFn: function (e) {
        var that = this;
        let type = e.currentTarget.dataset.type;
        let number = e.currentTarget.dataset.number;
        let _that = this;
        if (typeof type == "string") { //上下页
            if (type == "previous_page") { //上一页
                if (_that.data.pagesNum - 1 < 1) {
                    return false
                }
                if ((_that.data.pagesNum - 1) % 6 == 0) {
                    if ((_that.data.pagesNum - 7) <= 1) {
                        this.setData({
                            lastPage: true,
                            frontPage: false,
                        })
                    }
                    _that.setData({
                        tyindex: _that.data.pagesNum - 6,
                        pagesNum: _that.data.pagesNum - 1,
                    })
                } else {
                    _that.setData({
                        pagesNum: _that.data.pagesNum - 1,
                    })
                }
            } else if (type == "next_page") { //下一页
                if (_that.data.pagesNum + 1 > _that.data.maxPages) {
                    return false
                }
                if (_that.data.pagesNum % 6 == 0) {

                    if ((_that.data.pagesNum + 6) >= _that.data.maxPages) {
                        this.setData({
                            lastPage: false,
                            frontPage: true,
                        })
                    }
                    _that.setData({
                        tyindex: _that.data.pagesNum + 1,
                        pagesNum: _that.data.pagesNum + 1,
                    })
                } else {
                    _that.setData({
                        pagesNum: _that.data.pagesNum + 1,
                    })
                }
            } else if (type == "start") { //第一页
                _that.setData({
                    pagesNum: 1,
                    tyindex: 1,
                    lastPage: true,
                    frontPage: false,
                })
            } else { //最后一页
                if ((_that.data.maxPages % 6) == 0) {
                    _that.setData({
                        pagesNum: _that.data.maxPages,
                        tyindex: _that.data.maxPages - 5,
                        lastPage: false,
                        frontPage: true,
                    })
                } else {
                    _that.setData({
                        pagesNum: _that.data.maxPages,
                        tyindex: _that.data.maxPages + 1 - (_that.data.maxPages % 6),
                        lastPage: false,
                        frontPage: true,
                    })
                }
            }
        } else {
            _that.setData({
                pagesNum: number
            })
        }

        if (tab == 0) {
            that.getsynthesis(that.data.pagesNum);
        } else if (tab == 2) {
            that.getSales(that.data.pagesNum)
        }

        // this.DataFn()可在此调用数据函数
    },

})