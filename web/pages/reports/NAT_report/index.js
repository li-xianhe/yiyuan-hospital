var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        NATreports: [],
        // 所有订单时间
        times: [],
        // 展示的报告时间（由于获取的是时间戳，需转换）
        time: '',
        // 要查询的时间
        section_time: '',
        orders: [],
        image: app.globalData.Global_imgUrl + '/images/kongkong.jpg',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        if (options.cid) {
            this.getNATorder(openid, options.cid);
        } else {
            this.getNATorder(openid);
        }
    },
    //   获取订单
    getNATorder(openid, cid) {
        var that = this;
        var times = that.data.orders;
        wx.request({
            url: app.getPort(8083) + "/orders",
            method: 'POST',
            //成功返回的数据
            data: {
                "openid": openid,
            },
            success: function (res) {
                res.data.forEach((r) => {
                    var date = new Date(Number(r.o_time));
                    var y = date.getFullYear();
                    var m = date.getMonth() + 1;
                    m = m < 10 ? ('0' + m) : m;
                    var d = date.getDate();
                    d = d < 10 ? ('0' + d) : d;
                    var h = date.getHours();
                    h = h < 10 ? ('0' + h) : h;
                    var minute = date.getMinutes();
                    var second = date.getSeconds();
                    minute = minute < 10 ? ('0' + minute) : minute;
                    second = second < 10 ? ('0' + second) : second;
                    var date = y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
                    r.o_time = date
                    times.unshift(date);
                })
                that.setData({
                    orders: res.data,
                    times: times,
                    section_time: res.data[0].o_time
                })
                var index = res.data.length - 1;
                if (cid) {
                    var id = cid;
                } else {
                    var id = res.data[index].o_id;
                }
                that.getNATreport(id);
            }
        });
    },
    //   获取报告
    getNATreport(oid) {
        var that = this;
        wx.request({
            url: app.getPort(8083) + "/orders/orderMsg",
            method: 'POST',
            //成功返回的数据
            data: {
                "o_id": oid
            },
            success: function (res) {
                if (res.data) {
                    var date = new Date(Number(res.data.order.o_time));
                    var y = date.getFullYear();
                    var m = date.getMonth() + 1;
                    m = m < 10 ? ('0' + m) : m;
                    var d = date.getDate();
                    d = d < 10 ? ('0' + d) : d;
                    var date1 = y + '-' + m + '-' + d;
                    var h = date.getHours();
                    h = h < 10 ? ('0' + h) : h;
                    var minute = date.getMinutes();
                    var second = date.getSeconds();
                    minute = minute < 10 ? ('0' + minute) : minute;
                    second = second < 10 ? ('0' + second) : second;
                    var date2 = y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
                    res.data.order.o_time = date2;
                    that.setData({
                        NATreports: res.data,
                        time: date1
                    })
                }
            }
        });
    },
    // 时间改变
    bindPickerChange: function (e) {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        let index = that.data.orders.findIndex(v => v.o_time === that.data.times[e.detail.value]);
        var oid = that.data.orders[index].o_id;
        that.getNATreport(openid, oid);
    },
})