var app = getApp();
Page({
    data: {
        userinfoadd: {},
        cardNumber: '',
        phone: '',
        name: '',
        images: [
            app.globalData.Global_imgUrl + '/icons/NAT.png',
            app.globalData.Global_imgUrl + '/images/nat.png',
            app.globalData.Global_imgUrl + '/icons/add.png'
        ]
    },
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        if (!wx.getStorageSync("user") && !wx.getStorageSync("userinfo")){
            wx.showModal({
                title: '重要通知',
                content: '请您先登录！',
                showCancel: false,
                //showCancel: false//加上此句话取消键没有
                success(res) {
                    wx.switchTab({
                        url: "/pages/user/index"
                    })
                }
            })
        }
        app.globalData.shift_order = 1;
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
    },
    onShow() {
        this.getNATuserDetail();
    },
    //   获取就诊人
    getNATuserDetail() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8086) + '/sicks/' + openid,
            method: 'GET',
            success: function (res) {
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].s_default == true) {
                        that.setData({
                            userinfoadd: res.data[i]
                        })
                        const e = res.data[i].s_licence;
                        const phone = res.data[i].s_phone;
                        var arr = '';
                        var pho = '';
                        var a = '*';
                        for (var j = 0; j < e.length; j++) {
                            if (j >= 4 && j < 14) {
                                arr += a;
                            } else {
                                arr += e[j];
                            }
                        }
                        for (var j = 0; j < phone.length; j++) {
                            if (j >= 3 && j < 7) {
                                pho += a;
                            } else {
                                pho += phone[j];
                            }
                        }
                        that.setData({
                            cardNumber: arr,
                            phone: pho
                        })
                    }
                }
            }
        })
    },
    // 点击去预约
    handleorder(e) {
        wx.redirectTo({
            url: "/pages/NAT/NAT_massage/index?name=" + e.currentTarget.id,
        })
    },
    // 点击切换就诊人
    handleshift() {
        wx.navigateTo({
            url: '/pages/NAT/shift_NATuser/index'
        })
    }
})