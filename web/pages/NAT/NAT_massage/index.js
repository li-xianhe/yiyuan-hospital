var app = getApp();
Page({
    data: {
        usercard: '',
        birth: '',
        sex: '',
        name: '',
        isDisabled: true,
        userinfoadd: {},
        date: '',
        startdate: '',
        NAT_type: "混采",
        price: 7,
        // 核酸检测选择是否隐藏
        isHidden: true,
        nowDate: ''
    },
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        var that = this;
        this.getDays();
        if (app.globalData.shift_order == 0) {
            that.setData({
                isHidden: true
            })
        } else {
            that.setData({
                isHidden: false
            })
        }
        if (!wx.getStorageSync("user") && !wx.getStorageSync("userinfo")){
            wx.showModal({
                title: '重要通知',
                content: '请您先登录！',
                showCancel: false,
                //showCancel: false//加上此句话取消键没有
                success(res) {
                    wx.switchTab({
                        url: "/pages/user/index"
                    })
                }
            })
        } else {
            wx.showModal({
                title: '挂号及就诊须知',
                content: '1.4月9日至4月10日就诊及陪同人员14天内无大连市域外旅居的，长春路院区及联合路院区门诊就诊须持就诊前48小时内核酸检测阴性报告；金普院区门诊就诊须持就诊前24小时内核酸检测阴性报告。\r\n2.4月11日始长春路院区以下科室就诊须持48小时内核酸检测阴性报告：呼吸内科、眼科、耳鼻喉科、口腔科、儿科、内镜、肺功能、碳14呼气试验及门诊气道雾化等需摘口罩的操作治疗项目。就诊及陪同人员14天内无大连市域外旅居史的，金普院区门诊就诊须持就诊前24小时内核酸检测阴性报告\r\n3．大连市域外来（返）连不足14天的就诊及陪同人员均需提供返连前后2次核酸检测报告（最近一次为来院就诊前48小时内）；若无返连前核酸报告，则需提供返连后2次核酸检测报告（最近一次为来院就诊前48小时内，间隔24小时）。\r\n4．就诊患者请务必携带身份证，本人预约挂号凭证、动态国务院行程卡，扫描辽事通健康码过闸机，配合体温测量并完成疫情相关流行病学调查后入院。\r\n5．非必要不陪诊，老年或行动不便患者、儿童、孕妇限一名亲友陪同。\r\n6．医院实行全预约挂号，取消现场挂号窗\r\n 7．挂号就诊当日当次有效，超出当次出诊时间复诊需要重新挂号。\r\n8．请在预约挂号时段提前30分钟于候诊区等候。患者就诊时长不同，故当日就诊时间以排队序号为准。\r\n9．预约医生如有特殊情况需要变更或停止出诊的，医院会提前电话通知，请您以医院电话通知为准。\r\n10．门诊全面实行电子病历，请您在就诊后到自助机打印电子病历或诊断书，诊断书须到挂号窗口加盖印章。\r\n11．预约挂号成功后请您按时就诊，如不能正常就诊，请于就诊当日7点之前自行取消挂号。',
                showCancel: false
            })
        }
        var that = this;
        var usercard = that.data.usercard;

        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        if (!options.nameData) {
            wx.request({
                url: app.getPort(8086) + '/sicks/' + openid,
                method: 'GET',
                success: function (result) {
                    for (var i = 0; i < result.data.length; i++) {
                        if (result.data[i].s_default == true) {
                            that.setData({
                                name: result.data[i].s_name
                            })
                        }
                    }
                }
            })
        } else {
            that.setData({
                name: options.nameData
            })
        }
        this.getNATuserDetail();
    },
    // 获取详细信息
    getNATuserDetail() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8086) + '/sicks/' + openid,
            method: 'GET',
            success: function (res) {
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].s_name == that.data.name) {
                        that.setData({
                            userinfoadd: res.data[i]
                        })
                        that.getInfo(res.data[i].s_licence);
                    }
                }
            }
        })
    },
    // 根据身份证获取信息
    getInfo(UUserCard) {
        var that = this;
        var birth = UUserCard.substring(6, 10) + "-" + UUserCard.substring(10, 12) + "-" + UUserCard.substring(12, 14);
        var sex = that.data.sex;
        if (parseInt(UUserCard.substr(16, 1)) % 2 == 1) {
            sex = '男';
        } else {
            sex = '女'
        }
        that.setData({
            birth: birth,
            sex: sex
        })
    },
    // 是否同意
    isagree(e) {
        if (e.detail.value == true) {
            this.setData({
                isDisabled: false
            })
        } else {
            this.setData({
                isDisabled: true
            })
        }
    },
    // 点击提交
    handlebutton() {
        var that = this;
        app.globalData.shift_doctors = 1
        if (app.globalData.shift_order == 0) {
            wx.navigateTo({
                url: '/pages/doctor_all/offices/index'
            })
        } else {
            // 是否混采
            if (that.data.NAT_type === "混采") {
                var is = true;
            } else {
                var is = false;
            }
            wx.request({
                url: app.getPort(8083) + '/orders/message',
                method: 'POST',
                //发送的数据
                data: {
                    "sid": that.data.userinfoadd.s_id,
                    "time": that.data.nowDate,
                    "is": is,
                },
                //成功返回的数据
                success: function (res) {
                    app.globalData.totalPrice = that.data.price;
                    wx.redirectTo({
                        url: '/pages/payment/countdown/index'
                    })
                }
            });
        }

    },
    // 日期补0
    paddingZero: function (n) {
        if (n < 10) {
            return '0' + n;
        } else {
            return n;
        }
    },
    // 日期开始
    getDays() {
        var that = this;
        let myDate = new Date();
        // 转化为时间戳
        let nowDate = Date.parse(myDate);
        let year = myDate.getFullYear();
        let month = myDate.getMonth() + 1;
        let dates = myDate.getDate();
        let date = year + '-' + that.paddingZero(month) + '-' + that.paddingZero(myDate.getDate() + 1);
        that.setData({
            startdate: date,
            date: date,
            nowDate: nowDate
        })

    },
    // 日期改变
    bindDateChange: function (e) {
        this.setData({
            date: e.detail.value
        })
    },
    // 核酸检测类型改变，默认混采
    changeType(e) {
        if (e.detail.value === "混采") {
            var price = 7;
        } else {
            var price = 40;
        }
        this.setData({
            NAT_type: e.detail.value,
            price: price
        })
    }
})