var app = getApp();
var licence_error = false;
var phone_error = false;
var agree = false;
var isname = false;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        name: "",
        licence: "",
        phone: "",
        gender: "",
        isDefault: true,
        isAgree: false,
        isDisabled: true,
        // 身份证是否合法
        islicenceLegal: true,
        // 手机号是否合法
        isphoneLegal: true,
        num: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        var that = this;
        wx.request({
            url: app.getPort(8086) + "/sicks/" + openid,
            method: 'GET',
            //成功返回的数据
            success: function (res) {
                that.setData({
                    num: res.data.length
                })
            }
        });
    },
    // 点击提交
    formSubmit: function (e) {
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        const user = e.detail.value;
        let {
            name,
            licence,
            phone,
            gender,
            isDefault,
        } = e.detail.value;
        this.setData({
            name,
            licence,
            phone,
            gender,
            isDefault
        })
        if (this.data.num == 0) {
            isDefault = true
        } else {
            isDefault = e.detail.value.isDefault
        }
        wx.request({
            url: app.getPort(8086) + '/sicks',
            method: 'POST',
            data: {
                "gender": e.detail.value.gender,
                "name": e.detail.value.name,
                "licence": e.detail.value.licence,
                "phone": e.detail.value.phone,
                "isDefault": isDefault,
                "openid": openid
            },
            success: function (res) {
                if (res.data == 1) {
                    wx.showModal({
                        title: '添加成功！',
                        showCancel: false, //加上此句话取消键没有
                        //点击按钮触发
                        success(res) {
                            wx.navigateBack({
                                delta: 1
                            });
                        }
                    })
                }
            }
        })
    },
    // 是否点击同意
    isagree(e) {
        var that = this;
        if (e.detail.value == true) {
            agree = true;
            that.handlecommit();
        } else {
            this.setData({
                isDisabled: true
            })
        }
    },
    // 点击协议触发
    handlehttp() {
        wx.showModal({
            title: '《患者移动服务软件用户使用协议》',
            content: '1、软件上为您的便利而提供的外部链接，该链接所指向网页之所有内容，均系该网页所属第三方软件的所有者制作和提供（以下“第三方网页”）。第三方网页并非也不反映软件之任何意见和主张，也不表示软件同意或支持该第三方网页上的任何内容、主张或立场。软件对第三方网页中内容之合法性、准确性、真实性、适用性、安全性和完整性等概不承担任何负责。任何单位或个人如需要第三方网页中内容（包括资讯、资料、消息、产品或服务介绍、报价等），并欲据此进行交易或其他行为前，应慎重辨别这些内容的合法性、准确性、真实性、适用性、完整性和安全性（包括下载第三方网页中内容是否会感染电脑病毒），并采取谨慎的预防措施。如您不确定这些内容是否合法、准确、真实、实用、完整和安全，建议您先咨询专业人士。\r\n2、任何单位或者个人因相信、使用第三方网页中信息、服务、产品等内容，或据此进行交易等行为，而引致的人身伤亡、财产毁损（包括因下载而感染电脑病毒）、名誉或商誉诽谤、版权或知识产权等权利的侵犯等事件，及因该等事件所造成的损害后果，软件概不承担任何法律责任。无论何种原因，软件不对任何非与软件直接发生的交易和行为承担任何直接、间接、附带或衍生的损失和责任。\r\n十七、网络服务的内容所有权软件提供网络服务的内容包括：文字、软件、声音、照片、视频、录像、图表、网页中的全部内容；电子邮件中的全部内容；软件为您提供的其他信息。所有这些信息均受版权、商标、标签和其他财产所有权法律的保护。未经相关权利人同意，上述资料均不得在任何媒体直接或间接发布、播放、出于播放或发布目的而改写或再发行，或者被用于其他任何商业目的。所有这些资料或资料的任何部分仅可作为私人和非商业用途而保存在某台计算机内。软件的所有内容版权归原文作者和软件共同所有，任何人需要转载软件的内容，必须获得原文作者或软件的明确授权。',
        })
    },
    // 名字失去焦点
    awayNmae(e) {
        if (e.detail.value) {
            isname = true;
            this.handlecommit();
        }
    },
    // 身份证号检验
    // 身份证号获得焦点
    handleLicence() {
        this.setData({
            islicenceLegal: true,
            isDisabled: true
        })
    },
    // 身份证号失去焦点
    awayLicence(e) {
        var that = this;
        if (e.detail.value) {
            wx.request({
                url: app.getPort(8086) + '/verify/licence',
                method: 'POST',
                //发送的数据
                data: {
                    "licence": e.detail.value
                },
                //成功返回的数据
                success: function (res) {
                    if (res.data == 0) {
                        that.setData({
                            islicenceLegal: false,
                        })
                    } else {
                        licence_error = true;
                        that.handlecommit();
                    }
                }
            });
        }
    },
    // 手机号获得焦点
    handlephone() {
        this.setData({
            isphoneLegal: true,
            isDisabled: true
        })
    },
    // 手机号失去焦点
    awayphone(e) {
        var that = this;
        if (e.detail.value) {
            wx.request({
                url: app.getPort(8086) + '/verify/phone',
                method: 'POST',
                //发送的数据
                data: {
                    "phone": e.detail.value
                },
                //成功返回的数据
                success: function (res) {
                    if (res.data == 0) {
                        that.setData({
                            isphoneLegal: false,
                        })
                    } else {
                        phone_error = true;
                        that.handlecommit();
                    }
                }
            });
        }
    },
    // 是否能提交
    handlecommit() {
        if (isname && phone_error && licence_error && agree) {
            this.setData({
                isDisabled: false
            })
        }
    },
})