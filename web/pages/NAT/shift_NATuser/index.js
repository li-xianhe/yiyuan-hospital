var app = getApp();
Page({
    data: {
        NATuserDetail: [],
        num: 0,
        isMoren: true,
        s_licence: '',
        // 是否点击管理按钮
        isrun: false,
        deletegole: [],
        // 取消按钮是否隐藏
        isreset: true,
        image:app.globalData.Global_imgUrl+'/icons/change.png',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    onShow(){
        this.getNATuserDetail();
    },
    //   获取就诊人信息
    getNATuserDetail() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8086) + '/sicks/' + openid,
            method: 'GET',
            success: function (res) {
                that.setData({
                    NATuserDetail: res.data,
                    num: res.data.length
                })
            }
        })
    },
    // radio选中事件
    radioChange(e) {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        for (var i = 0; i < that.data.NATuserDetail.length; i++) {
            if (that.data.NATuserDetail[i].s_name == e.detail.value) {
                that.setData({
                    s_licence: that.data.NATuserDetail[i].s_licence
                })
                wx.request({
                    url: app.getPort(8086) + '/sicks/default',
                    method: 'PUT',
                    //发送的数据
                    data: {
                        "openid": openid,
                        "licence": that.data.NATuserDetail[i].s_licence
                    },
                });
            }
        }
    },
    // 点击铅笔修改信息
    handlealter(e) {
        wx.redirectTo({
            url: '/pages/NAT/alter_NATuser/index?licence=' + e.currentTarget.id
        })
    },
    // 点击添加事件
    handleadd() {
        if (this.data.num < 5) {
            wx.navigateTo({
                url: '/pages/NAT/add_NATinfo/index'
            })
        }
    },
    // 点击管理按钮
    handlerun() {
        this.setData({
            isrun: true,
            isreset: false
        })
    },
    // 点击取消按钮
    handlereset() {
        this.setData({
            isrun: false,
            isreset: true
        })
    },
    // 选择需要删除的
    deleteChange(e) {
        this.setData({
            deletegole: e.detail.value
        })
    },
    // 点击删除就诊人
    handledelete() {
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        for (var i = 0; i < this.data.deletegole.length; i++) {
            var licence = this.data.deletegole[i];
            wx.request({
                url: app.getPort(8086) + '/sicks',
                method: 'DELETE',
                //发送的数据
                data: {
                    "openid": openid,
                    "licence": licence
                },
            });
        }
        wx.redirectTo({
            url: '/pages/NAT/shift_NATuser/index'
        })
    }
})