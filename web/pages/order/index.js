var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        orders: []
    },
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        this.getNATorder();
    },
    //   获取报告
    getNATorder() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8083) + "/orders",
            method: 'POST',
            //成功返回的数据
            data: {
                "openid": openid,
            },
            success: function (res) {
                res.data.forEach((r) => {
                    var date = new Date(Number(r.o_time));
                    var y = date.getFullYear();
                    var m = date.getMonth() + 1;
                    m = m < 10 ? ('0' + m) : m;
                    var d = date.getDate();
                    d = d < 10 ? ('0' + d) : d;
                    var h = date.getHours();
                    h = h < 10 ? ('0' + h) : h;
                    var minute = date.getMinutes();
                    var second = date.getSeconds();
                    minute = minute < 10 ? ('0' + minute) : minute;
                    second = second < 10 ? ('0' + second) : second;
                    var date = y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
                    r.o_time = date;
                    r.o_is_h = r.o_is_h == false ? '单采' : '混采';
                    r.money = r.o_is_h == '单采' ? 40 : 7;
                })
                that.setData({
                    orders: res.data
                })
            }
        });
    },
})