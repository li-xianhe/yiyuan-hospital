// pages/Depression/test/index.js

var app = getApp();
Page({
    data: {
        problemList: [],
        scorelist: [],
        sum: 0
    },
    onLoad(options) {
        this.getproblem();
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    // 获取问题
    getproblem() {
        var that = this;
        wx.request({
            url: app.getPort(8082) + '/tests',
            method: 'GET',
            success: function (res) {
                that.setData({
                    problemList: res.data
                })
            }
        });
    },

    formSubmit(e) {
        var that = this;
        var sum = 0;
        for (var i = 1; i <= 30; i++) {
            sum = sum + parseInt(e.detail.value[i]);
            this.setData({
                sum: sum
            })
        }
        if (that.data.sum) {
            wx.redirectTo({
                url: '/pages/Depression/result/index?sum=' + that.data.sum
            })
        } else {
            wx.showModal({
                title: '错误提示',
                content: '您有题未完成！',
                showCancel: false,
                success(res) {

                }
            });
        }
    }
})