// pages/Depression/result/index.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        result: [],
        score: '',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        var that = this;
        let {
            sum
        } = options;
        wx.request({
            url: app.getPort(8082) + '/standards',
            method: 'POST',
            //发送的数据
            data: {
                "score": sum
            },
            //成功返回的数据
            success: function (res) {
                that.setData({
                    result: res.data,
                    score: sum
                })
            }
        });
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
})