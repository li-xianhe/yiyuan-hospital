var app = getApp();
Page({
    data: {
        doctors: [],
        num: 0
    },
    onLoad: function (options) {
        const {
            cid
        } = options;
        this.getGoodsDetail(cid);
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    getGoodsDetail(cid) {
        var that = this;
        wx.request({
            url: app.getPort(8086) + '/doctors_by_work/' + cid,
            method: 'GET',
            success: function (res) {
                that.setData({
                    doctors: res.data,
                    num: res.data.length
                })
            }
        })
    },


})