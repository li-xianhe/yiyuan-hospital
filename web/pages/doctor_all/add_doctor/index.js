// pages/sleep_cost/index.js

let app = getApp();
Page({
    data: {
        isSubmit: false,
        name: "",
        email: "",
        Email: "",
        age: "",
        sex: "男",
        timer: "",
        desc: "",
        best: "",
        openid: "",
        Num: '60',
        user: {},
        isDisabled: false,
        // 邮箱是否已被注册
        isVerify: true,
        // 邮箱是否合法
        isLegal: true,
        // 验证码错误提示是否显示
        isCodeError: true,
        // 验证码过期提示是否显示
        isCodeOut: true
    },
    Flag: 0,
    email: "",
    verify: "",
    onLoad: function (options) {
        this.getname();
        this.getuser();
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").doc_open_id;
            this.setData({
                user: wx.getStorageSync("user")
            })
        } else {
            var openid = wx.getStorageSync("openid");
        }
        this.setData({
            openid: openid
        })
    },
    // 获取名字
    getname() {
        const userinfo = wx.getStorageSync("userinfo");
        this.setData({
            name: userinfo.nickName,
        })
    },
    //  提交
    formSubmit: function (e) {
        wx.request({
            url: app.getPort(8086) + '/prefect/doctor',
            method: 'PUT',
            data: {
                "openid": e.detail.value.openid,
                "name": e.detail.value.name,
                "email": e.detail.value.email,
                "age": e.detail.value.age,
                "sex": e.detail.value.sex,
                "desc": e.detail.value.desc,
                "best": e.detail.value.best,
            },
        })
        let {
            openid,
            name,
            email,
            age,
            desc,
            best,
            sex,
        } = e.detail.value;

        this.setData({
            isSubmit: true,
            openid,
            name,
            email,
            age,
            desc,
            best,
            sex
        })
    },
    // 点击邮箱
    handleemail() {
        this.setData({
            // isCode:false,
            isVerify: true,
            isLegal: true
        })
    },
    // 邮箱失去焦点触发事件
    eaway(e) {
        const email = e.detail.value;
        this.email = e.detail.value;
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/email/register',
                method: 'POST',
                data: {
                    "email": email,
                },
                success: res => {
                    if (res.data == 1) {
                        that.setData({
                            isVerify: false,
                            // isDisabled:true
                        })
                    } else if (res.data == -1) {
                        that.setData({
                            isLegal: false,
                            // isDisabled:true
                        })
                    } else {
                        this.Flag = 1;
                    }
                }
            })
        }
    },
    // 验证码获得焦点
    handlecode() {
        this.setData({
            // 验证码错误提示是否显示
            isCodeError: true,
            // 验证码过期提示是否显示
            isCodeOut: true
        })
    },
    // 验证码失去焦点触发
    vaway(e) {
        const verify = e.detail.value;
        const email = this.email;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/email/code',
                method: 'POST',
                data: {
                    "data": {
                        "email": email,
                        "code": verify
                    }
                },
                success: res => {
                    if (res.data == 'out of date') {
                        this.setData({
                            isCodeOut: false
                        })
                    } else if (res.data == 'error') {
                        this.setData({
                            isCodeError: false
                        })
                    }
                }
            })
        }
    },
    /**
        验证码倒计时
    */
    countDown: function () {
        const email = this.email;
        // this.isEmail_isnormal();
        wx.request({
            url: app.getPort(8086) + '/verify/email/register',
            method: 'POST',
            data: {
                "email": email,
            },
            success: res => {
                if (res.data == 1) {
                    this.setData({
                        isVerify: false,
                    })
                } else if (res.data == -1) {
                    this.setData({
                        isLegal: false,
                    })
                } else {
                    this.Flag = 1;
                }
            }
        })
        if (this.Flag === 1) {
            this.Flag = 0;
            const email = this.email;
            var that = this;
            var Num = that.data.Num
            var isDisabled = this.data.isDisabled
            var timer = setInterval(function () {
                Num -= 1;
                that.setData({
                    Num: Num,
                    isDisabled: true
                })
                if (Num <= -1) {
                    clearInterval(timer)
                    that.setData({
                        Num: 60,
                        isDisabled: false
                    })
                }
            }, 1000)
            wx.request({
                url: app.getPort(8086) + '/msg/sendEmail',
                method: 'POST',
                data: {
                    "email": email
                },
            })
        }
    },
    getuser() {
        const user = wx.getStorageSync("user");
        this.setData({
            Email: user.p_email,
        })
    }
})