var app = getApp();
Page({
    data: {
        doctor: [],
        BaseFontSize: '',
    },
    onLoad: function (options) {
        const {
            cid
        } = options;
        this.getGoodsDetail(cid);
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    onShow() {
        this.setData({
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    //获取详情
    getGoodsDetail(cid) {
        var that = this;
        wx.request({
            url: app.getPort(8086) + '/doctors/' + cid,
            method: 'GET',
            success: function (res) {
                that.setData({
                    doctor: res.data,
                })
            }
        })
    },
})