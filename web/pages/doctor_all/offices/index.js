var app = getApp();
Page({
    data: {
        works: [],
        BaseFontSize: '',
    },
    onShow() {
        this.getGoodsDetail();
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        this.setData({
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    getGoodsDetail() {
        var that = this;
        wx.request({
            url: app.getPort(8086) + '/works',
            method: 'GET',
            success: function (res) {
                that.setData({
                    works: res.data,
                })
            }
        })
    },


})