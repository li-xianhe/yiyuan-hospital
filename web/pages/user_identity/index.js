// pages/user_identity/index.js
var app = getApp()
Page({
  data: {
    images:[
        app.globalData.Global_imgUrl+'/images/resident.png',
        app.globalData.Global_imgUrl+'/images/doctor.png',
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 发送给朋友、分享朋友圈
    app.onShareAppMessage();
  },
})