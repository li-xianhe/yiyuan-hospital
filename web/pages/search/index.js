var app = getApp();
Page({
    data: {
        drug: [],
        Drug: [],
        List: [],
        ranking: [],
        history: [],
        keyword: '',
        // 取消 按钮 是否显示
        isFocus: false,
        // 输入框的值
        inpValue: "",
        // 榜单是否隐藏
        ishide: true,
        BaseFontSize: '',
        click: 0,
        img: '',
        images:[
            app.globalData.Global_imgUrl+'/icons/search2.png',
            app.globalData.Global_imgUrl+'/icons/zuoshang.png',
        ]
    },
    TimeId: -1,
    onLoad: function () {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        this.getranking();
        this.setData({
            img: app.globalData.Global_imgUrl + '/icons/search.png'
        })
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        if (openid) {
            wx.request({
                url: app.getPort(8081) + '/history',
                method: 'POST',
                data: {
                    "openid": openid
                },
                header: {
                    'Content-Type': 'application/json'
                },
                success: res => {

                    that.setData({
                        history: res.data
                    })
                }
            })
        }
    },
    onShow() {
        this.setData({
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    // 输入框获得焦点
    handleFocus(e) {
        this.setData({
            click: 1,
            img: app.globalData.Global_imgUrl + '/icons/search_active.png'
        })
    },
    // 输入框的值改变 就会触发的事件
    handleInput(e) {
        // 1 获取输入框的值
        const {
            value
        } = e.detail;

        this.setData({
            keyword: e.detail.value
        })
        // 2 检测合法性
        if (!value.trim()) {
            this.setData({
                drug: [],
                isFocus: false,
                ishide: true
            })
            // 值不合法
            return;
        }
        // 3 准备发送请求获取数据
        this.setData({
            isFocus: true,
            ishide: false,
        })
        clearTimeout(this.TimeId);
        this.TimeId = setTimeout(() => {
            this.qsearch(value);
        }, 1000);
    },
    // 输入框失去焦点
    awayInput(e) {
        this.setData({
            click: 0,
            img: app.globalData.Global_imgUrl + '/icons/search.png'
        })
    },
    // 发送请求获取搜索建议 数据
    qsearch(query) {
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/search',
            method: 'POST',
            data: {
                "keyword": query,
            },
            header: {
                'Content-Type': 'application/json'
            },
            success: res => {
                that.setData({
                    drug: res.data
                })
                app.globalData.search_List = [];
                const arr = app.globalData.search_List;
                for (var i = 0; i < res.data.length; i++) {

                    var drugId = String(res.data[i].id)
                    wx.request({
                        url: app.getPort(8081) + '/drugs/id',
                        method: 'POST',
                        data: {
                            "drugId": drugId
                        },
                        //成功返回的数据
                        success: function (result) {
                            arr.unshift(result.data);
                        }
                    });
                }
                app.globalData.search_List = arr;
            }
        })
    },
    // 点击搜索按钮
    handleSearch() {
        var that = this;
        var keyword = this.data.keyword;

        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }

        if (openid) {
            wx.request({
                url: app.getPort(8081) + '/history',
                method: 'PUT',
                data: {
                    "username": openid,
                    "keyword": keyword
                },
                header: {
                    'Content-Type': 'application/json'
                },
                success: res => {

                }
            })
        }
        wx.redirectTo({
            url: '/pages/search_list/index'
        })
    },
    // 获取排行榜
    getranking() {
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/sort',
            method: 'GET',
            //成功返回的数据
            success: function (res) {

                // 获取排行榜里的药名
                for (var i = 0; i < res.data.length; i++) {
                    var drugId = String(res.data[i].id)
                    wx.request({
                        url: app.getPort(8081) + '/drugs/id',
                        method: 'POST',
                        data: {
                            "drugId": drugId
                        },
                        //成功返回的数据
                        success: function (result) {
                            var arr = that.data.Drug;
                            arr.push(result.data.d_name)
                            that.setData({
                                Drug: arr
                            })
                        }
                    });
                }
                that.setData({
                    ranking: res.data
                })
            }
        });
    },
    // 点击历史
    handlehistory(e) {
        this.setData({
            inpValue: e.target.id,
            isFocus: true,
            ishide: false
        })
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/search',
            method: 'POST',
            data: {
                "keyword": e.target.id,
            },
            header: {
                'Content-Type': 'application/json'
            },
            success: res => {
                app.globalData.search_List = [];
                const arr = app.globalData.search_List;
                for (var i = 0; i < res.data.length; i++) {

                    var drugId = String(res.data[i].id)
                    wx.request({
                        url: app.getPort(8081) + '/drugs/id',
                        method: 'POST',
                        data: {
                            "drugId": drugId
                        },
                        //成功返回的数据
                        success: function (result) {
                            arr.unshift(result.data);
                        }
                    });
                }
                app.globalData.search_List = arr;
                wx.redirectTo({
                    url: '/pages/search_list/index'
                })
            }
        })
    },
})