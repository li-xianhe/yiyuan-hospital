var app = getApp();
var id = 0;
Page({
    data: {
        collects: [],
        num: 0,
        delBtnWidth: 75 ,//删除按钮宽度单位（rpx）
        image:app.globalData.Global_imgUrl+'/images/nocollect.jpg',
    },

    onLoad: function (options) {
        this.getCollects();
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    // 获取用户收藏信息
    getCollects() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8086) + '/collects/_all',
            method: 'POST',
            data: {
                "openid": openid
            },
            //成功返回的数据
            success: function (res) {
                res.data.forEach((r) => {
                    r.txtStyle = ""
                })
                that.setData({
                    collects: res.data,
                    num: res.data.length
                })
            }
        })
    },
    touchS: function (e) {
        // e.touches[0].clientX
        if (e.touches.length == 1) {
            this.setData({
                //设置触摸起始点水平方向位置
                startX: e.touches[0].clientX
            });
        }
    },
    touchM: function (e) {
        if (e.touches.length == 1) {
            //手指移动时水平方向位置
            var moveX = e.touches[0].clientX;
            //手指起始点位置与移动期间的差值
            var disX = this.data.startX - moveX;
            var delBtnWidth = this.data.delBtnWidth;
            var txtStyle = "";
            if (disX == 0 || disX < 0) { //如果移动距离小于等于0，文本层位置不变
                txtStyle = "left:0px";
            } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离
                txtStyle = "left:-" + disX + "px";
                if (disX >= delBtnWidth) {
                    //控制手指移动距离最大值为删除按钮的宽度
                    txtStyle = "left:-" + delBtnWidth + "px";
                }
            }
            //获取手指触摸的是哪一项
            var index = e.target.dataset.index;
            var collects = this.data.collects;
            collects[index].txtStyle = txtStyle;
            //更新列表的状态
            this.setData({
                collects: collects
            });
        }
    },

    touchE: function (e) {
        if (e.changedTouches.length == 1) {
            //手指移动结束后水平位置
            var endX = e.changedTouches[0].clientX;
            //触摸开始与结束，手指移动的距离
            var disX = this.data.startX - endX;
            var delBtnWidth = this.data.delBtnWidth;
            //如果距离小于删除按钮的1/2，不显示删除按钮
            var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
            //获取手指触摸的是哪一项
            var index = e.target.dataset.index;
            var collects = this.data.collects;
            collects[index].txtStyle = txtStyle;
            //更新列表的状态
            this.setData({
                collects: collects
            });
        }
    },
    //获取元素自适应后的实际宽度
    getEleWidth: function (w) {
        var real = 0;
        try {
            var res = wx.getSystemInfoSync().windowWidth;
            var scale = (750 / 2) / (w / 2); //以宽度750px设计稿做宽度的自适应
            real = Math.floor(res / scale);
            return real;
        } catch (e) {
            return false;
            // Do something when catch error
        }
    },
    initEleWidth: function () {
        var delBtnWidth = this.getEleWidth(this.data.delBtnWidth);
        this.setData({
            delBtnWidth: delBtnWidth
        });
    },
    //点击删除按钮事件
    delItem: function (e) {
        //获取列表中要删除项的下标
        var index = e.target.dataset.index;
        var collects = this.data.collects;
        var id = collects[index].drug.d_id;
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8086) + '/collects',
            method: 'DELETE',
            data: {
                "openid": openid,
                "drugId": id
            },
        })
        //移除列表中下标为index的项
        collects.splice(index, 1);
        //更新列表的状态
        that.setData({
            collects: collects,
            num: that.data.num - 1
        });
    },
    //   点击挑选
    handleSection() {
        wx.switchTab({
            url: '/pages/category/index'
        })
    },

})