// pages/sleep_cost/index.js

let app = getApp();

var is_dao = false;
var username_flag = false;
var password_is_error = false;
var password_same = false;
var email_flag = false;
var verify_code = false;
var isSuccess = false;
Page({
    data: {
        isSubmit: false,
        warn: "",
        email: "",
        username: "",
        pwd: "",
        repwd: "",
        timer: "",
        Num: '120',
        isDisabled: false,
        // 邮箱是否已被注册
        isVerify: true,
        // 邮箱是否合法
        isLegal: true,
        // 验证码错误提示是否显示
        isCodeError: true,
        // 验证码过期提示是否显示
        isCodeOut: true,
        // 用户名是否重复
        isRename: true,
        // 密码含有非法字符
        isPwdlegal: true,
        // 密码小于6位
        isPwdsix: true,
        // 密码不是由大写字母、小写字母、数字组成!
        isPwdmake: true,
        // 密码是否相同
        isPwdsame: true,
        // 是否提交
        iscommit: true
    },
    pwd: "",
    repwd: "",
    Flag: 0,
    name: "",
    email: "",
    verify: "",

    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        this.puzzleVerify = this.selectComponent("#puzzleVerify");
    },
    // 用户名点击事件
    handlename() {
        this.setData({
            isRename: true
        })
    },
    // 用户名失去焦点
    awayname(e) {
        const name = e.detail.value;
        this.name = e.detail.value;
        var that = this;
        that.setData({
            username: name
        })
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/username/register',
                method: 'POST',
                data: {
                    "username": name,
                },
                success: res => {
                    if (res.data == 1) {
                        that.setData({
                            isRename: false,
                        })
                    } else {
                        username_flag = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 密码获得焦点
    handlepwd() {
        this.setData({
            // 密码含有非法字符
            isPwdlegal: true,
            // 密码小于6位
            isPwdsix: true,
            // 密码不是由大写字母、小写字母、数字组成!
            isPwdmake: true
        })
    },
    // 密码失去焦点
    awaypwd(e) {
        this.setData({
            pwd: e.detail.value,
        })
        const pwd = e.detail.value;
        this.pwd = e.detail.value;
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/password/legal',
                method: 'POST',
                data: {
                    "password": pwd,
                },
                success: res => {
                    if (res.data == -1) {
                        that.setData({
                            isPwdlegal: false
                            // isDisabled:true
                        })
                    } else if (res.data == -2) {
                        that.setData({
                            isPwdsix: false
                        })
                    } else if (res.data == 0) {
                        that.setData({
                            isPwdmake: false
                        })
                    } else {
                        password_is_error = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 确认密码获得焦点
    handlerepwd() {
        this.setData({
            isPwdsame: true,
        })
    },
    // 确认密码失去焦点
    awayrepwd(e) {
        const repwd = e.detail.value;
        var pwd = this.pwd;
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/password/same',
                method: 'POST',
                data: {
                    "password1": pwd,
                    "password2": repwd,
                },
                success: res => {
                    if (res.data == 0) {
                        that.setData({
                            isPwdsame: false
                        })
                    } else {
                        password_same = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 点击邮箱
    handleemail() {
        this.setData({
            // isCode:false,
            isVerify: true,
            isLegal: true
        })
    },
    // 邮箱失去焦点触发事件
    eaway(e) {
        const email = e.detail.value;
        this.email = e.detail.value;
        var that = this;
        that.setData({
            email: email
        })
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/email/register',
                method: 'POST',
                data: {
                    "email": email,
                },
                success: res => {
                    if (res.data == 1) {
                        that.setData({
                            isVerify: false,
                            // isDisabled:true
                        })
                        // that.handlecommit();
                    } else if (res.data == -1) {
                        that.setData({
                            isLegal: false,
                            // isDisabled:true
                        })
                        // that.handlecommit();
                    } else {
                        this.Flag = 1;
                        email_flag = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 验证码获得焦点
    handlecode() {
        this.setData({
            // 验证码错误提示是否显示
            isCodeError: true,
            // 验证码过期提示是否显示
            isCodeOut: true
        })
    },

    // 验证码倒计时
    countdown: function () {
        if (!is_dao) {
            const email = this.email;
            wx.request({
                url: app.getPort(8086) + '/verify/email/register',
                method: 'POST',
                data: {
                    "email": email,
                },
                success: res => {
                    if (res.data == 1) {
                        this.setData({
                            isVerify: false,
                            // isDisabled:true
                        })
                    } else if (res.data == -1) {
                        this.setData({
                            isLegal: false,
                            // isDisabled:true
                        })
                    } else {
                        this.Flag = 1;
                    }
                }
            })
            if (this.Flag === 1) {
                this.Flag = 0;
                const email = this.email;
                var that = this;
                var Num = that.data.Num
                var isDisabled = this.data.isDisabled
                var timer = setInterval(function () {
                    Num -= 1;
                    is_dao = true;
                    that.setData({
                        Num: Num,
                        isDisabled: true
                    })
                    if (Num <= -1) {
                        clearInterval(timer)
                        that.setData({
                            Num: 120,
                            isDisabled: false
                        })
                    }
                }, 1000)
                wx.request({
                    url: app.getPort(8086) + '/msg/sendEmail',
                    method: 'POST',
                    data: {
                        "email": email
                    },
                })
            }
        }
    },
    // 是否能提交
    handlecommit() {
        if (username_flag && password_is_error && password_same && email_flag && verify_code) {
            this.setData({
                iscommit: false
            })
        }
    },
    // 验证码输入事件
    code(e) {
        var that = this;
        const verify = e.detail.value;
        if (e.detail.value.length == 6) {
            const email = this.email;
            if (e.detail.value != "") {
                wx.request({
                    url: app.getPort(8086) + '/verify/email/code',
                    method: 'POST',
                    data: {

                        "email": email,
                        "code": verify

                    },
                    success: res => {
                        if (res.data == -1) {
                            this.setData({
                                isCodeOut: false
                            })
                        } else if (res.data == 0) {
                            this.setData({
                                isCodeError: false
                            })
                        } else {
                            verify_code = true;
                            this.handlecommit();
                        }
                    }
                })
            }
        }

    },
    // 滑块验证成功操作
    countDown() {
        var that = this;
        wx.request({
            url: app.getPort(8086) + '/register',
            method: 'POST',
            data: {
                "username": that.data.username,
                "password": that.data.pwd,
                "email": that.data.email,
                "status": app.globalData.identity
            },
            success: function (res) {
                if (res.data == 1) {
                    wx.navigateTo({
                        url: '/pages/auth/accountLogin/accountLogin'
                    })
                }
            }
        })
    },
    // //  提交
    formSubmit: function (e) {
        this.puzzleVerify.visidlisd();
        if (e.detail.value.email != '' && e.detail.value.pwd != '' && e.detail.value.repwd != '' && e.detail.value.username != '' && e.detail.value.verify != '') {
            let {
                username,
                email,
                pwd
            } = e.detail.value;

            this.setData({
                isSubmit: true,
                username,
                email,
                pwd
            })
        }
    },

})