var app = getApp();
var is_dao = false;
var username_flag = false;
var password_is_error = false;
var password_same = false;
var email_flag = false;
var verify_code = false;
Page({
    data: {
        username: '',
        code: '',
        password: '',
        confirmPassword: '',
        email: '',
        Num: 120,
        // 邮箱是否已被注册
        isVerify: true,
        // 邮箱是否合法
        isLegal: true,
        // 验证码错误提示是否显示
        isCodeError: true,
        // 验证码过期提示是否显示
        isCodeOut: true,
        // 密码含有非法字符
        isPwdlegal: true,
        // 密码小于6位
        isPwdsix: true,
        // 密码不是由大写字母、小写字母、数字组成!
        isPwdmake: true,
        // 密码是否相同
        isPwdsame: true,
        // 是否重置
        isDisabled: true
    },
    Flag: 0,
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    // 验证码倒计时
    countDown: function () {
        if (!is_dao) {
            const email = this.data.email;
            // this.isEmail_isnormal();
            wx.request({
                url: app.getPort(8086) + '/verify/email/register',
                method: 'POST',
                data: {
                    "email": email,
                },
                success: res => {
                    if (res.data == 0) {
                        this.setData({
                            isVerify: false,
                            // isDisabled:true
                        })
                    } else if (res.data == -1) {
                        this.setData({
                            isLegal: false,
                            // isDisabled:true
                        })
                    } else {
                        this.Flag = 1;
                    }
                }
            })

            if (this.Flag === 1) {
                this.Flag = 0;
                const email = this.data.email;

                var that = this;
                var Num = that.data.Num
                var isDisabled = this.data.isDisabled
                var timer = setInterval(function () {
                    Num -= 1;
                    is_dao = true;
                    that.setData({
                        Num: Num,
                    })
                    if (Num <= -1) {
                        clearInterval(timer)
                        that.setData({
                            Num: 120,
                        })
                    }
                }, 1000)


                wx.request({
                    url: app.getPort(8086) + '/msg/sendEmail',
                    method: 'POST',
                    data: {
                        "email": email
                    },
                })
            }
        }
    },
    // 验证码获得焦点
    handlecode() {
        this.setData({
            // 验证码错误提示是否显示
            isCodeError: true,
            // 验证码过期提示是否显示
            isCodeOut: true,
        })
    },
    // 输入验证码
    code(e) {

        var verify = e.detail.value;
        if (e.detail.value.length == 6) {
            const email = this.data.email;
            wx.request({
                url: app.getPort(8086) + '/verify/email/code',
                method: 'POST',
                data: {
                    "email": email,
                    "code": verify
                },
                success: res => {
                    if (res.data == -1) {

                        this.setData({
                            isCodeOut: false
                        })
                    } else if (res.data == 0) {

                        this.setData({
                            isCodeError: false
                        })
                    } else {
                        verify_code = true;

                        this.handlecommit();
                    }
                }
            })

        }
    },
    // 密码重置
    startReset: function () {
        var that = this;
        var email = that.data.email;
        var password = that.data.password;

        wx.request({
            url: app.getPort(8086) + '/forget',
            method: 'POST',
            data: {
                "email": email,
                "password": password,
                "status": app.globalData.identity
            },
            success: function (res) {
                if (res.data == 1) {
                    wx.navigateTo({
                        url: '/pages/auth/accountLogin/accountLogin'
                    })
                }
            }
        });
    },
    // 密码获得焦点
    bindPasswordInput: function (e) {
        this.setData({
            password: e.detail.value,
            // 密码含有非法字符
            isPwdlegal: true,
            // 密码小于6位
            isPwdsix: true,
            // 密码不是由大写字母、小写字母、数字组成!
            isPwdmake: true
        });
    },
    // 密码输入失去焦点
    pwdaway(e) {
        const pwd = e.detail.value;
        this.setData({
            password: e.detail.value
        })
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/password/legal',
                method: 'POST',
                data: {
                    "password": pwd,
                },
                success: res => {
                    if (res.data == -1) {
                        that.setData({
                            isPwdlegal: false
                        })
                    } else if (res.data == -2) {
                        that.setData({
                            isPwdsix: false
                        })
                    } else if (res.data == 0) {
                        that.setData({
                            isPwdmake: false
                        })
                    } else {
                        password_is_error = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 确认密码获得焦点
    bindConfirmPasswordInput: function (e) {
        this.setData({
            confirmPassword: e.detail.value,
            isPwdsame: true,
        });
    },
    // 确认密码失去焦点
    inputrepwd(e) {
        const repwd = e.detail.value;
        var pwd = this.data.password;
        var that = this;
        if (e.detail.value.length == pwd.length) {
            wx.request({
                url: app.getPort(8086) + '/verify/password/same',
                method: 'POST',
                data: {
                    "password1": pwd,
                    "password2": repwd,
                },
                success: res => {
                    if (res.data == 0) {
                        that.setData({
                            isPwdsame: false
                        })
                    } else {
                        password_same = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 账号获得焦点
    bindUsernameInput: function (e) {
        this.setData({
            email: e.detail.value,
            isVerify: true,
            isLegal: true
        });
    },
    // 邮箱输入失去焦点
    nameaway(e) {
        this.setData({
            email: e.detail.value
        })
        const email = e.detail.value;
        this.email = e.detail.value;
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/email/register',
                method: 'POST',
                data: {
                    "email": email,
                },
                success: res => {
                    if (res.data == 0) {
                        that.setData({
                            isVerify: false,
                        })
                    } else if (res.data == -1) {
                        that.setData({
                            isLegal: false,
                        })
                    } else {
                        this.Flag = 1;
                        email_flag = true;
                        that.handlecommit();
                    }
                }
            })
        }
    },
    // 是否能提交
    handlecommit() {

        if (email_flag && verify_code && password_is_error && password_same) {
            this.setData({
                isDisabled: false
            })
        }
    },

})