var app = getApp();
var _username = true;
var _email = true;
var kemail = false;
var kpassword = false;
var days = 0;
let openid = '';
var id = '';
var uuid = '';
var kcode = false;
Page({
    data: {
        username: '',
        password: '',
        code: '',
        loginErrorCount: 0,
        // 账号错误
        isUsernameError: true,
        isDisabled: true,
        click: 0,
        ImgUrl: '',
        sildeBlockCont: '', //滑块背景图
        // 验证码
        VerifyCode: '',
        iscode: true,
        isUser_Pwd: true,
        images: [
            app.globalData.Global_imgUrl + '/images/zhanghao.png',
            app.globalData.Global_imgUrl + '/images/password.png',
            app.globalData.Global_imgUrl + '/images/code.png',
        ]
    },
    appId: 'wxd34f960e8de43d52',
    appSecret: '2e866c2e1ddf3ad7026450d9dde9d828',
    onLoad: function (options) {
        // 页面初始化 options为页面跳转所带来的参数
        // 页面渲染完成
        const {
            cid
        } = options;
        app.globalData.identity = cid
        wx.setStorageSync("identity_id", cid);
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        // this.getCode();
        var s = wx.getStorageSync('cookieKey');
        uuid = Date.now().toString(36);
    },
    onReady() {
        var that = this;
        // 获取openid
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        openid = openid;
        wx.request({
            url: app.getPort(8086) + '/login/getVerify',
            method: 'POST',
            data: {
                "uuid": uuid
            },
            responseType: "arraybuffer", //此处是请求文件流，必须带入的属性
            success: function (res) {
                if (res.statusCode === 200) {
                    var imgSrc = wx.arrayBufferToBase64(res.data); //二进制流转为base64编码
                    var save = wx.getFileSystemManager();
                    var number = Math.random();
                    save.writeFile({
                        filePath: wx.env.USER_DATA_PATH + '/pic' + number + '.png',
                        data: imgSrc,
                        encoding: 'base64',
                        success: res => {
                            that.setData({
                                ImgUrl: wx.env.USER_DATA_PATH + '/pic' + number + '.png',
                            })
                        },
                        fail: err => {
                            console.log(err)
                        }
                    })
                }
            },
            fail: function (error) {
                console.log(error);
            }
        });

        this.puzzleVerify = this.selectComponent("#puzzleVerify");
    },
    // 用户名或邮箱失去焦点
    nameaway(e) {
        var username = e.detail.value;
        var that = this;
        if (e.detail.value != "") {
            wx.request({
                url: app.getPort(8086) + '/verify/username/register',
                method: 'POST',
                data: {
                    "username": username,
                },
                success: res => {
                    if (res.data == 1) {
                        _username = true;
                    } else {
                        _username = false;
                        if (_email == false) {
                            this.setData({
                                isUsernameError: false
                            })
                        }
                    }
                }
            })
            wx.request({
                url: app.getPort(8086) + '/verify/email/register',
                method: 'POST',
                data: {
                    "email": username,
                },
                success: res => {
                    if (res.data == 1) {} else if (res.data == 0) {
                        _email = false;
                        if (_username == false) {
                            this.setData({
                                isUsernameError: false
                            })
                        }
                    }
                }
            })
        }
    },
    // 用户名或邮箱输入
    bindUsernameInput(e) {
        this.setData({
            username: e.detail.value
        })
        if (e.detail.value != '') {
            kemail = true;
            if (kpassword) {
                this.setData({
                    isDisabled: false
                })
            }
        } else {
            kemail = false;
            this.setData({
                isDisabled: true,
                isUser_Pwd: true,
            })
        }
    },
    // 密码输入
    bindPasswordInput(e) {
        this.setData({
            password: e.detail.value
        })
        if (e.detail.value != '') {
            kpassword = true;
            this.setData({
                isDisabled: false,
                isUser_Pwd: true,
            })
        } else {
            kpassword = false;
            this.setData({
                isDisabled: true
            })
        }
    },
    // 获取验证码
    handleCode() {
        wx.redirectTo({
            url: '/pages/auth/accountLogin/accountLogin?cid=' + app.globalData.identity
        })
    },
    // 点击输入验证码
    handlecode() {
        this.setData({
            iscode: true,
            isUser_Pwd: true,
        })
    },
    // 输入验证码
    bindCodeInput(e) {
        var that = this;
        that.setData({
            VerifyCode: e.detail.value
        })
        if (e.detail.value.length == 4) {
            wx.request({
                url: app.getPort(8086) + '/login/checkVerify',
                method: 'POST',
                data: {
                    "code": that.data.VerifyCode,
                    "uuid": uuid
                },
                //成功返回的数据
                success: function (res) {
                    if (res.data == 1) {
                        kcode = true;
                    } else {
                        that.setData({
                            iscode: false
                        })
                    }
                }
            });
        }
    },
    // 账号登录
    Login() {
        // 出现验证图片
        this.puzzleVerify.visidlisd();
    },
    // 验证码图片滑动成功
    countDown() {
        var that = this;
        var username = this.data.username;
        var password = this.data.password;
        wx.request({
            url: app.getPort(8086) + '/login',
            method: 'POST',
            data: {
                "emailOrUserName": username,
                "password": password,
                "status": app.globalData.identity
            },
            success: function (res) {
                if (res.data) {
                    const user = res.data;
                    wx.setStorageSync("user", user);
                    wx.switchTab({
                        url: "/pages/user/index"
                    });
                } else {
                    that.setData({
                        isUser_Pwd: false
                    })
                }
            }
        })
    },
    // ------------登录方式切换----------------------
    //  点击账号登录
    handleaccount() {
        this.setData({
            click: 0
        })
    },
    // 点击微信登录
    handleweixin() {
        this.setData({
            click: 1
        })
    },
    // 获取微信用户信息 
    getUserProfile(e) {
        wx.getUserProfile({
            desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
                const userInfo = res.userInfo;
                wx.setStorageSync("userinfo", userInfo);
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                })
                var id = wx.getStorageSync("identity_id");
                const userinfo = wx.getStorageSync("userinfo");
                var appId = this.appId;
                var appSecret = this.appSecret;
                var that = this;
                wx.login({
                    success(res) {
                        wx.request({
                            url: 'https://api.weixin.qq.com/sns/jscode2session',
                            data: {
                                appid: appId,
                                secret: appSecret,
                                js_code: res.code,
                                grant_type: 'authorization_code'
                            },
                            method: "GET",
                            success(res) {
                                // 将得到的openid存入缓存

                                wx.setStorageSync("openid", res.data.openid);
                                wx.switchTab({
                                    url: "/pages/user/index"
                                })
                                // 给后端发信息和Openid
                                wx.request({
                                    url: app.getPort(8086) + '/wxlogin',
                                    method: 'POST',
                                    data: {
                                        "name": userinfo.nickName,
                                        "image": userinfo.avatarUrl,
                                        "openid": res.data.openid,
                                        "status": id
                                    },
                                    header: {
                                        'Content-Type': 'application/json'
                                    },
                                })
                            }
                        })
                    }
                })
            }
        })
    },
})