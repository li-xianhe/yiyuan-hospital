var app = getApp();
Page({
    data: {
        address: [],
        totalPrice: '',
        image: app.globalData.Global_imgUrl + '/images/1.png',
    },
    onLoad(options) {
        this.setData({
            totalPrice: app.globalData.totalPrice,
            checked_list: app.globalData.checked_List
        })
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    onShow() {
        this.getAddress();
    },
    // 获得地址列表
    getAddress() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8085) + '/address/_all',
            method: 'POST',
            //发送的数据
            data: {
                "openid": openid
            },
            //成功返回的数据
            success: function (res) {
                let index = res.data.findIndex(v => v.a_is_default === true);
                that.setData({
                    address: res.data[index]
                })
            }
        });
    },
    // 切换收货地址
    shift_address() {
        app.globalData.shift_address = 1;
        wx.redirectTo({
            url: '/pages/shop_address/address_list/index'
        })
    },
    // 点击添加收货地址
    handleadd() {
        app.globalData.alter_address = 0;
        wx.navigateTo({
            url: '/pages/shop_address/add_address/index'
        })
    },
    // 确认支付
    handleOrderPay() {
        if (this.data.address) {
            wx.navigateTo({
                url: '/pages/payment/countdown/index'
            })
        } else {
            wx.showModal({
                title: '重要通知',
                content: '请添加收货地址！',
                showCancel: false,
                //showCancel: false//加上此句话取消键没有
            })
        }
    }
})