var app = getApp();
Page({
    data: {
        address: {},
        cart: [],
        allChecked: false,
        totalPrice: 0,
        totalNum: 0,
        num: '',
    },
    onLoad: function (options) {
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
        if (!wx.getStorageSync("user") && !wx.getStorageSync("userinfo")){
            wx.showModal({
                title: '重要通知',
                content: '请您先登录！',
                showCancel: false,
                //showCancel: false//加上此句话取消键没有
                success(res) {
                    wx.switchTab({
                        url: "/pages/user/index"
                    })
                }
            })
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.getCartList();
    },
    // 获取购物车数据
    getCartList() {
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        wx.request({
            url: app.getPort(8087) + '/carts/_all',
            method: 'POST',
            data: {
                "openid": openid
            },
            //成功返回的数据
            success: function (res) {
                res.data.forEach((r) => {
                    r.checked = false
                })
                that.setData({
                    cart: res.data
                })
            }
        });
    },
    // 商品的选中,结算的数据
    handeItemChange(e) {
        let allChecked = true;
        var that = this;
        // 1 获取传递过来的参数 
        const {
            id
        } = e.currentTarget.dataset;
        let totalPrice = 0;
        let totalNum = 0;
        let index = that.data.cart.findIndex(v => v.drug.d_id === id);
        let checked = "cart[" + index + "].checked";
        if (e.detail.value.length > 0) {
            that.setData({
                [checked]: true
            })
        } else {
            that.setData({
                [checked]: false
            })
        }
        that.data.cart.forEach(v => {
            if (v.checked) {
                totalPrice += v.c_count * v.drug.d_price;
                totalNum += v.c_count;
            } else {
                allChecked = false;
            }
        })
        // 判断数组是否为空
        allChecked = that.data.cart.length != 0 ? allChecked : false;
        that.setData({
            "totalPrice": totalPrice,
            "totalNum": totalNum,
            "allChecked": allChecked
        })
    },
    //   商品数量加减
    handleItemNumEdit(e) {
        var that = this;
        let totalPrice = that.data.totalPrice;
        let totalNum = that.data.totalNum;
        // 1 获取传递过来的参数 
        const {
            operation,
            id,
            num
        } = e.currentTarget.dataset;
        var that = this;
        if (wx.getStorageSync("user")) {
            var openid = wx.getStorageSync("user").p_openid;
        } else {
            var openid = wx.getStorageSync("openid");
        }
        // 1 获取被修改的商品的id
        const d_id = id;
        // 2 获取购物车数组 
        let {
            cart
        } = this.data;
        // 3 找到被修改的商品对象
        let index = cart.findIndex(v => v.drug.d_id === d_id);
        if (cart[index].c_count == 1 && operation == -1) {
            wx.showModal({
                title: '重要通知',
                content: '确认删除药品？',
                //showCancel: false//加上此句话取消键没有
                //点击按钮触发
                success(res) {
                    if (res.confirm) {
                        wx.request({
                            url: app.getPort(8087) + '/carts',
                            method: 'DELETE',
                            data: {
                                "openid": openid,
                                "drugId": id,
                            },
                            //成功返回的数据
                            success: function (res) {
                                if (res.data == 1) {
                                    // 页面刷新
                                    that.onReady();
                                }
                            }
                        });
                    }
                }
            })
        } else {
            wx.request({
                url: app.getPort(8087) + '/carts',
                method: 'PUT',
                data: {
                    "openid": openid,
                    "drugId": id,
                    "count": operation
                },
                //成功返回的数据
                success: function (res) {
                    // 4  进行修改数量,修改列表里的元素
                    let setNum = "cart[" + index + "].c_count";
                    that.setData({
                        [setNum]: num + operation
                    })
                }
            });
        }
        if (cart[index].checked) {
            totalPrice += cart[index].drug.d_price * operation;
            totalNum += operation;
            that.setData({
                totalPrice: totalPrice,
                totalNum: totalNum
            })
        }
    },
    // 商品全选功能
    handleItemAllCheck() {
        var that = this;
        let totalPrice = 0;
        let totalNum = 0;
        // 1 获取data中的数据
        let {
            cart,
            allChecked
        } = that.data;
        // 2 修改值
        allChecked = !allChecked;
        // 3 循环修改cart数组 中的商品选中状态
        cart.forEach(v => v.checked = allChecked);
        if (allChecked) {
            that.data.cart.forEach(v => {
                totalPrice += v.c_count * v.drug.d_price;
                totalNum += v.c_count;
            })
        } else {
            totalPrice = 0;
            totalNum = 0;
        }
        // 4 把修改后的值 填充回data或者缓存中
        that.setData({
            cart: cart,
            allChecked: allChecked,
            totalPrice: totalPrice,
            totalNum: totalNum
        })
    },
    // 点击 结算 
    handlePay() {
        const {
            totalPrice,
            totalNum,
            cart
        } = this.data;
        // 找到被选中药品信息
        let checked_list = [];
        for (var i = 0; i < cart.length; i++) {
            if (cart[i].checked == true) {
                checked_list.unshift(cart[i])
            }
        }

        app.globalData.checked_List = checked_list;
        // 2 判断用户有没有选购商品
        if (totalNum === 0) {
            wx.showToast({
                title: "您还没有选购商品"
            });
            return;
        }
        app.globalData.totalPrice = totalPrice;
        // 3 跳转到 支付页面
        wx.navigateTo({
            url: '/pages/payment/pay/index'
        });
    },
    //   点击挑选
    handleSection() {
        wx.switchTab({
            url: '/pages/category/index'
        })
    }
})