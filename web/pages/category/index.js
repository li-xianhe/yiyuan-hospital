var app = getApp();
Page({
    data: {
        drug: [],
        rightContent: [],
        view: 1,
        BaseFontSize: '',
    },
    onLoad: function (options) {
        var that = this;
        this.getclassify();
        wx.request({
            url: app.getPort(8081) + '/drugs/classify/1',
            method: 'GET',
            //成功返回的数据
            success: function (res) {
                that.setData({
                    rightContent: res.data
                })
            }
        });
        // 发送给朋友、分享朋友圈
        app.onShareAppMessage();
    },
    onShow(){
        this.setData({
            BaseFontSize: app.globalData.BaseFontSize
        })
    },
    // 获取全部分类
    getclassify() {
        var that = this;
        wx.request({
            url: app.getPort(8081) + '/classifys',
            method: 'GET',
            //成功返回的数据
            success: function (res) {
                that.setData({
                    classify: res.data
                })
            }
        });
    },
    // 点击分类获取对应药品
    onClicktext: function (e) {
        var cid = e.currentTarget.dataset.cid;
        var that = this;
        wx.request({
            url: app.getPort(8081) + "/drugs/classify/" + cid,
            method: 'GET',
            //成功返回的数据
            success: function (res) {
                that.setData({
                    rightContent: res.data
                })
            }
        });
        this.setData({
            view: cid
        })
    },
})