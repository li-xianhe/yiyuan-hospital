// 同时发送异步代码的次数
let ajaxTimes=0;

export const request = (params) => {
    ajaxTimes++;
    // 显示加载中
    wx.showLoading({
        title: '加载中',
        mask:true
      })
    // 定义公共的url
     // 127.0.0.1本地
    // 192.168.0.115校园网
    // 192.168.43.99肥猪热点
    // http://8g5jtq.natappfree.cc
    // 8084与医院相关的信息获取端口
    const baseUrl="http://192.168.0.115:8080/hospital";
    return new Promise((resolve, reject) => {
        wx.request({
            ...params,
            url:baseUrl+params.url,
            success: (result) => {
                resolve(result);
            },
            fail: (err) => {
                reject(err);
            },
            complete:()=>{
                ajaxTimes--;
                // 关闭正在等待图标
                if(ajaxTimes===0){
                    wx.hideLoading();
                }
            }
        });
    })
}