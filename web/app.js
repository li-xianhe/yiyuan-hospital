App({
    globalData: {
        identity: '',
        posturl: '',
        search_List: [],
        // 填完信息收集跳转到不同页面1核酸检测预约，0预约挂号
        shift_order: 0,
        // 0时添加收货地址，1时更改收货地址
        alter_address: 0,
        // 修改地址时，需要修改的地址id
        address_id: '',
        // 切换收货地址1,保存、或者进入收货地址管理0
        shift_adress: 0,
        hasLogin: false,
        // 购物车选中的商品信息
        checked_List: [],
        navHeight: 300,
        // 基础字体大小
        BaseFontSize: 28,
        // 首页弹窗是否出现0出现，1不出现
        index_showModal: 0,
        Global_imgUrl: 'http://liuhuiying.natapp1.cc',
        totalPrice: ''
    },
    onLaunch: function () {
        const updateManager = wx.getUpdateManager();
        wx.getUpdateManager().onUpdateReady(function () {
            wx.showModal({
                title: '更新提示',
                content: '新版本已经准备好，是否重启应用？',
                success: function (res) {
                    if (res.confirm) {
                        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                        updateManager.applyUpdate()
                    }
                }
            })
        })
        // 获取顶部栏信息
        const getSystemInfo = wx.getSystemInfo();
        wx.getSystemInfo({
            success: res => {
                //导航高度
                this.globalData.navHeight = res.statusBarHeight + 46
            },
            fail: err => {
                console.log(err)
            }
        })
    },
    // 端口号
    getPort(port) {
        this.globalData.posturl = 'http://192.168.0.115:' + port + '/hospital';
        return this.globalData.posturl;
    },
    // 发送给朋友
    onShareAppMessage: function () {
        wx.showShareMenu({
            withShareTicket: true,
            menus: ['shareAppMessage', 'shareTimeline']
        })
        return {
            title: '意源医院',
            path: '/pages/index/index'
        }
    },
})