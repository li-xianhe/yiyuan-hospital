# hospital

## 介绍
一个基于微信小程序、SpringCloud、mybatis、redis、RabbitMQ，MongoDB的前后端分离的开源医院项目

## 技术选型

<img src="README.assets/1661909677785.png" alt="1661909677785" style="zoom:80%;float:left" />

## 编码规范

- 规范方式：后端严格遵守阿里编码规约，前端使用eslint；
- 命名统一：简介最大程度上达到了见名知意；
- 分包明确：层级分明可快速定位到代码位置；
- 注释完整：描述性高大量减少了开发人员的代码阅读工作量；
- 工具规范：使用统一jar包避免出现内容冲突；
- 代码整洁：可读性、维护性更高。

## **第一章**: 选题定位

### 1.1 **选题背景** 

卫生部进行了第三次全国卫生医疗调查，结果显示：我国城乡居民应就诊而未就诊的比例由1993年的36.4％上升到48.9％；患者应住院而没有住院的比例高达29.6％； 

在住院患者中，主动提出提前出院的比例为43.3％，其中六成以上是因为支付不起相关费用而提前出院的；农民应住院而没有住院的比例更是从1998年的63.7％上升到 

75.4％；因病致贫、因病返贫的农民占全部贫困农民的33.4％；在西部地区农村，62％的患者因为经济困难应治疗而没有治疗，75.1％的患者还没有治愈就要求提前出院。 

这些数字比较真实地反映出了城乡居民就医难的问题。 

尤其是疫情严重的今天，好多偏远乡村地区，看病、买药、做核酸检测环境十分恶劣，这也成为国家疫情管控的一个难题，所以为了援助偏远乡村地区并给国家疫情防 

控助力，我们医院计划做出一款小程序帮助这些人实现线上就诊，足不出户就可以在家里看病，购药以及预线上预约核酸检测。 

1.2 **目标用户** 

资源匮乏、卫生人力资源再生性困难、卫生服务能力严重不足的偏远乡村地区人民以及医疗条件较优越的城市地区的人 

## 第二章 产品定位

### 2.1 **小程序说明** 

本次参赛作品由于涉及药品购买以及无法实现支付功能（不是商家），因此提交为体验版

<img src="README.assets/1661909468031.png" alt="1661909468031" style="zoom:80%;float:left" />

### 2.2 **主要功能** 

* 预约核酸检测 
* 创建就诊人（包括填写就诊人信息） 
* 通过就诊人创建预约核酸检测的订单（包括病人填写检测时间 和 是否混采） 
* 核酸检测后的约10小时将可查询核酸检测的结果 
* 与医生远程在线交流 
* 病人足不出户即可实现在家诊病 
* 犹豫测试 
* 通过30道判断题，每道题有不同的分值，最后根据总分判断你是的心态是否健康 
* 药品购买 
* 可以收藏药品 
* 加入购物车 
* 创建收货地址 
* 支付（由于不是商家，所以采用模拟支付） 

### 2.3 **创新点** 

* 由于医院信息比较隐私，防止用户手机在别人手上就被轻易获取就诊信息，本程序可以使用账号密码（无记住密码）和微信两种登录方式 
* 为了满足部分用户的需求，本程序的字体有标准版和大字体版，方便老年人的使用 
* 病人足不出户即可在家与医生线上诊病。 
* 用户可以将小程序通过小程序页的形式分享给好友和微信群，便于用户直达服务 。 
* 小程序内一键接入医院客服，与绑定的微信运营者直接微信沟通，促成交易和服务沟通。 
* 设置药品的关键词，让用户快速搜索到心仪药品，相比于百度的网页搜索，小程序搜索实现了真实的服务搜索。 

### 2.4 **应用场景** 

本小程序的应用场景如下： 

（场景一）：让偏远贫困地区的人实现购药容易，看病便捷，对于这类人，本医院将提供极大的优惠政策。 

（场景二）：对于偏远地区的人们，线上诊病是一种非常有效的服务方式，可以实现在家诊病。 

## **第三章 界面设计** 

本小程序使用ColorUI组件库、iview-weapp组件库等多个组件库以及自己设计的组件库（如：自定义SearchInput组件） 

本次小程序一共设计了大约40个界面， UI设计规范统一（以蓝色为小程序主题颜色）、美观精致 ， 流程逻辑清晰 

用户易懂易用，主要的功能都已实现 

但是项目任务量庞大，很可能会存在一些我们未考虑周到的不足之处。 

### 3.1 **用户体验** 

本小程序经过本学校约60名学生使用，用户们有人提出一些意见，如：某个部分难以操作，某个界面的样式不美观等，经过长时间的用户测 

试、分析用户给出的意见、我们功能和样式经过不断的更新迭代，最终绝大部分功能保证通俗易懂，用户操作方便，不会出现严重的使Bug， 

合理运用设计策略引导用户行为，不存在过度营销现象。 

### 3.2 **界面展示** 

![1661909520229](README.assets/1661909520229.png)

![1661909548934](README.assets/1661909548934.png)

![1661909588595](README.assets/1661909588595.png)

![1661909600651](README.assets/1661909600651.png)

![1661909617125](README.assets/1661909617125.png)

![1661909630514](README.assets/1661909630514.png)

## **第四章 项目介绍** 

### 4.1 **项目开发时间** 

2022/4/1 - 2022/5/31 

### 4.2 Gitee 

https://gitee.com/li-xianhe/hospital 

### 4.3 **使用工具** 

前端开发工具：微信开发者工具 

后端开发工具：IDEA 

虚拟机：VMware 

测试工具：Postman 

代码管理工具：Git 

UI设计：墨刀 

思维导图工具：Xmind 