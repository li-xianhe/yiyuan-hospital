//年月日
var selYear = window.document.getElementById("selYear");
var selMonth = window.document.getElementById("selMonth");
var selDay = window.document.getElementById("selDay");
var date = new Date();
var year = date.getFullYear(); //返回年
var month = date.getMonth() + 1; //返回月份小一个月 月份必须加一才准
var dates = date.getDate(); //返回日
// 新建一个DateSelector类的实例，将三个select对象传进去
new DateSelector(selYear, selMonth, selDay, year, month, dates);
// 也可以试试下边的代码
// var dt = new Date(2004, 1, 29);
// new DateSelector(selYear, selMonth ,selDay, dt);

//获取选中年月日的值,这里有个问题,默认的年月日如果不选择,会发生报错,这时候应该设置文本域默认的value值和默认的年月日一样,就可以避免错误了
var selectY = document.getElementById('selYear');
var selectM = document.getElementById('selMonth');
var selectD = document.getElementById('selDay');
var h1 = document.getElementById('HF1');
var h2 = document.getElementById('HF2');
var h3 = document.getElementById('HF3');
selectY.onchange = function () {
    h1.value = selectY.options[selectY.selectedIndex].text;
}
selectM.onchange = function () {
    h2.value = selectM.options[selectM.selectedIndex].text;
}
selectD.onchange = function () {
    h3.value = selectD.options[selectD.selectedIndex].text;
}