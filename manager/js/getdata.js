// --------------------导航栏头部----------------------------
// 获取元素
var tab_list = document.querySelector('.tab_list');
var lis = tab_list.querySelectorAll('li');
var items = document.querySelectorAll('.item');
// for循环绑定点击事件
for (var i = 0; i < lis.length; i++) {
    // 开始给5个小li 设置索引号 
    lis[i].setAttribute('index', i);
    lis[i].onclick = function() {
        // 1. 上的模块选项卡，点击某一个，当前这一个底色会是红色，其余不变（排他思想） 修改类名的方式

        // 干掉所有人 其余的li清除 class 这个类
        for (var i = 0; i < lis.length; i++) {
            lis[i].className = '';
        }
        // 留下我自己 
        this.className = 'current';
        // 2. 下面的显示内容模块
        var index = this.getAttribute('index');
        // console.log(index);
        // 干掉所有人 让其余的item 这些div 隐藏
        for (var i = 0; i < items.length; i++) {
            items[i].style.display = 'none';
        }
        // 留下我自己 让对应的item 显示出来
        items[index].style.display = 'block';
    }
}
// --------------------导航栏内容----------------------------
// -------------------订单-----------------------------------

// 1
var httpRequest1 = new XMLHttpRequest(); //第一步：建立所需的对象
httpRequest1.open('GET', 'http://10.3.21.140:7000/order', true); //第二步：打开连接  将请求参数写在url中  ps:"./Ptest.php?name=test&nameone=testone"
httpRequest1.send(); //第三步：发送请求  将请求参数写在URL中
/**
 * 获取数据后的处理程序
 */
httpRequest1.onreadystatechange = function() {
    if (httpRequest1.readyState == 4 && httpRequest1.status == 200) {
        var json = httpRequest1.responseText; //获取到json字符串，还需解析
        var obj = JSON.parse(json);
        console.log(obj);
        let orders = obj.map((item, index) => {
            return Object.assign({}, { 'o_id': item.o_id, 'licence': item.sick.s_licence, 'name': item.sick.s_name, 'sex': item.sick.s_gender, 'phone': item.sick.s_phone, 'o_time': shift_time(item.o_time), })
        })
        console.log(orders);

        // 2. 往tbody 里面创建行： 有几个人（通过数组的长度）我们就创建几行
        var orders_tbody = document.querySelector('.orders_tbody');
        for (var i = 0; i < orders.length; i++) { // 外面的for循环管行 tr
            // 1. 创建 tr行
            var tr = document.createElement('tr');
            orders_tbody.appendChild(tr);
            // 2. 行里面创建单元格(跟数据有关系的3个单元格) td 单元格的数量取决于每个对象里面的属性个数  for循环遍历对象 datas[i]
            for (var k in orders[i]) { // 里面的for循环管列 td
                // 创建单元格 
                var td = document.createElement('td');
                // 把对象里面的属性值 datas[i][k] 给 td
                // console.log(datas[i][k]);
                td.innerHTML = orders[i][k];
                tr.appendChild(td);
            }
        }
    }
};
// -----------------------------------药品------------------------------
var httpRequest2 = new XMLHttpRequest(); //第一步：建立所需的对象
httpRequest2.open('GET', 'http://10.3.21.140:7000/drug', true); //第二步：打开连接  将请求参数写在url中  ps:"./Ptest.php?name=test&nameone=testone"
httpRequest2.send(); //第三步：发送请求  将请求参数写在URL中
/**
 * 获取数据后的处理程序
 */
httpRequest2.onreadystatechange = function() {
    if (httpRequest2.readyState == 4 && httpRequest2.status == 200) {
        var json = httpRequest2.responseText; //获取到json字符串，还需解析
        var obj2 = JSON.parse(json);
        console.log(obj2);
        let drugs = obj2.map((item, index) => {
            return Object.assign({}, { 'id': item.d_id, 'name': item.d_name, 'price': item.d_price, 'count': item.d_count, 'collect_count': item.collect_count, })
        })
        console.log(drugs);
        // 2. 往tbody 里面创建行： 有几个人（通过数组的长度）我们就创建几行
        var drugs_tobdy = document.querySelector('.drugs_tobdy');
        for (var i = 0; i < drugs.length; i++) { // 外面的for循环管行 tr
            // 1. 创建 tr行
            var tr = document.createElement('tr');
            drugs_tobdy.appendChild(tr);
            // 2. 行里面创建单元格(跟数据有关系的3个单元格) td 单元格的数量取决于每个对象里面的属性个数  for循环遍历对象 datas[i]
            for (var k in drugs[i]) { // 里面的for循环管列 td
                // 创建单元格 
                var td = document.createElement('td');
                // 把对象里面的属性值 datas[i][k] 给 td  
                // console.log(datas[i][k]);
                td.innerHTML = drugs[i][k];
                tr.appendChild(td);
            }
        }
    }
};


// ---------------------检查报告--------------------------------------------
var httpRequest3 = new XMLHttpRequest(); //第一步：建立所需的对象
httpRequest3.open('GET', 'http://10.3.21.140:7000/result', true); //第二步：打开连接  将请求参数写在url中  ps:"./Ptest.php?name=test&nameone=testone"
httpRequest3.send(); //第三步：发送请求  将请求参数写在URL中
/**
 * 获取数据后的处理程序
 */
httpRequest3.onreadystatechange = function() {
    if (httpRequest3.readyState == 4 && httpRequest3.status == 200) {
        var json = httpRequest3.responseText; //获取到json字符串，还需解析
        var obj3 = JSON.parse(json);
        // console.log(obj3);
        let reports = obj3.map((item, index) => {
            return Object.assign({}, { 'id': item.r_id, 'licence': item.order.sick.s_licence, 'name': item.order.sick.s_name, 'sex': item.order.sick.s_gender, 'result': item.r_res, 'type': item.order.o_is_h == 0 ? '单采' : '混采', })
        })
        console.log(reports[0].type);

        // 2. 往tbody 里面创建行： 有几个人（通过数组的长度）我们就创建几行
        var reports_tbody = document.querySelector('.reports_tbody');
        for (var i = 0; i < reports.length; i++) { // 外面的for循环管行 tr
            // 1. 创建 tr行
            var tr = document.createElement('tr');
            reports_tbody.appendChild(tr);
            // 2. 行里面创建单元格(跟数据有关系的3个单元格) td 单元格的数量取决于每个对象里面的属性个数  for循环遍历对象 datas[i]
            for (var k in reports[i]) { // 里面的for循环管列 td
                // 创建单元格 
                var td = document.createElement('td');
                // 把对象里面的属性值 datas[i][k] 给 td  
                // console.log(datas[i][k]);
                td.innerHTML = reports[i][k];
                tr.appendChild(td);
            }
        }
    }
};
//------------------------时间转换函数--------------------------------------------
function shift_time(time) {
    var date = new Date(Number(time));
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    var date = y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
    return date;
}